/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IMyDateModel() { }
if (false) {
    /** @type {?} */
    IMyDateModel.prototype.date;
    /** @type {?} */
    IMyDateModel.prototype.jsdate;
    /** @type {?} */
    IMyDateModel.prototype.formatted;
    /** @type {?} */
    IMyDateModel.prototype.epoc;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktZGF0ZS1tb2RlbC5pbnRlcmZhY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teWRhdGVwaWNrZXIvIiwic291cmNlcyI6WyJsaWIvaW50ZXJmYWNlcy9teS1kYXRlLW1vZGVsLmludGVyZmFjZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBRUEsa0NBS0M7OztJQUpHLDRCQUFjOztJQUNkLDhCQUFhOztJQUNiLGlDQUFrQjs7SUFDbEIsNEJBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJTXlEYXRlIH0gZnJvbSBcIi4vbXktZGF0ZS5pbnRlcmZhY2VcIjtcblxuZXhwb3J0IGludGVyZmFjZSBJTXlEYXRlTW9kZWwge1xuICAgIGRhdGU6IElNeURhdGU7XG4gICAganNkYXRlOiBEYXRlO1xuICAgIGZvcm1hdHRlZDogc3RyaW5nO1xuICAgIGVwb2M6IG51bWJlcjtcbn1cbiJdfQ==
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IMyCalendarDay() { }
if (false) {
    /** @type {?} */
    IMyCalendarDay.prototype.dateObj;
    /** @type {?} */
    IMyCalendarDay.prototype.cmo;
    /** @type {?} */
    IMyCalendarDay.prototype.currDay;
    /** @type {?} */
    IMyCalendarDay.prototype.disabled;
    /** @type {?} */
    IMyCalendarDay.prototype.markedDate;
    /** @type {?} */
    IMyCalendarDay.prototype.highlight;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktY2FsZW5kYXItZGF5LmludGVyZmFjZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215ZGF0ZXBpY2tlci8iLCJzb3VyY2VzIjpbImxpYi9pbnRlcmZhY2VzL215LWNhbGVuZGFyLWRheS5pbnRlcmZhY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUdBLG9DQU9DOzs7SUFORyxpQ0FBaUI7O0lBQ2pCLDZCQUFZOztJQUNaLGlDQUFpQjs7SUFDakIsa0NBQWtCOztJQUNsQixvQ0FBMEI7O0lBQzFCLG1DQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElNeURhdGUgfSBmcm9tIFwiLi9teS1kYXRlLmludGVyZmFjZVwiO1xuaW1wb3J0IHsgSU15TWFya2VkRGF0ZSB9IGZyb20gXCIuL215LW1hcmtlZC1kYXRlLmludGVyZmFjZVwiO1xuXG5leHBvcnQgaW50ZXJmYWNlIElNeUNhbGVuZGFyRGF5IHtcbiAgICBkYXRlT2JqOiBJTXlEYXRlO1xuICAgIGNtbzogbnVtYmVyO1xuICAgIGN1cnJEYXk6IGJvb2xlYW47XG4gICAgZGlzYWJsZWQ6IGJvb2xlYW47XG4gICAgbWFya2VkRGF0ZTogSU15TWFya2VkRGF0ZTtcbiAgICBoaWdobGlnaHQ6IGJvb2xlYW47XG59Il19
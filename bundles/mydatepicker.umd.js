(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/forms'), require('@angular/common'), require('angular2-text-mask')) :
    typeof define === 'function' && define.amd ? define('mydatepicker', ['exports', '@angular/core', '@angular/forms', '@angular/common', 'angular2-text-mask'], factory) :
    (global = global || self, factory(global.mydatepicker = {}, global.ng.core, global.ng.forms, global.ng.common, global.angular2TextMask));
}(this, function (exports, core, forms, common, angular2TextMask) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m) return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MydatepickerService = /** @class */ (function () {
        function MydatepickerService() {
        }
        MydatepickerService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        MydatepickerService.ctorParameters = function () { return []; };
        /** @nocollapse */ MydatepickerService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function MydatepickerService_Factory() { return new MydatepickerService(); }, token: MydatepickerService, providedIn: "root" });
        return MydatepickerService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var M = "m";
    /** @type {?} */
    var MM = "mm";
    /** @type {?} */
    var MMM = "mmm";
    /** @type {?} */
    var D = "d";
    /** @type {?} */
    var DD = "dd";
    /** @type {?} */
    var YYYY = "yyyy";
    var UtilService = /** @class */ (function () {
        function UtilService() {
            this.weekDays = ["su", "mo", "tu", "we", "th", "fr", "sa"];
        }
        /**
         * @param {?} dateStr
         * @param {?} dateFormat
         * @param {?} minYear
         * @param {?} maxYear
         * @param {?} disableUntil
         * @param {?} disableSince
         * @param {?} disableWeekends
         * @param {?} disableWeekDays
         * @param {?} disableDays
         * @param {?} disableDateRanges
         * @param {?} monthLabels
         * @param {?} enableDays
         * @return {?}
         */
        UtilService.prototype.isDateValid = /**
         * @param {?} dateStr
         * @param {?} dateFormat
         * @param {?} minYear
         * @param {?} maxYear
         * @param {?} disableUntil
         * @param {?} disableSince
         * @param {?} disableWeekends
         * @param {?} disableWeekDays
         * @param {?} disableDays
         * @param {?} disableDateRanges
         * @param {?} monthLabels
         * @param {?} enableDays
         * @return {?}
         */
        function (dateStr, dateFormat, minYear, maxYear, disableUntil, disableSince, disableWeekends, disableWeekDays, disableDays, disableDateRanges, monthLabels, enableDays) {
            /** @type {?} */
            var returnDate = { day: 0, month: 0, year: 0 };
            /** @type {?} */
            var daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            /** @type {?} */
            var isMonthStr = dateFormat.indexOf(MMM) !== -1;
            /** @type {?} */
            var delimeters = this.getDateFormatDelimeters(dateFormat);
            /** @type {?} */
            var dateValue = this.getDateValue(dateStr, dateFormat, delimeters);
            /** @type {?} */
            var year = this.getNumberByValue(dateValue[0]);
            /** @type {?} */
            var month = isMonthStr ? this.getMonthNumberByMonthName(dateValue[1], monthLabels) : this.getNumberByValue(dateValue[1]);
            /** @type {?} */
            var day = this.getNumberByValue(dateValue[2]);
            if (month !== -1 && day !== -1 && year !== -1) {
                if (year < minYear || year > maxYear || month < 1 || month > 12) {
                    return returnDate;
                }
                /** @type {?} */
                var date = { year: year, month: month, day: day };
                if (this.isDisabledDay(date, minYear, maxYear, disableUntil, disableSince, disableWeekends, disableWeekDays, disableDays, disableDateRanges, enableDays)) {
                    return returnDate;
                }
                if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
                    daysInMonth[1] = 29;
                }
                if (day < 1 || day > daysInMonth[month - 1]) {
                    return returnDate;
                }
                // Valid date
                return date;
            }
            return returnDate;
        };
        /**
         * @param {?} dateStr
         * @param {?} dateFormat
         * @param {?} delimeters
         * @return {?}
         */
        UtilService.prototype.getDateValue = /**
         * @param {?} dateStr
         * @param {?} dateFormat
         * @param {?} delimeters
         * @return {?}
         */
        function (dateStr, dateFormat, delimeters) {
            /** @type {?} */
            var del = delimeters[0];
            if (delimeters[0] !== delimeters[1]) {
                del = delimeters[0] + delimeters[1];
            }
            /** @type {?} */
            var re = new RegExp("[" + del + "]");
            /** @type {?} */
            var ds = dateStr.split(re);
            /** @type {?} */
            var df = dateFormat.split(re);
            /** @type {?} */
            var da = [];
            for (var i = 0; i < df.length; i++) {
                if (df[i].indexOf(YYYY) !== -1) {
                    da[0] = { value: ds[i], format: df[i] };
                }
                if (df[i].indexOf(M) !== -1) {
                    da[1] = { value: ds[i], format: df[i] };
                }
                if (df[i].indexOf(D) !== -1) {
                    da[2] = { value: ds[i], format: df[i] };
                }
            }
            return da;
        };
        /**
         * @param {?} df
         * @param {?} monthLabels
         * @return {?}
         */
        UtilService.prototype.getMonthNumberByMonthName = /**
         * @param {?} df
         * @param {?} monthLabels
         * @return {?}
         */
        function (df, monthLabels) {
            if (df.value) {
                for (var key = 1; key <= 12; key++) {
                    if (df.value.toLowerCase() === monthLabels[key].toLowerCase()) {
                        return key;
                    }
                }
            }
            return -1;
        };
        /**
         * @param {?} df
         * @return {?}
         */
        UtilService.prototype.getNumberByValue = /**
         * @param {?} df
         * @return {?}
         */
        function (df) {
            if (!/^\d+$/.test(df.value)) {
                return -1;
            }
            /** @type {?} */
            var nbr = Number(df.value);
            if (df.format.length === 1 && df.value.length !== 1 && nbr < 10 || df.format.length === 1 && df.value.length !== 2 && nbr >= 10) {
                nbr = -1;
            }
            else if (df.format.length === 2 && df.value.length > 2) {
                nbr = -1;
            }
            return nbr;
        };
        /**
         * @param {?} dateFormat
         * @return {?}
         */
        UtilService.prototype.getDateFormatDelimeters = /**
         * @param {?} dateFormat
         * @return {?}
         */
        function (dateFormat) {
            return dateFormat.match(/[^(dmy)]{1,}/g);
        };
        /**
         * @param {?} monthString
         * @return {?}
         */
        UtilService.prototype.parseDefaultMonth = /**
         * @param {?} monthString
         * @return {?}
         */
        function (monthString) {
            /** @type {?} */
            var month = { monthTxt: "", monthNbr: 0, year: 0 };
            if (monthString !== "") {
                /** @type {?} */
                var split = monthString.split(monthString.match(/[^0-9]/)[0]);
                month.monthNbr = split[0].length === 2 ? parseInt(split[0]) : parseInt(split[1]);
                month.year = split[0].length === 2 ? parseInt(split[1]) : parseInt(split[0]);
            }
            return month;
        };
        /**
         * @param {?} date
         * @param {?} dateFormat
         * @param {?} monthLabels
         * @return {?}
         */
        UtilService.prototype.formatDate = /**
         * @param {?} date
         * @param {?} dateFormat
         * @param {?} monthLabels
         * @return {?}
         */
        function (date, dateFormat, monthLabels) {
            /** @type {?} */
            var formatted = dateFormat.replace(YYYY, String(date.year));
            if (dateFormat.indexOf(MMM) !== -1) {
                formatted = formatted.replace(MMM, monthLabels[date.month]);
            }
            else if (dateFormat.indexOf(MM) !== -1) {
                formatted = formatted.replace(MM, this.preZero(date.month));
            }
            else {
                formatted = formatted.replace(M, String(date.month));
            }
            if (dateFormat.indexOf(DD) !== -1) {
                formatted = formatted.replace(DD, this.preZero(date.day));
            }
            else {
                formatted = formatted.replace(D, String(date.day));
            }
            return formatted;
        };
        /**
         * @param {?} val
         * @return {?}
         */
        UtilService.prototype.preZero = /**
         * @param {?} val
         * @return {?}
         */
        function (val) {
            return val < 10 ? "0" + val : String(val);
        };
        /**
         * @param {?} date
         * @param {?} minYear
         * @param {?} maxYear
         * @param {?} disableUntil
         * @param {?} disableSince
         * @param {?} disableWeekends
         * @param {?} disableWeekDays
         * @param {?} disableDays
         * @param {?} disableDateRanges
         * @param {?} enableDays
         * @return {?}
         */
        UtilService.prototype.isDisabledDay = /**
         * @param {?} date
         * @param {?} minYear
         * @param {?} maxYear
         * @param {?} disableUntil
         * @param {?} disableSince
         * @param {?} disableWeekends
         * @param {?} disableWeekDays
         * @param {?} disableDays
         * @param {?} disableDateRanges
         * @param {?} enableDays
         * @return {?}
         */
        function (date, minYear, maxYear, disableUntil, disableSince, disableWeekends, disableWeekDays, disableDays, disableDateRanges, enableDays) {
            var e_1, _a, e_2, _b, e_3, _c, e_4, _d;
            try {
                for (var enableDays_1 = __values(enableDays), enableDays_1_1 = enableDays_1.next(); !enableDays_1_1.done; enableDays_1_1 = enableDays_1.next()) {
                    var e = enableDays_1_1.value;
                    if (e.year === date.year && e.month === date.month && e.day === date.day) {
                        return false;
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (enableDays_1_1 && !enableDays_1_1.done && (_a = enableDays_1.return)) _a.call(enableDays_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            /** @type {?} */
            var dn = this.getDayNumber(date);
            if (date.year < minYear && date.month === 12 || date.year > maxYear && date.month === 1) {
                return true;
            }
            /** @type {?} */
            var dateMs = this.getTimeInMilliseconds(date);
            if (this.isInitializedDate(disableUntil) && dateMs <= this.getTimeInMilliseconds(disableUntil)) {
                return true;
            }
            if (this.isInitializedDate(disableSince) && dateMs >= this.getTimeInMilliseconds(disableSince)) {
                return true;
            }
            if (disableWeekends) {
                if (dn === 0 || dn === 6) {
                    return true;
                }
            }
            if (disableWeekDays.length > 0) {
                try {
                    for (var disableWeekDays_1 = __values(disableWeekDays), disableWeekDays_1_1 = disableWeekDays_1.next(); !disableWeekDays_1_1.done; disableWeekDays_1_1 = disableWeekDays_1.next()) {
                        var wd = disableWeekDays_1_1.value;
                        if (dn === this.getWeekdayIndex(wd)) {
                            return true;
                        }
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (disableWeekDays_1_1 && !disableWeekDays_1_1.done && (_b = disableWeekDays_1.return)) _b.call(disableWeekDays_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
            try {
                for (var disableDays_1 = __values(disableDays), disableDays_1_1 = disableDays_1.next(); !disableDays_1_1.done; disableDays_1_1 = disableDays_1.next()) {
                    var d = disableDays_1_1.value;
                    if (d.year === date.year && d.month === date.month && d.day === date.day) {
                        return true;
                    }
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (disableDays_1_1 && !disableDays_1_1.done && (_c = disableDays_1.return)) _c.call(disableDays_1);
                }
                finally { if (e_3) throw e_3.error; }
            }
            try {
                for (var disableDateRanges_1 = __values(disableDateRanges), disableDateRanges_1_1 = disableDateRanges_1.next(); !disableDateRanges_1_1.done; disableDateRanges_1_1 = disableDateRanges_1.next()) {
                    var d = disableDateRanges_1_1.value;
                    if (this.isInitializedDate(d.begin) && this.isInitializedDate(d.end) && dateMs >= this.getTimeInMilliseconds(d.begin) && dateMs <= this.getTimeInMilliseconds(d.end)) {
                        return true;
                    }
                }
            }
            catch (e_4_1) { e_4 = { error: e_4_1 }; }
            finally {
                try {
                    if (disableDateRanges_1_1 && !disableDateRanges_1_1.done && (_d = disableDateRanges_1.return)) _d.call(disableDateRanges_1);
                }
                finally { if (e_4) throw e_4.error; }
            }
            return false;
        };
        /**
         * @param {?} date
         * @param {?} markedDates
         * @param {?} markWeekends
         * @return {?}
         */
        UtilService.prototype.isMarkedDate = /**
         * @param {?} date
         * @param {?} markedDates
         * @param {?} markWeekends
         * @return {?}
         */
        function (date, markedDates, markWeekends) {
            var e_5, _a, e_6, _b;
            try {
                for (var markedDates_1 = __values(markedDates), markedDates_1_1 = markedDates_1.next(); !markedDates_1_1.done; markedDates_1_1 = markedDates_1.next()) {
                    var md = markedDates_1_1.value;
                    try {
                        for (var _c = __values(md.dates), _d = _c.next(); !_d.done; _d = _c.next()) {
                            var d = _d.value;
                            if (d.year === date.year && d.month === date.month && d.day === date.day) {
                                return { marked: true, color: md.color };
                            }
                        }
                    }
                    catch (e_6_1) { e_6 = { error: e_6_1 }; }
                    finally {
                        try {
                            if (_d && !_d.done && (_b = _c.return)) _b.call(_c);
                        }
                        finally { if (e_6) throw e_6.error; }
                    }
                }
            }
            catch (e_5_1) { e_5 = { error: e_5_1 }; }
            finally {
                try {
                    if (markedDates_1_1 && !markedDates_1_1.done && (_a = markedDates_1.return)) _a.call(markedDates_1);
                }
                finally { if (e_5) throw e_5.error; }
            }
            if (markWeekends && markWeekends.marked) {
                /** @type {?} */
                var dayNbr = this.getDayNumber(date);
                if (dayNbr === 0 || dayNbr === 6) {
                    return { marked: true, color: markWeekends.color };
                }
            }
            return { marked: false, color: "" };
        };
        /**
         * @param {?} date
         * @param {?} sunHighlight
         * @param {?} satHighlight
         * @param {?} highlightDates
         * @return {?}
         */
        UtilService.prototype.isHighlightedDate = /**
         * @param {?} date
         * @param {?} sunHighlight
         * @param {?} satHighlight
         * @param {?} highlightDates
         * @return {?}
         */
        function (date, sunHighlight, satHighlight, highlightDates) {
            var e_7, _a;
            /** @type {?} */
            var dayNbr = this.getDayNumber(date);
            if (sunHighlight && dayNbr === 0 || satHighlight && dayNbr === 6) {
                return true;
            }
            try {
                for (var highlightDates_1 = __values(highlightDates), highlightDates_1_1 = highlightDates_1.next(); !highlightDates_1_1.done; highlightDates_1_1 = highlightDates_1.next()) {
                    var d = highlightDates_1_1.value;
                    if (d.year === date.year && d.month === date.month && d.day === date.day) {
                        return true;
                    }
                }
            }
            catch (e_7_1) { e_7 = { error: e_7_1 }; }
            finally {
                try {
                    if (highlightDates_1_1 && !highlightDates_1_1.done && (_a = highlightDates_1.return)) _a.call(highlightDates_1);
                }
                finally { if (e_7) throw e_7.error; }
            }
            return false;
        };
        /**
         * @param {?} date
         * @return {?}
         */
        UtilService.prototype.getWeekNumber = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var d = new Date(date.year, date.month - 1, date.day, 0, 0, 0, 0);
            d.setDate(d.getDate() + (d.getDay() === 0 ? -3 : 4 - d.getDay()));
            return Math.round(((d.getTime() - new Date(d.getFullYear(), 0, 4).getTime()) / 86400000) / 7) + 1;
        };
        /**
         * @param {?} date
         * @param {?} disableUntil
         * @return {?}
         */
        UtilService.prototype.isMonthDisabledByDisableUntil = /**
         * @param {?} date
         * @param {?} disableUntil
         * @return {?}
         */
        function (date, disableUntil) {
            return this.isInitializedDate(disableUntil) && this.getTimeInMilliseconds(date) <= this.getTimeInMilliseconds(disableUntil);
        };
        /**
         * @param {?} date
         * @param {?} disableSince
         * @return {?}
         */
        UtilService.prototype.isMonthDisabledByDisableSince = /**
         * @param {?} date
         * @param {?} disableSince
         * @return {?}
         */
        function (date, disableSince) {
            return this.isInitializedDate(disableSince) && this.getTimeInMilliseconds(date) >= this.getTimeInMilliseconds(disableSince);
        };
        /**
         * @param {?} date
         * @return {?}
         */
        UtilService.prototype.isInitializedDate = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return date.year !== 0 && date.month !== 0 && date.day !== 0;
        };
        /**
         * @param {?} d1
         * @param {?} d2
         * @return {?}
         */
        UtilService.prototype.isSameDate = /**
         * @param {?} d1
         * @param {?} d2
         * @return {?}
         */
        function (d1, d2) {
            return d1.year === d2.year && d1.month === d2.month && d1.day === d2.day;
        };
        /**
         * @param {?} date
         * @return {?}
         */
        UtilService.prototype.getTimeInMilliseconds = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return new Date(date.year, date.month - 1, date.day, 0, 0, 0, 0).getTime();
        };
        /**
         * @param {?} date
         * @return {?}
         */
        UtilService.prototype.getDayNumber = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return new Date(date.year, date.month - 1, date.day, 0, 0, 0, 0).getDay();
        };
        /**
         * @return {?}
         */
        UtilService.prototype.getWeekDays = /**
         * @return {?}
         */
        function () {
            return this.weekDays;
        };
        /**
         * @param {?} wd
         * @return {?}
         */
        UtilService.prototype.getWeekdayIndex = /**
         * @param {?} wd
         * @return {?}
         */
        function (wd) {
            return this.weekDays.indexOf(wd);
        };
        UtilService.decorators = [
            { type: core.Injectable }
        ];
        return UtilService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LocaleService = /** @class */ (function () {
        function LocaleService() {
            this.locales = {
                "en": {
                    dayLabels: { su: "Sun", mo: "Mon", tu: "Tue", we: "Wed", th: "Thu", fr: "Fri", sa: "Sat" },
                    monthLabels: { 1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "May", 6: "Jun", 7: "Jul", 8: "Aug", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dec" },
                    dateFormat: "mm/dd/yyyy",
                    todayBtnTxt: "Today",
                    firstDayOfWeek: "mo",
                    sunHighlight: true,
                },
                "he": {
                    dayLabels: { su: "רא", mo: "שנ", tu: "של", we: "רב", th: "חמ", fr: "שי", sa: "שב" },
                    monthLabels: { 1: "ינו", 2: "פבר", 3: "מרץ", 4: "אפר", 5: "מאי", 6: "יונ", 7: "יול", 8: "אוג", 9: "ספט", 10: "אוק", 11: "נוב", 12: "דצמ" },
                    dateFormat: "dd/mm/yyyy",
                    todayBtnTxt: "היום",
                    firstDayOfWeek: "su",
                    sunHighlight: false
                },
                "ja": {
                    dayLabels: { su: "日", mo: "月", tu: "火", we: "水", th: "木", fr: "金", sa: "土" },
                    monthLabels: { 1: "１月", 2: "２月", 3: "３月", 4: "４月", 5: "５月", 6: "６月", 7: "７月", 8: "８月", 9: "９月", 10: "１０月", 11: "１１月", 12: "１２月" },
                    dateFormat: "yyyy.mm.dd",
                    todayBtnTxt: "今日",
                    sunHighlight: false
                },
                "fr": {
                    dayLabels: { su: "Dim", mo: "Lun", tu: "Mar", we: "Mer", th: "Jeu", fr: "Ven", sa: "Sam" },
                    monthLabels: { 1: "Jan", 2: "Fév", 3: "Mar", 4: "Avr", 5: "Mai", 6: "Juin", 7: "Juil", 8: "Aoû", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Déc" },
                    dateFormat: "dd/mm/yyyy",
                    todayBtnTxt: "Aujourd'hui",
                    firstDayOfWeek: "mo",
                    sunHighlight: true,
                },
                "fr-ch": {
                    dayLabels: { su: "Dim", mo: "Lun", tu: "Mar", we: "Mer", th: "Jeu", fr: "Ven", sa: "Sam" },
                    monthLabels: { 1: "Jan", 2: "Fév", 3: "Mar", 4: "Avr", 5: "Mai", 6: "Juin", 7: "Juil", 8: "Aoû", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Déc" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Aujourd'hui",
                    firstDayOfWeek: "mo",
                    sunHighlight: true,
                },
                "fi": {
                    dayLabels: { su: "Su", mo: "Ma", tu: "Ti", we: "Ke", th: "To", fr: "Pe", sa: "La" },
                    monthLabels: { 1: "Tam", 2: "Hel", 3: "Maa", 4: "Huh", 5: "Tou", 6: "Kes", 7: "Hei", 8: "Elo", 9: "Syy", 10: "Lok", 11: "Mar", 12: "Jou" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Tänään",
                    firstDayOfWeek: "mo",
                    sunHighlight: true,
                },
                "es": {
                    dayLabels: { su: "Do", mo: "Lu", tu: "Ma", we: "Mi", th: "Ju", fr: "Vi", sa: "Sa" },
                    monthLabels: { 1: "Ene", 2: "Feb", 3: "Mar", 4: "Abr", 5: "May", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dic" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Hoy",
                    firstDayOfWeek: "mo",
                    sunHighlight: true,
                },
                "hu": {
                    dayLabels: { su: "Vas", mo: "Hét", tu: "Kedd", we: "Sze", th: "Csü", fr: "Pén", sa: "Szo" },
                    monthLabels: { 1: "Jan", 2: "Feb", 3: "Már", 4: "Ápr", 5: "Máj", 6: "Jún", 7: "Júl", 8: "Aug", 9: "Szep", 10: "Okt", 11: "Nov", 12: "Dec" },
                    dateFormat: "yyyy-mm-dd",
                    todayBtnTxt: "Ma",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "sv": {
                    dayLabels: { su: "Sön", mo: "Mån", tu: "Tis", we: "Ons", th: "Tor", fr: "Fre", sa: "Lör" },
                    monthLabels: { 1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "Maj", 6: "Jun", 7: "Jul", 8: "Aug", 9: "Sep", 10: "Okt", 11: "Nov", 12: "Dec" },
                    dateFormat: "yyyy-mm-dd",
                    todayBtnTxt: "Idag",
                    firstDayOfWeek: "mo",
                    sunHighlight: false
                },
                "nl": {
                    dayLabels: { su: "Zon", mo: "Maa", tu: "Din", we: "Woe", th: "Don", fr: "Vri", sa: "Zat" },
                    monthLabels: { 1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "Mei", 6: "Jun", 7: "Jul", 8: "Aug", 9: "Sep", 10: "Okt", 11: "Nov", 12: "Dec" },
                    dateFormat: "dd-mm-yyyy",
                    todayBtnTxt: "Vandaag",
                    firstDayOfWeek: "mo",
                    sunHighlight: false
                },
                "ru": {
                    dayLabels: { su: "Вс", mo: "Пн", tu: "Вт", we: "Ср", th: "Чт", fr: "Пт", sa: "Сб" },
                    monthLabels: { 1: "Янв", 2: "Фев", 3: "Март", 4: "Апр", 5: "Май", 6: "Июнь", 7: "Июль", 8: "Авг", 9: "Сент", 10: "Окт", 11: "Ноя", 12: "Дек" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Сегодня",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "uk": {
                    dayLabels: { su: "Нд", mo: "Пн", tu: "Вт", we: "Ср", th: "Чт", fr: "Пт", sa: "Сб" },
                    monthLabels: { 1: "Січ", 2: "Лют", 3: "Бер", 4: "Кві", 5: "Тра", 6: "Чер", 7: "Лип", 8: "Сер", 9: "Вер", 10: "Жов", 11: "Лис", 12: "Гру" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Сьогодні",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "no": {
                    dayLabels: { su: "Søn", mo: "Man", tu: "Tir", we: "Ons", th: "Tor", fr: "Fre", sa: "Lør" },
                    monthLabels: { 1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "Mai", 6: "Jun", 7: "Jul", 8: "Aug", 9: "Sep", 10: "Okt", 11: "Nov", 12: "Des" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "I dag",
                    firstDayOfWeek: "mo",
                    sunHighlight: false
                },
                "tr": {
                    dayLabels: { su: "Paz", mo: "Pzt", tu: "Sal", we: "Çar", th: "Per", fr: "Cum", sa: "Cmt" },
                    monthLabels: { 1: "Oca", 2: "Şub", 3: "Mar", 4: "Nis", 5: "May", 6: "Haz", 7: "Tem", 8: "Ağu", 9: "Eyl", 10: "Eki", 11: "Kas", 12: "Ara" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Bugün",
                    firstDayOfWeek: "mo",
                    sunHighlight: false
                },
                "pt-br": {
                    dayLabels: { su: "Dom", mo: "Seg", tu: "Ter", we: "Qua", th: "Qui", fr: "Sex", sa: "Sab" },
                    monthLabels: { 1: "Jan", 2: "Fev", 3: "Mar", 4: "Abr", 5: "Mai", 6: "Jun", 7: "Jul", 8: "Ago", 9: "Set", 10: "Out", 11: "Nov", 12: "Dez" },
                    dateFormat: "dd/mm/yyyy",
                    todayBtnTxt: "Hoje",
                    firstDayOfWeek: "su",
                    sunHighlight: true
                },
                "de": {
                    dayLabels: { su: "So", mo: "Mo", tu: "Di", we: "Mi", th: "Do", fr: "Fr", sa: "Sa" },
                    monthLabels: { 1: "Jan", 2: "Feb", 3: "Mär", 4: "Apr", 5: "Mai", 6: "Jun", 7: "Jul", 8: "Aug", 9: "Sep", 10: "Okt", 11: "Nov", 12: "Dez" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Heute",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "de-ch": {
                    dayLabels: { su: "So", mo: "Mo", tu: "Di", we: "Mi", th: "Do", fr: "Fr", sa: "Sa" },
                    monthLabels: { 1: "Jan", 2: "Feb", 3: "Mär", 4: "Apr", 5: "Mai", 6: "Jun", 7: "Jul", 8: "Aug", 9: "Sep", 10: "Okt", 11: "Nov", 12: "Dez" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Heute",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "it": {
                    dayLabels: { su: "Dom", mo: "Lun", tu: "Mar", we: "Mer", th: "Gio", fr: "Ven", sa: "Sab" },
                    monthLabels: { 1: "Gen", 2: "Feb", 3: "Mar", 4: "Apr", 5: "Mag", 6: "Giu", 7: "Lug", 8: "Ago", 9: "Set", 10: "Ott", 11: "Nov", 12: "Dic" },
                    dateFormat: "dd/mm/yyyy",
                    todayBtnTxt: "Oggi",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "it-ch": {
                    dayLabels: { su: "Dom", mo: "Lun", tu: "Mar", we: "Mer", th: "Gio", fr: "Ven", sa: "Sab" },
                    monthLabels: { 1: "Gen", 2: "Feb", 3: "Mar", 4: "Apr", 5: "Mag", 6: "Giu", 7: "Lug", 8: "Ago", 9: "Set", 10: "Ott", 11: "Nov", 12: "Dic" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Oggi",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "pl": {
                    dayLabels: { su: "Nie", mo: "Pon", tu: "Wto", we: "Śro", th: "Czw", fr: "Pią", sa: "Sob" },
                    monthLabels: { 1: "Sty", 2: "Lut", 3: "Mar", 4: "Kwi", 5: "Maj", 6: "Cze", 7: "Lip", 8: "Sie", 9: "Wrz", 10: "Paź", 11: "Lis", 12: "Gru" },
                    dateFormat: "yyyy-mm-dd",
                    todayBtnTxt: "Dzisiaj",
                    firstDayOfWeek: "mo",
                    sunHighlight: true,
                },
                "my": {
                    dayLabels: { su: "တနင်္ဂနွေ", mo: "တနင်္လာ", tu: "အင်္ဂါ", we: "ဗုဒ္ဓဟူး", th: "ကြသပတေး", fr: "သောကြာ", sa: "စနေ" },
                    monthLabels: { 1: "ဇန်နဝါရီ", 2: "ဖေဖော်ဝါရီ", 3: "မတ်", 4: "ဧပြီ", 5: "မေ", 6: "ဇွန်", 7: "ဇူလိုင်", 8: "ဩဂုတ်", 9: "စက်တင်ဘာ", 10: "အောက်တိုဘာ", 11: "နိုဝင်ဘာ", 12: "ဒီဇင်ဘာ" },
                    dateFormat: "yyyy-mm-dd",
                    todayBtnTxt: "ယနေ့",
                    firstDayOfWeek: "mo",
                    sunHighlight: true,
                },
                "sk": {
                    dayLabels: { su: "Ne", mo: "Po", tu: "Ut", we: "St", th: "Št", fr: "Pi", sa: "So" },
                    monthLabels: { 1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "Máj", 6: "Jún", 7: "Júl", 8: "Aug", 9: "Sep", 10: "Okt", 11: "Nov", 12: "Dec" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Dnes",
                    firstDayOfWeek: "mo",
                    sunHighlight: true,
                },
                "sl": {
                    dayLabels: { su: "Ned", mo: "Pon", tu: "Tor", we: "Sre", th: "Čet", fr: "Pet", sa: "Sob" },
                    monthLabels: { 1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "Maj", 6: "Jun", 7: "Jul", 8: "Avg", 9: "Sep", 10: "Okt", 11: "Nov", 12: "Dec" },
                    dateFormat: "dd. mm. yyyy",
                    todayBtnTxt: "Danes",
                    firstDayOfWeek: "mo",
                    sunHighlight: true,
                },
                "zh-cn": {
                    dayLabels: { su: "日", mo: "一", tu: "二", we: "三", th: "四", fr: "五", sa: "六" },
                    monthLabels: { 1: "1月", 2: "2月", 3: "3月", 4: "4月", 5: "5月", 6: "6月", 7: "7月", 8: "8月", 9: "9月", 10: "10月", 11: "11月", 12: "12月" },
                    dateFormat: "yyyy-mm-dd",
                    todayBtnTxt: "今天",
                    firstDayOfWeek: "mo",
                    sunHighlight: true,
                },
                "ro": {
                    dayLabels: { su: "du", mo: "lu", tu: "ma", we: "mi", th: "jo", fr: "vi", sa: "sa" },
                    monthLabels: { 1: "ian", 2: "feb", 3: "mart", 4: "apr", 5: "mai", 6: "iun", 7: "iul", 8: "aug", 9: "sept", 10: "oct", 11: "nov", 12: "dec" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Astăzi",
                    firstDayOfWeek: "mo",
                    sunHighlight: true,
                },
                "ca": {
                    dayLabels: { su: "dg", mo: "dl", tu: "dt", we: "dc", th: "dj", fr: "dv", sa: "ds" },
                    monthLabels: { 1: "Gen", 2: "Febr", 3: "Març", 4: "Abr", 5: "Maig", 6: "Juny", 7: "Jul", 8: "Ag", 9: "Set", 10: "Oct", 11: "Nov", 12: "Des" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Avui",
                    firstDayOfWeek: "mo",
                    sunHighlight: true,
                },
                "id": {
                    dayLabels: { su: "Min", mo: "Sen", tu: "Sel", we: "Rab", th: "Kam", fr: "Jum", sa: "Sab" },
                    monthLabels: { 1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "Mei", 6: "Jun", 7: "Jul", 8: "Ags", 9: "Sep", 10: "Okt", 11: "Nov", 12: "Des" },
                    dateFormat: "dd-mm-yyyy",
                    todayBtnTxt: "Hari ini",
                    firstDayOfWeek: "su",
                    sunHighlight: true
                },
                "en-au": {
                    dayLabels: { su: "Sun", mo: "Mon", tu: "Tue", we: "Wed", th: "Thu", fr: "Fri", sa: "Sat" },
                    monthLabels: { 1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "May", 6: "Jun", 7: "Jul", 8: "Aug", 9: "Sep", 10: "Oct", 11: "Nov", 12: "Dec" },
                    dateFormat: "dd/mm/yyyy",
                    todayBtnTxt: "Today",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "am-et": {
                    dayLabels: { su: "እሑድ", mo: "ሰኞ", tu: "ማክሰኞ", we: "ረቡዕ", th: "ሐሙስ", fr: "ዓርብ", sa: "ቅዳሜ" },
                    monthLabels: { 1: "ጃንዩ", 2: "ፌብሩ", 3: "ማርች", 4: "ኤፕረ", 5: "ሜይ", 6: "ጁን", 7: "ጁላይ", 8: "ኦገስ", 9: "ሴፕቴ", 10: "ኦክተ", 11: "ኖቬም", 12: "ዲሴም" },
                    dateFormat: "yyyy-mm-dd",
                    todayBtnTxt: "ዛሬ",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "cs": {
                    dayLabels: { su: "Ne", mo: "Po", tu: "Út", we: "St", th: "Čt", fr: "Pá", sa: "So" },
                    monthLabels: { 1: "Led", 2: "Úno", 3: "Bře", 4: "Dub", 5: "Kvě", 6: "Čvn", 7: "Čvc", 8: "Srp", 9: "Zář", 10: "Říj", 11: "Lis", 12: "Pro" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Dnes",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "el": {
                    dayLabels: { su: "Κυρ", mo: "Δευ", tu: "Τρι", we: "Τετ", th: "Πεμ", fr: "Παρ", sa: "Σαβ" },
                    monthLabels: { 1: "Ιαν", 2: "Φεβ", 3: "Μαρ", 4: "Απρ", 5: "Μαι", 6: "Ιουν", 7: "Ιουλ", 8: "Αυγ", 9: "Σεπ", 10: "Οκτ", 11: "Νοε", 12: "Δεκ" },
                    dateFormat: "dd/mm/yyyy",
                    todayBtnTxt: "Σήμερα",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "kk": {
                    dayLabels: { su: "Жк", mo: "Дс", tu: "Сс", we: "Ср", th: "Бс", fr: "Жм", sa: "Сб" },
                    monthLabels: { 1: "Қаң", 2: "Ақп", 3: "Нау", 4: "Сәу", 5: "Мам", 6: "Мау", 7: "Шіл", 8: "Там", 9: "Қырк", 10: "Қаз", 11: "Қар", 12: "Желт" },
                    dateFormat: "dd-mmm-yyyy",
                    todayBtnTxt: "Бүгін",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "th": {
                    dayLabels: { su: "อา", mo: "จ", tu: "อ", we: "พ", th: "พฤ", fr: "ศ", sa: "ส" },
                    monthLabels: { 1: "ม.ค", 2: "ก.พ.", 3: "มี.ค.", 4: "เม.ย.", 5: "พ.ค.", 6: "มิ.ย.", 7: "ก.ค.", 8: "ส.ค.", 9: "ก.ย.", 10: "ต.ค.", 11: "พ.ย.", 12: "ธ.ค." },
                    dateFormat: "dd-mm-yyyy",
                    todayBtnTxt: "วันนี้",
                    firstDayOfWeek: "su",
                    sunHighlight: true
                },
                "ko-kr": {
                    dayLabels: { su: "일", mo: "월", tu: "화", we: "수", th: "목", fr: "금", sa: "토" },
                    monthLabels: { 1: "1월", 2: "2월", 3: "3월", 4: "4월", 5: "5월", 6: "6월", 7: "7월", 8: "8월", 9: "9월", 10: "10월", 11: "11월", 12: "12월" },
                    dateFormat: "yyyy mm dd",
                    todayBtnTxt: "오늘",
                    firstDayOfWeek: "su",
                    sunHighlight: true
                },
                "da": {
                    dayLabels: { su: "Søn", mo: "Man", tu: "Tir", we: "Ons", th: "Tor", fr: "Fre", sa: "Lør" },
                    monthLabels: { 1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "Maj", 6: "Jun", 7: "Jul", 8: "Aug", 9: "Sep", 10: "Okt", 11: "Nov", 12: "Dec" },
                    dateFormat: "dd-mm-yyyy",
                    todayBtnTxt: "I dag",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "lt": {
                    dayLabels: { su: "Sk", mo: "Pr", tu: "An", we: "Tr", th: "Kt", fr: "Pn", sa: "Št" },
                    monthLabels: { 1: "Saus.", 2: "Vas.", 3: "Kov.", 4: "Bal.", 5: "Geg.", 6: "Birž.", 7: "Liep.", 8: "Rugp.", 9: "Rugs.", 10: "Sapl.", 11: "Lapkr.", 12: "Gruod." },
                    dateFormat: "yyyy-mm-dd",
                    todayBtnTxt: "Šianien",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "vi": {
                    dayLabels: { su: "CN", mo: "T2", tu: "T3", we: "T4", th: "T5", fr: "T6", sa: "T7" },
                    monthLabels: { 1: "THG 1", 2: "THG 2", 3: "THG 3", 4: "THG 4", 5: "THG 5", 6: "THG 6", 7: "THG 7", 8: "THG 8", 9: "THG 9", 10: "THG 10", 11: "THG 11", 12: "THG 12" },
                    dateFormat: "dd/mm/yyyy",
                    todayBtnTxt: "Hôm nay",
                    firstDayOfWeek: "mo",
                    sunHighlight: true,
                },
                "bn": {
                    dayLabels: { su: "রবি", mo: "সোম", tu: "মঙ্গল", we: "বুধ", th: "বৃহঃ", fr: "শুক্র", sa: "শনি" },
                    monthLabels: { 1: "জানু", 2: "ফেব্রু", 3: "মার্চ", 4: "এপ্রিল", 5: "মে", 6: "জুন", 7: "জুলাই", 8: "আগস্ট", 9: "সেপ্টে", 10: "অক্টো", 11: "নভে", 12: "ডিসে" },
                    dateFormat: "dd-mm-yyyy",
                    todayBtnTxt: "আজ",
                    firstDayOfWeek: "su",
                    sunHighlight: true
                },
                "bg": {
                    dayLabels: { su: "нд", mo: "пн", tu: "вт", we: "ср", th: "чт", fr: "пт", sa: "сб" },
                    monthLabels: { 1: "яну.", 2: "фев.", 3: "март", 4: "апр.", 5: "май", 6: "юни", 7: "юли", 8: "авг.", 9: "сеп.", 10: "окт.", 11: "ное.", 12: "дек." },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "днес",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "hr": {
                    dayLabels: { su: "Ne", mo: "Po", tu: "Ul", we: "Sr", th: "Če", fr: "Pe", sa: "Su" },
                    monthLabels: { 1: "Sij", 2: "Vel", 3: "Ožu", 4: "Tra", 5: "Svi", 6: "Lip", 7: "Srp", 8: "Kol", 9: "Ruj", 10: "Lis", 11: "Stu", 12: "Pro" },
                    dateFormat: "dd.mm.yyyy.",
                    todayBtnTxt: "danas",
                    firstDayOfWeek: "su",
                    sunHighlight: true
                },
                "ar": {
                    dayLabels: { su: "الأحد", mo: "الاثنين", tu: "الثلاثاء", we: "الاربعاء", th: "الخميس", fr: "الجمعة", sa: "السبت" },
                    monthLabels: { 1: "يناير", 2: "فبراير", 3: "مارس", 4: "ابريل", 5: "مايو", 6: "يونيو", 7: "يوليو", 8: "أغسطس", 9: "سبتمبر", 10: "أكتوبر", 11: "نوفمبر", 12: "ديسمبر" },
                    dateFormat: "yyyy-mm-dd",
                    todayBtnTxt: "اليوم",
                    firstDayOfWeek: "sa",
                    sunHighlight: true
                },
                "is": {
                    dayLabels: { su: "sun", mo: "mán", tu: "þri", we: "mið", th: "fim", fr: "fös", sa: "lau" },
                    monthLabels: { 1: "jan", 2: "feb", 3: "mar", 4: "apr", 5: "maí", 6: "jún", 7: "júl", 8: "ágú", 9: "sep", 10: "okt", 11: "nóv", 12: "des" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Í dag",
                    firstDayOfWeek: "su",
                    sunHighlight: true
                },
                "tw": {
                    dayLabels: { su: "週日", mo: "週一", tu: "週二", we: "週三", th: "週四", fr: "週五", sa: "週六" },
                    monthLabels: { 1: "一月", 2: "二月", 3: "三月", 4: "四月", 5: "五月", 6: "六月", 7: "七月", 8: "八月", 9: "九月", 10: "十月", 11: "十一月", 12: "十二月" },
                    dateFormat: "yyyy-mm-dd",
                    todayBtnTxt: "今天",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "lv": {
                    dayLabels: { su: "S", mo: "P", tu: "O", we: "T", th: "C", fr: "P", sa: "S" },
                    monthLabels: { 1: "Janv", 2: "Febr", 3: "Marts", 4: "Apr", 5: "Maijs", 6: "Jūn", 7: "Jūl", 8: "Aug", 9: "Sept", 10: "Okt", 11: "Nov", 12: "Dec" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Šodien",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                },
                "et": {
                    dayLabels: { su: "P", mo: "E", tu: "T", we: "K", th: "N", fr: "R", sa: "L" },
                    monthLabels: { 1: "Jaan", 2: "Veebr", 3: "Märts", 4: "Apr", 5: "Mai", 6: "Juuni", 7: "Juuli", 8: "Aug", 9: "Sept", 10: "Okt", 11: "Nov", 12: "Dets" },
                    dateFormat: "dd.mm.yyyy",
                    todayBtnTxt: "Täna",
                    firstDayOfWeek: "mo",
                    sunHighlight: true
                }
            };
        }
        /**
         * @param {?} locale
         * @return {?}
         */
        LocaleService.prototype.getLocaleOptions = /**
         * @param {?} locale
         * @return {?}
         */
        function (locale) {
            if (locale && this.locales.hasOwnProperty(locale)) {
                // User given locale
                return this.locales[locale];
            }
            // Default: en
            return this.locales["en"];
        };
        LocaleService.decorators = [
            { type: core.Injectable }
        ];
        return LocaleService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    // webpack1_
    // declare var require: any;
    // const myDpStyles: string = require("./my-date-picker.component.css");
    // const myDpTpl: string = require("./my-date-picker.component.html");
    // webpack2_
    /** @type {?} */
    var MYDP_VALUE_ACCESSOR = {
        provide: forms.NG_VALUE_ACCESSOR,
        useExisting: core.forwardRef((/**
         * @return {?}
         */
        function () { return MyDatePicker; })),
        multi: true
    };
    /** @enum {number} */
    var CalToggle = {
        Open: 1, CloseByDateSel: 2, CloseByCalBtn: 3, CloseByOutClick: 4, CloseByEsc: 5, CloseByApi: 6,
    };
    CalToggle[CalToggle.Open] = 'Open';
    CalToggle[CalToggle.CloseByDateSel] = 'CloseByDateSel';
    CalToggle[CalToggle.CloseByCalBtn] = 'CloseByCalBtn';
    CalToggle[CalToggle.CloseByOutClick] = 'CloseByOutClick';
    CalToggle[CalToggle.CloseByEsc] = 'CloseByEsc';
    CalToggle[CalToggle.CloseByApi] = 'CloseByApi';
    /** @enum {number} */
    var Year = {
        min: 1000, max: 9999,
    };
    Year[Year.min] = 'min';
    Year[Year.max] = 'max';
    /** @enum {number} */
    var InputFocusBlur = {
        focus: 1, blur: 2,
    };
    InputFocusBlur[InputFocusBlur.focus] = 'focus';
    InputFocusBlur[InputFocusBlur.blur] = 'blur';
    /** @enum {number} */
    var KeyCode = {
        enter: 13, esc: 27, space: 32,
    };
    KeyCode[KeyCode.enter] = 'enter';
    KeyCode[KeyCode.esc] = 'esc';
    KeyCode[KeyCode.space] = 'space';
    /** @enum {number} */
    var MonthId = {
        prev: 1, curr: 2, next: 3,
    };
    MonthId[MonthId.prev] = 'prev';
    MonthId[MonthId.curr] = 'curr';
    MonthId[MonthId.next] = 'next';
    /** @type {?} */
    var MMM$1 = "mmm";
    var MyDatePicker = /** @class */ (function () {
        function MyDatePicker(elem, renderer, cdr, localeService, utilService) {
            this.elem = elem;
            this.renderer = renderer;
            this.cdr = cdr;
            this.localeService = localeService;
            this.utilService = utilService;
            this.dateChanged = new core.EventEmitter();
            this.inputFieldChanged = new core.EventEmitter();
            this.calendarViewChanged = new core.EventEmitter();
            this.calendarToggle = new core.EventEmitter();
            this.inputFocusBlur = new core.EventEmitter();
            this.onChangeCb = (/**
             * @return {?}
             */
            function () { });
            this.onTouchedCb = (/**
             * @return {?}
             */
            function () { });
            this.showSelector = false;
            this.visibleMonth = { monthTxt: "", monthNbr: 0, year: 0 };
            this.selectedMonth = { monthTxt: "", monthNbr: 0, year: 0 };
            this.selectedDate = { year: 0, month: 0, day: 0 };
            this.weekDays = [];
            this.dates = [];
            this.months = [];
            this.years = [];
            this.selectionDayTxt = "";
            this.invalidDate = false;
            this.disableTodayBtn = false;
            this.dayIdx = 0;
            this.selectMonth = false;
            this.selectYear = false;
            this.prevMonthDisabled = false;
            this.nextMonthDisabled = false;
            this.prevYearDisabled = false;
            this.nextYearDisabled = false;
            this.prevYearsDisabled = false;
            this.nextYearsDisabled = false;
            this.prevMonthId = MonthId.prev;
            this.currMonthId = MonthId.curr;
            this.nextMonthId = MonthId.next;
            // Default options
            this.opts = {
                dayLabels: (/** @type {?} */ ({})),
                monthLabels: (/** @type {?} */ ({})),
                dateFormat: (/** @type {?} */ ("")),
                showTodayBtn: (/** @type {?} */ (true)),
                todayBtnTxt: (/** @type {?} */ ("")),
                firstDayOfWeek: (/** @type {?} */ ("")),
                satHighlight: (/** @type {?} */ (false)),
                sunHighlight: (/** @type {?} */ (true)),
                highlightDates: (/** @type {?} */ ([])),
                markCurrentDay: (/** @type {?} */ (true)),
                markCurrentMonth: (/** @type {?} */ (true)),
                markCurrentYear: (/** @type {?} */ (true)),
                disableUntil: (/** @type {?} */ ({ year: 0, month: 0, day: 0 })),
                disableSince: (/** @type {?} */ ({ year: 0, month: 0, day: 0 })),
                disableDays: (/** @type {?} */ ([])),
                enableDays: (/** @type {?} */ ([])),
                markDates: (/** @type {?} */ ([])),
                markWeekends: (/** @type {?} */ ({})),
                disableDateRanges: (/** @type {?} */ ([])),
                disableWeekends: (/** @type {?} */ (false)),
                disableWeekdays: (/** @type {?} */ ([])),
                showWeekNumbers: (/** @type {?} */ (false)),
                height: (/** @type {?} */ ("34px")),
                width: (/** @type {?} */ ("100%")),
                selectionTxtFontSize: (/** @type {?} */ ("14px")),
                selectorHeight: (/** @type {?} */ ("232px")),
                selectorWidth: (/** @type {?} */ ("252px")),
                allowDeselectDate: (/** @type {?} */ (false)),
                inline: (/** @type {?} */ (false)),
                showClearDateBtn: (/** @type {?} */ (true)),
                showDecreaseDateBtn: (/** @type {?} */ (false)),
                showIncreaseDateBtn: (/** @type {?} */ (false)),
                alignSelectorRight: (/** @type {?} */ (false)),
                openSelectorTopOfInput: (/** @type {?} */ (false)),
                indicateInvalidDate: (/** @type {?} */ (true)),
                editableDateField: (/** @type {?} */ (true)),
                monthSelector: (/** @type {?} */ (true)),
                yearSelector: (/** @type {?} */ (true)),
                disableHeaderButtons: (/** @type {?} */ (true)),
                minYear: (/** @type {?} */ (Year.min)),
                maxYear: (/** @type {?} */ (Year.max)),
                componentDisabled: (/** @type {?} */ (false)),
                showSelectorArrow: (/** @type {?} */ (true)),
                showInputField: (/** @type {?} */ (true)),
                openSelectorOnInputClick: (/** @type {?} */ (false)),
                allowSelectionOnlyInCurrentMonth: (/** @type {?} */ (true)),
                ariaLabelInputField: (/** @type {?} */ ("Date input field")),
                ariaLabelClearDate: (/** @type {?} */ ("Clear Date")),
                ariaLabelDecreaseDate: (/** @type {?} */ ("Decrease Date")),
                ariaLabelIncreaseDate: (/** @type {?} */ ("Increase Date")),
                ariaLabelOpenCalendar: (/** @type {?} */ ("Open Calendar")),
                ariaLabelPrevMonth: (/** @type {?} */ ("Previous Month")),
                ariaLabelNextMonth: (/** @type {?} */ ("Next Month")),
                ariaLabelPrevYear: (/** @type {?} */ ("Previous Year")),
                ariaLabelNextYear: (/** @type {?} */ ("Next Year")),
                dateMaskRegex: (/** @type {?} */ ([])),
            };
            this.setLocaleOptions();
        }
        /**
         * @return {?}
         */
        MyDatePicker.prototype.setLocaleOptions = /**
         * @return {?}
         */
        function () {
            var _this = this;
            /** @type {?} */
            var opts = this.localeService.getLocaleOptions(this.locale);
            Object.keys(opts).forEach((/**
             * @param {?} k
             * @return {?}
             */
            function (k) {
                ((/** @type {?} */ (_this.opts)))[k] = opts[k];
            }));
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.setOptions = /**
         * @return {?}
         */
        function () {
            var _this = this;
            if (this.options !== undefined) {
                Object.keys(this.options).forEach((/**
                 * @param {?} k
                 * @return {?}
                 */
                function (k) {
                    ((/** @type {?} */ (_this.opts)))[k] = _this.options[k];
                }));
            }
            if (this.opts.minYear < Year.min) {
                this.opts.minYear = Year.min;
            }
            if (this.opts.maxYear > Year.max) {
                this.opts.maxYear = Year.max;
            }
            if (this.disabled !== undefined) {
                this.opts.componentDisabled = this.disabled;
            }
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.getSelectorTopPosition = /**
         * @return {?}
         */
        function () {
            if (this.opts.openSelectorTopOfInput) {
                return this.elem.nativeElement.children[0].offsetHeight + "px";
            }
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.resetMonthYearSelect = /**
         * @return {?}
         */
        function () {
            this.selectMonth = false;
            this.selectYear = false;
        };
        /**
         * @param {?} event
         * @return {?}
         */
        MyDatePicker.prototype.onSelectMonthClicked = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            event.stopPropagation();
            this.selectMonth = !this.selectMonth;
            this.selectYear = false;
            this.cdr.detectChanges();
            if (this.selectMonth) {
                /** @type {?} */
                var today = this.getToday();
                this.months.length = 0;
                for (var i = 1; i <= 12; i += 3) {
                    /** @type {?} */
                    var row = [];
                    for (var j = i; j < i + 3; j++) {
                        /** @type {?} */
                        var disabled = this.utilService.isMonthDisabledByDisableUntil({ year: this.visibleMonth.year, month: j, day: this.daysInMonth(j, this.visibleMonth.year) }, this.opts.disableUntil)
                            || this.utilService.isMonthDisabledByDisableSince({ year: this.visibleMonth.year, month: j, day: 1 }, this.opts.disableSince);
                        row.push({ nbr: j, name: this.opts.monthLabels[j], currMonth: j === today.month && this.visibleMonth.year === today.year, selected: j === this.visibleMonth.monthNbr, disabled: disabled });
                    }
                    this.months.push(row);
                }
            }
        };
        /**
         * @param {?} cell
         * @return {?}
         */
        MyDatePicker.prototype.onMonthCellClicked = /**
         * @param {?} cell
         * @return {?}
         */
        function (cell) {
            /** @type {?} */
            var mc = cell.nbr !== this.visibleMonth.monthNbr;
            this.visibleMonth = { monthTxt: this.monthText(cell.nbr), monthNbr: cell.nbr, year: this.visibleMonth.year };
            this.generateCalendar(cell.nbr, this.visibleMonth.year, mc);
            this.selectMonth = false;
            this.selectorEl.nativeElement.focus();
        };
        /**
         * @param {?} event
         * @param {?} cell
         * @return {?}
         */
        MyDatePicker.prototype.onMonthCellKeyDown = /**
         * @param {?} event
         * @param {?} cell
         * @return {?}
         */
        function (event, cell) {
            if ((event.keyCode === KeyCode.enter || event.keyCode === KeyCode.space) && !cell.disabled) {
                event.preventDefault();
                this.onMonthCellClicked(cell);
            }
        };
        /**
         * @param {?} event
         * @return {?}
         */
        MyDatePicker.prototype.onSelectYearClicked = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            event.stopPropagation();
            this.selectYear = !this.selectYear;
            this.selectMonth = false;
            this.cdr.detectChanges();
            if (this.selectYear) {
                this.generateYears(Number(this.visibleMonth.year));
            }
        };
        /**
         * @param {?} cell
         * @return {?}
         */
        MyDatePicker.prototype.onYearCellClicked = /**
         * @param {?} cell
         * @return {?}
         */
        function (cell) {
            /** @type {?} */
            var yc = cell.year !== this.visibleMonth.year;
            this.visibleMonth = { monthTxt: this.visibleMonth.monthTxt, monthNbr: this.visibleMonth.monthNbr, year: cell.year };
            this.generateCalendar(this.visibleMonth.monthNbr, cell.year, yc);
            this.selectYear = false;
            this.selectorEl.nativeElement.focus();
        };
        /**
         * @param {?} event
         * @param {?} cell
         * @return {?}
         */
        MyDatePicker.prototype.onYearCellKeyDown = /**
         * @param {?} event
         * @param {?} cell
         * @return {?}
         */
        function (event, cell) {
            if ((event.keyCode === KeyCode.enter || event.keyCode === KeyCode.space) && !cell.disabled) {
                event.preventDefault();
                this.onYearCellClicked(cell);
            }
        };
        /**
         * @param {?} event
         * @param {?} year
         * @return {?}
         */
        MyDatePicker.prototype.onPrevYears = /**
         * @param {?} event
         * @param {?} year
         * @return {?}
         */
        function (event, year) {
            event.stopPropagation();
            this.generateYears(Number(year) - 25);
        };
        /**
         * @param {?} event
         * @param {?} year
         * @return {?}
         */
        MyDatePicker.prototype.onNextYears = /**
         * @param {?} event
         * @param {?} year
         * @return {?}
         */
        function (event, year) {
            event.stopPropagation();
            this.generateYears(Number(year) + 25);
        };
        /**
         * @param {?} year
         * @return {?}
         */
        MyDatePicker.prototype.generateYears = /**
         * @param {?} year
         * @return {?}
         */
        function (year) {
            this.years.length = 0;
            /** @type {?} */
            var today = this.getToday();
            for (var i = year; i <= 20 + year; i += 5) {
                /** @type {?} */
                var row = [];
                for (var j = i; j < i + 5; j++) {
                    /** @type {?} */
                    var disabled = this.utilService.isMonthDisabledByDisableUntil({ year: j, month: this.visibleMonth.monthNbr, day: this.daysInMonth(this.visibleMonth.monthNbr, j) }, this.opts.disableUntil)
                        || this.utilService.isMonthDisabledByDisableSince({ year: j, month: this.visibleMonth.monthNbr, day: 1 }, this.opts.disableSince);
                    /** @type {?} */
                    var minMax = j < this.opts.minYear || j > this.opts.maxYear;
                    row.push({ year: j, currYear: j === today.year, selected: j === this.visibleMonth.year, disabled: disabled || minMax });
                }
                this.years.push(row);
            }
            this.prevYearsDisabled = this.years[0][0].year <= this.opts.minYear || this.utilService.isMonthDisabledByDisableUntil({ year: this.years[0][0].year - 1, month: this.visibleMonth.monthNbr, day: this.daysInMonth(this.visibleMonth.monthNbr, this.years[0][0].year - 1) }, this.opts.disableUntil);
            this.nextYearsDisabled = this.years[4][4].year >= this.opts.maxYear || this.utilService.isMonthDisabledByDisableSince({ year: this.years[4][4].year + 1, month: this.visibleMonth.monthNbr, day: 1 }, this.opts.disableSince);
        };
        /**
         * @param {?} value
         * @return {?}
         */
        MyDatePicker.prototype.onUserDateInput = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            if (value.length === 0) {
                if (this.utilService.isInitializedDate(this.selectedDate)) {
                    this.clearDate();
                }
                else {
                    this.invalidInputFieldChanged(value);
                }
            }
            else {
                /** @type {?} */
                var date = this.utilService.isDateValid(value, this.opts.dateFormat, this.opts.minYear, this.opts.maxYear, this.opts.disableUntil, this.opts.disableSince, this.opts.disableWeekends, this.opts.disableWeekdays, this.opts.disableDays, this.opts.disableDateRanges, this.opts.monthLabels, this.opts.enableDays);
                if (this.utilService.isInitializedDate(date)) {
                    if (!this.utilService.isSameDate(date, this.selectedDate)) {
                        this.selectDate(date, CalToggle.CloseByDateSel);
                    }
                    else {
                        this.updateDateValue(date);
                    }
                }
                else {
                    this.invalidInputFieldChanged(value);
                }
            }
        };
        /**
         * @param {?} event
         * @return {?}
         */
        MyDatePicker.prototype.onFocusInput = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            this.inputFocusBlur.emit({ reason: InputFocusBlur.focus, value: event.target.value });
        };
        /**
         * @param {?} event
         * @return {?}
         */
        MyDatePicker.prototype.onBlurInput = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            this.selectionDayTxt = event.target.value;
            this.onTouchedCb();
            this.inputFocusBlur.emit({ reason: InputFocusBlur.blur, value: event.target.value });
        };
        /**
         * @param {?} event
         * @return {?}
         */
        MyDatePicker.prototype.onCloseSelector = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            if (event.keyCode === KeyCode.esc && this.showSelector && !this.opts.inline) {
                this.calendarToggle.emit(CalToggle.CloseByEsc);
                this.showSelector = false;
            }
        };
        /**
         * @param {?} value
         * @return {?}
         */
        MyDatePicker.prototype.invalidInputFieldChanged = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.invalidDate = value.length > 0;
            this.inputFieldChanged.emit({ value: value, dateFormat: this.opts.dateFormat, valid: false });
            this.onChangeCb(null);
            this.onTouchedCb();
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.isTodayDisabled = /**
         * @return {?}
         */
        function () {
            this.disableTodayBtn = this.utilService.isDisabledDay(this.getToday(), this.opts.minYear, this.opts.maxYear, this.opts.disableUntil, this.opts.disableSince, this.opts.disableWeekends, this.opts.disableWeekdays, this.opts.disableDays, this.opts.disableDateRanges, this.opts.enableDays);
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.parseOptions = /**
         * @return {?}
         */
        function () {
            if (this.locale) {
                this.setLocaleOptions();
            }
            this.setOptions();
            /** @type {?} */
            var weekDays = this.utilService.getWeekDays();
            this.isTodayDisabled();
            this.dayIdx = weekDays.indexOf(this.opts.firstDayOfWeek);
            if (this.dayIdx !== -1) {
                /** @type {?} */
                var idx = this.dayIdx;
                for (var i = 0; i < weekDays.length; i++) {
                    this.weekDays.push(this.opts.dayLabels[weekDays[idx]]);
                    idx = weekDays[idx] === "sa" ? 0 : idx + 1;
                }
            }
        };
        /**
         * @param {?} value
         * @return {?}
         */
        MyDatePicker.prototype.writeValue = /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            if (value && (value["date"] || value["jsdate"] || value["formatted"])) {
                this.selectedDate = value["date"] ? this.parseSelectedDate(value["date"]) : value["jsdate"] ? this.parseSelectedDate(this.jsDateToMyDate(value["jsdate"])) : this.parseSelectedDate(value["formatted"]);
                /** @type {?} */
                var cvc = this.visibleMonth.year !== this.selectedDate.year || this.visibleMonth.monthNbr !== this.selectedDate.month;
                if (cvc) {
                    this.visibleMonth = { monthTxt: this.opts.monthLabels[this.selectedDate.month], monthNbr: this.selectedDate.month, year: this.selectedDate.year };
                    this.generateCalendar(this.selectedDate.month, this.selectedDate.year, cvc);
                }
                this.selectionDayTxt = this.utilService.formatDate(this.selectedDate, this.opts.dateFormat, this.opts.monthLabels);
            }
            else if (value === null || value === "") {
                this.selectedDate = { year: 0, month: 0, day: 0 };
                this.selectionDayTxt = "";
            }
            this.inputFieldChanged.emit({ value: this.selectionDayTxt, dateFormat: this.opts.dateFormat, valid: this.selectionDayTxt.length > 0 });
            this.invalidDate = false;
        };
        /**
         * @param {?} disabled
         * @return {?}
         */
        MyDatePicker.prototype.setDisabledState = /**
         * @param {?} disabled
         * @return {?}
         */
        function (disabled) {
            this.opts.componentDisabled = disabled;
        };
        /**
         * @param {?} fn
         * @return {?}
         */
        MyDatePicker.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) {
            this.onChangeCb = fn;
        };
        /**
         * @param {?} fn
         * @return {?}
         */
        MyDatePicker.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) {
            this.onTouchedCb = fn;
        };
        /**
         * @param {?} changes
         * @return {?}
         */
        MyDatePicker.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            var _this = this;
            if (changes.hasOwnProperty("selector")) {
                /** @type {?} */
                var s = changes["selector"].currentValue;
                if (typeof s === "object") {
                    if (s.open) {
                        this.showSelector = true;
                        this.openSelector(CalToggle.Open);
                    }
                    else {
                        this.showSelector = false;
                        this.closeSelector(CalToggle.CloseByApi);
                    }
                }
                else if (s > 0) {
                    this.openBtnClicked();
                }
            }
            if (changes.hasOwnProperty("placeholder")) {
                this.placeholder = changes["placeholder"].currentValue;
            }
            if (changes.hasOwnProperty("locale")) {
                this.locale = changes["locale"].currentValue;
            }
            if (changes.hasOwnProperty("disabled")) {
                this.disabled = changes["disabled"].currentValue;
            }
            if (changes.hasOwnProperty("options")) {
                this.options = changes["options"].currentValue;
            }
            this.weekDays.length = 0;
            this.parseOptions();
            /** @type {?} */
            var dmChange = false;
            if (changes.hasOwnProperty("defaultMonth")) {
                /** @type {?} */
                var dm = changes["defaultMonth"].currentValue;
                if (typeof dm === "object") {
                    dm = dm.defMonth;
                }
                if (dm !== null && dm !== undefined && dm !== "") {
                    this.selectedMonth = this.parseSelectedMonth(dm);
                }
                else {
                    this.selectedMonth = { monthTxt: "", monthNbr: 0, year: 0 };
                }
                dmChange = true;
            }
            if (changes.hasOwnProperty("selDate")) {
                /** @type {?} */
                var sd = changes["selDate"];
                if (sd.currentValue !== null && sd.currentValue !== undefined && sd.currentValue !== "" && Object.keys(sd.currentValue).length !== 0) {
                    this.selectedDate = this.parseSelectedDate(sd.currentValue);
                    setTimeout((/**
                     * @return {?}
                     */
                    function () {
                        _this.onChangeCb(_this.getDateModel(_this.selectedDate));
                    }));
                }
                else {
                    // Do not clear on init
                    if (!sd.isFirstChange()) {
                        this.clearDate();
                    }
                }
            }
            if (this.visibleMonth.year === 0 && this.visibleMonth.monthNbr === 0 || dmChange) {
                this.setVisibleMonth();
            }
            else {
                this.visibleMonth.monthTxt = this.opts.monthLabels[this.visibleMonth.monthNbr];
                this.generateCalendar(this.visibleMonth.monthNbr, this.visibleMonth.year, false);
            }
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.removeBtnClicked = /**
         * @return {?}
         */
        function () {
            // Remove date button clicked
            this.clearDate();
            if (this.showSelector) {
                this.calendarToggle.emit(CalToggle.CloseByCalBtn);
            }
            this.showSelector = false;
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.onDecreaseBtnClicked = /**
         * @return {?}
         */
        function () {
            // Decrease date button clicked
            this.decreaseIncreaseDate(true);
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.onIncreaseBtnClicked = /**
         * @return {?}
         */
        function () {
            // Increase date button clicked
            this.decreaseIncreaseDate(false);
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.openBtnClicked = /**
         * @return {?}
         */
        function () {
            // Open selector button clicked
            this.showSelector = !this.showSelector;
            this.cdr.detectChanges();
            if (this.showSelector) {
                this.openSelector(CalToggle.Open);
            }
            else {
                this.closeSelector(CalToggle.CloseByCalBtn);
            }
        };
        /**
         * @param {?} reason
         * @return {?}
         */
        MyDatePicker.prototype.openSelector = /**
         * @param {?} reason
         * @return {?}
         */
        function (reason) {
            var _this = this;
            this.globalListener = this.globalListener || this.renderer.listenGlobal("document", "click", (/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                if (_this.showSelector && event.target && _this.elem.nativeElement !== event.target && !_this.elem.nativeElement.contains(event.target)) {
                    _this.showSelector = false;
                    _this.calendarToggle.emit(CalToggle.CloseByOutClick);
                }
                if (_this.opts.monthSelector || _this.opts.yearSelector) {
                    _this.resetMonthYearSelect();
                }
            }));
            this.setVisibleMonth();
            this.calendarToggle.emit(reason);
        };
        /**
         * @param {?} reason
         * @return {?}
         */
        MyDatePicker.prototype.closeSelector = /**
         * @param {?} reason
         * @return {?}
         */
        function (reason) {
            if (this.globalListener) {
                this.globalListener();
            }
            this.calendarToggle.emit(reason);
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.setVisibleMonth = /**
         * @return {?}
         */
        function () {
            // Sets visible month of calendar
            /** @type {?} */
            var y = 0;
            /** @type {?} */
            var m = 0;
            if (!this.utilService.isInitializedDate(this.selectedDate)) {
                if (this.selectedMonth.year === 0 && this.selectedMonth.monthNbr === 0) {
                    /** @type {?} */
                    var today = this.getToday();
                    y = today.year;
                    m = today.month;
                }
                else {
                    y = this.selectedMonth.year;
                    m = this.selectedMonth.monthNbr;
                }
            }
            else {
                y = this.selectedDate.year;
                m = this.selectedDate.month;
            }
            this.visibleMonth = { monthTxt: this.opts.monthLabels[m], monthNbr: m, year: y };
            // Create current month
            this.generateCalendar(m, y, true);
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.onPrevMonth = /**
         * @return {?}
         */
        function () {
            // Previous month from calendar
            /** @type {?} */
            var d = this.getDate(this.visibleMonth.year, this.visibleMonth.monthNbr, 1);
            d.setMonth(d.getMonth() - 1);
            /** @type {?} */
            var y = d.getFullYear();
            /** @type {?} */
            var m = d.getMonth() + 1;
            this.visibleMonth = { monthTxt: this.monthText(m), monthNbr: m, year: y };
            this.generateCalendar(m, y, true);
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.onNextMonth = /**
         * @return {?}
         */
        function () {
            // Next month from calendar
            /** @type {?} */
            var d = this.getDate(this.visibleMonth.year, this.visibleMonth.monthNbr, 1);
            d.setMonth(d.getMonth() + 1);
            /** @type {?} */
            var y = d.getFullYear();
            /** @type {?} */
            var m = d.getMonth() + 1;
            this.visibleMonth = { monthTxt: this.monthText(m), monthNbr: m, year: y };
            this.generateCalendar(m, y, true);
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.onPrevYear = /**
         * @return {?}
         */
        function () {
            // Previous year from calendar
            this.visibleMonth.year--;
            this.generateCalendar(this.visibleMonth.monthNbr, this.visibleMonth.year, true);
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.onNextYear = /**
         * @return {?}
         */
        function () {
            // Next year from calendar
            this.visibleMonth.year++;
            this.generateCalendar(this.visibleMonth.monthNbr, this.visibleMonth.year, true);
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.onTodayClicked = /**
         * @return {?}
         */
        function () {
            // Today button clicked
            /** @type {?} */
            var today = this.getToday();
            this.selectDate(today, CalToggle.CloseByDateSel);
            if (this.opts.inline && today.year !== this.visibleMonth.year || today.month !== this.visibleMonth.monthNbr) {
                this.visibleMonth = { monthTxt: this.opts.monthLabels[today.month], monthNbr: today.month, year: today.year };
                this.generateCalendar(today.month, today.year, true);
            }
        };
        /**
         * @param {?} cell
         * @return {?}
         */
        MyDatePicker.prototype.onCellClicked = /**
         * @param {?} cell
         * @return {?}
         */
        function (cell) {
            // Cell clicked on the calendar
            if (cell.cmo === this.prevMonthId) {
                // Previous month day
                this.onPrevMonth();
                if (!this.opts.allowSelectionOnlyInCurrentMonth) {
                    this.selectDate(cell.dateObj, CalToggle.CloseByDateSel);
                }
            }
            else if (cell.cmo === this.currMonthId) {
                // Current month day - if date is already selected clear it
                if (this.opts.allowDeselectDate && this.utilService.isSameDate(cell.dateObj, this.selectedDate)) {
                    this.clearDate();
                }
                else {
                    this.selectDate(cell.dateObj, CalToggle.CloseByDateSel);
                }
            }
            else if (cell.cmo === this.nextMonthId) {
                // Next month day
                this.onNextMonth();
                if (!this.opts.allowSelectionOnlyInCurrentMonth) {
                    this.selectDate(cell.dateObj, CalToggle.CloseByDateSel);
                }
            }
            this.resetMonthYearSelect();
        };
        /**
         * @param {?} event
         * @param {?} cell
         * @return {?}
         */
        MyDatePicker.prototype.onCellKeyDown = /**
         * @param {?} event
         * @param {?} cell
         * @return {?}
         */
        function (event, cell) {
            // Cell keyboard handling
            if ((event.keyCode === KeyCode.enter || event.keyCode === KeyCode.space) && !cell.disabled) {
                event.preventDefault();
                this.onCellClicked(cell);
            }
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.clearDate = /**
         * @return {?}
         */
        function () {
            // Clears the date
            this.updateDateValue({ year: 0, month: 0, day: 0 });
            this.setFocusToInputBox();
        };
        /**
         * @param {?} decrease
         * @return {?}
         */
        MyDatePicker.prototype.decreaseIncreaseDate = /**
         * @param {?} decrease
         * @return {?}
         */
        function (decrease) {
            // Decreases or increases the date depending on the parameter
            /** @type {?} */
            var date = this.selectedDate;
            if (this.utilService.isInitializedDate(date)) {
                /** @type {?} */
                var d = this.getDate(date.year, date.month, date.day);
                d.setDate(decrease ? d.getDate() - 1 : d.getDate() + 1);
                date = { year: d.getFullYear(), month: d.getMonth() + 1, day: d.getDate() };
            }
            else {
                date = this.getToday();
            }
            this.selectDate(date, CalToggle.CloseByCalBtn);
        };
        /**
         * @param {?} date
         * @param {?} closeReason
         * @return {?}
         */
        MyDatePicker.prototype.selectDate = /**
         * @param {?} date
         * @param {?} closeReason
         * @return {?}
         */
        function (date, closeReason) {
            this.updateDateValue(date);
            if (this.showSelector) {
                this.calendarToggle.emit(closeReason);
            }
            this.showSelector = false;
            this.setFocusToInputBox();
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.setFocusToInputBox = /**
         * @return {?}
         */
        function () {
            var _this = this;
            if (!this.opts.inline && this.opts.showInputField) {
                setTimeout((/**
                 * @return {?}
                 */
                function () {
                    _this.inputBoxEl.nativeElement.focus();
                }), 100);
            }
        };
        /**
         * @param {?} date
         * @return {?}
         */
        MyDatePicker.prototype.updateDateValue = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            /** @type {?} */
            var clear = !this.utilService.isInitializedDate(date);
            this.selectedDate = date;
            this.emitDateChanged(date);
            if (!this.opts.inline) {
                this.selectionDayTxt = clear ? "" : this.utilService.formatDate(date, this.opts.dateFormat, this.opts.monthLabels);
                this.inputFieldChanged.emit({ value: this.selectionDayTxt, dateFormat: this.opts.dateFormat, valid: !clear });
                this.invalidDate = false;
            }
        };
        /**
         * @param {?} date
         * @return {?}
         */
        MyDatePicker.prototype.emitDateChanged = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            if (this.utilService.isInitializedDate(date)) {
                /** @type {?} */
                var dateModel = this.getDateModel(date);
                this.dateChanged.emit(dateModel);
                this.onChangeCb(dateModel);
                this.onTouchedCb();
            }
            else {
                this.dateChanged.emit({ date: date, jsdate: null, formatted: "", epoc: 0 });
                this.onChangeCb(null);
                this.onTouchedCb();
            }
        };
        /**
         * @param {?} date
         * @return {?}
         */
        MyDatePicker.prototype.getDateModel = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            // Creates a date model object from the given parameter
            return { date: date, jsdate: this.getDate(date.year, date.month, date.day), formatted: this.utilService.formatDate(date, this.opts.dateFormat, this.opts.monthLabels), epoc: Math.round(this.getTimeInMilliseconds(date) / 1000.0) };
        };
        /**
         * @param {?} m
         * @return {?}
         */
        MyDatePicker.prototype.monthText = /**
         * @param {?} m
         * @return {?}
         */
        function (m) {
            // Returns month as a text
            return this.opts.monthLabels[m];
        };
        /**
         * @param {?} y
         * @param {?} m
         * @return {?}
         */
        MyDatePicker.prototype.monthStartIdx = /**
         * @param {?} y
         * @param {?} m
         * @return {?}
         */
        function (y, m) {
            // Month start index
            /** @type {?} */
            var d = new Date();
            d.setDate(1);
            d.setMonth(m - 1);
            d.setFullYear(y);
            /** @type {?} */
            var idx = d.getDay() + this.sundayIdx();
            return idx >= 7 ? idx - 7 : idx;
        };
        /**
         * @param {?} m
         * @param {?} y
         * @return {?}
         */
        MyDatePicker.prototype.daysInMonth = /**
         * @param {?} m
         * @param {?} y
         * @return {?}
         */
        function (m, y) {
            // Return number of days of current month
            return new Date(y, m, 0).getDate();
        };
        /**
         * @param {?} m
         * @param {?} y
         * @return {?}
         */
        MyDatePicker.prototype.daysInPrevMonth = /**
         * @param {?} m
         * @param {?} y
         * @return {?}
         */
        function (m, y) {
            // Return number of days of the previous month
            /** @type {?} */
            var d = this.getDate(y, m, 1);
            d.setMonth(d.getMonth() - 1);
            return this.daysInMonth(d.getMonth() + 1, d.getFullYear());
        };
        /**
         * @param {?} d
         * @param {?} m
         * @param {?} y
         * @param {?} cmo
         * @param {?} today
         * @return {?}
         */
        MyDatePicker.prototype.isCurrDay = /**
         * @param {?} d
         * @param {?} m
         * @param {?} y
         * @param {?} cmo
         * @param {?} today
         * @return {?}
         */
        function (d, m, y, cmo, today) {
            // Check is a given date the today
            return d === today.day && m === today.month && y === today.year && cmo === this.currMonthId;
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.getToday = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var date = new Date();
            return { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
        };
        /**
         * @param {?} date
         * @return {?}
         */
        MyDatePicker.prototype.getTimeInMilliseconds = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return this.getDate(date.year, date.month, date.day).getTime();
        };
        /**
         * @param {?} date
         * @return {?}
         */
        MyDatePicker.prototype.getWeekday = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            // Get weekday: su, mo, tu, we ...
            /** @type {?} */
            var weekDays = this.utilService.getWeekDays();
            return weekDays[this.utilService.getDayNumber(date)];
        };
        /**
         * @param {?} year
         * @param {?} month
         * @param {?} day
         * @return {?}
         */
        MyDatePicker.prototype.getDate = /**
         * @param {?} year
         * @param {?} month
         * @param {?} day
         * @return {?}
         */
        function (year, month, day) {
            // Creates a date object from given year, month and day
            return new Date(year, month - 1, day, 0, 0, 0, 0);
        };
        /**
         * @return {?}
         */
        MyDatePicker.prototype.sundayIdx = /**
         * @return {?}
         */
        function () {
            // Index of Sunday day
            return this.dayIdx > 0 ? 7 - this.dayIdx : 0;
        };
        /**
         * @param {?} m
         * @param {?} y
         * @param {?} notifyChange
         * @return {?}
         */
        MyDatePicker.prototype.generateCalendar = /**
         * @param {?} m
         * @param {?} y
         * @param {?} notifyChange
         * @return {?}
         */
        function (m, y, notifyChange) {
            this.dates.length = 0;
            /** @type {?} */
            var today = this.getToday();
            /** @type {?} */
            var monthStart = this.monthStartIdx(y, m);
            /** @type {?} */
            var dInThisM = this.daysInMonth(m, y);
            /** @type {?} */
            var dInPrevM = this.daysInPrevMonth(m, y);
            /** @type {?} */
            var dayNbr = 1;
            /** @type {?} */
            var cmo = this.prevMonthId;
            for (var i = 1; i < 7; i++) {
                /** @type {?} */
                var week = [];
                if (i === 1) {
                    // First week
                    /** @type {?} */
                    var pm = dInPrevM - monthStart + 1;
                    // Previous month
                    for (var j = pm; j <= dInPrevM; j++) {
                        /** @type {?} */
                        var date = { year: m === 1 ? y - 1 : y, month: m === 1 ? 12 : m - 1, day: j };
                        week.push({ dateObj: date, cmo: cmo, currDay: this.isCurrDay(j, m, y, cmo, today),
                            disabled: this.utilService.isDisabledDay(date, this.opts.minYear, this.opts.maxYear, this.opts.disableUntil, this.opts.disableSince, this.opts.disableWeekends, this.opts.disableWeekdays, this.opts.disableDays, this.opts.disableDateRanges, this.opts.enableDays),
                            markedDate: this.utilService.isMarkedDate(date, this.opts.markDates, this.opts.markWeekends),
                            highlight: this.utilService.isHighlightedDate(date, this.opts.sunHighlight, this.opts.satHighlight, this.opts.highlightDates) });
                    }
                    cmo = this.currMonthId;
                    // Current month
                    /** @type {?} */
                    var daysLeft = 7 - week.length;
                    for (var j = 0; j < daysLeft; j++) {
                        /** @type {?} */
                        var date = { year: y, month: m, day: dayNbr };
                        week.push({ dateObj: date, cmo: cmo, currDay: this.isCurrDay(dayNbr, m, y, cmo, today),
                            disabled: this.utilService.isDisabledDay(date, this.opts.minYear, this.opts.maxYear, this.opts.disableUntil, this.opts.disableSince, this.opts.disableWeekends, this.opts.disableWeekdays, this.opts.disableDays, this.opts.disableDateRanges, this.opts.enableDays),
                            markedDate: this.utilService.isMarkedDate(date, this.opts.markDates, this.opts.markWeekends),
                            highlight: this.utilService.isHighlightedDate(date, this.opts.sunHighlight, this.opts.satHighlight, this.opts.highlightDates) });
                        dayNbr++;
                    }
                }
                else {
                    // Rest of the weeks
                    for (var j = 1; j < 8; j++) {
                        if (dayNbr > dInThisM) {
                            // Next month
                            dayNbr = 1;
                            cmo = this.nextMonthId;
                        }
                        /** @type {?} */
                        var date = { year: cmo === this.nextMonthId && m === 12 ? y + 1 : y, month: cmo === this.currMonthId ? m : cmo === this.nextMonthId && m < 12 ? m + 1 : 1, day: dayNbr };
                        week.push({ dateObj: date, cmo: cmo, currDay: this.isCurrDay(dayNbr, m, y, cmo, today),
                            disabled: this.utilService.isDisabledDay(date, this.opts.minYear, this.opts.maxYear, this.opts.disableUntil, this.opts.disableSince, this.opts.disableWeekends, this.opts.disableWeekdays, this.opts.disableDays, this.opts.disableDateRanges, this.opts.enableDays),
                            markedDate: this.utilService.isMarkedDate(date, this.opts.markDates, this.opts.markWeekends),
                            highlight: this.utilService.isHighlightedDate(date, this.opts.sunHighlight, this.opts.satHighlight, this.opts.highlightDates) });
                        dayNbr++;
                    }
                }
                /** @type {?} */
                var weekNbr = this.opts.showWeekNumbers && this.opts.firstDayOfWeek === "mo" ? this.utilService.getWeekNumber(week[0].dateObj) : 0;
                this.dates.push({ week: week, weekNbr: weekNbr });
            }
            this.setHeaderBtnDisabledState(m, y);
            if (notifyChange) {
                // Notify parent
                this.calendarViewChanged.emit({ year: y, month: m, first: { number: 1, weekday: this.getWeekday({ year: y, month: m, day: 1 }) }, last: { number: dInThisM, weekday: this.getWeekday({ year: y, month: m, day: dInThisM }) } });
            }
        };
        /**
         * @param {?} selDate
         * @return {?}
         */
        MyDatePicker.prototype.parseSelectedDate = /**
         * @param {?} selDate
         * @return {?}
         */
        function (selDate) {
            // Parse date value - it can be string or IMyDate object
            /** @type {?} */
            var date = { day: 0, month: 0, year: 0 };
            if (typeof selDate === "string") {
                /** @type {?} */
                var sd = (/** @type {?} */ (selDate));
                /** @type {?} */
                var df = this.opts.dateFormat;
                /** @type {?} */
                var delimeters = this.utilService.getDateFormatDelimeters(df);
                /** @type {?} */
                var dateValue = this.utilService.getDateValue(sd, df, delimeters);
                date.year = this.utilService.getNumberByValue(dateValue[0]);
                date.month = df.indexOf(MMM$1) !== -1 ? this.utilService.getMonthNumberByMonthName(dateValue[1], this.opts.monthLabels) : this.utilService.getNumberByValue(dateValue[1]);
                date.day = this.utilService.getNumberByValue(dateValue[2]);
            }
            else if (typeof selDate === "object") {
                date = selDate;
            }
            this.selectionDayTxt = this.utilService.formatDate(date, this.opts.dateFormat, this.opts.monthLabels);
            return date;
        };
        /**
         * @param {?} date
         * @return {?}
         */
        MyDatePicker.prototype.jsDateToMyDate = /**
         * @param {?} date
         * @return {?}
         */
        function (date) {
            return { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() };
        };
        /**
         * @param {?} ms
         * @return {?}
         */
        MyDatePicker.prototype.parseSelectedMonth = /**
         * @param {?} ms
         * @return {?}
         */
        function (ms) {
            return this.utilService.parseDefaultMonth(ms);
        };
        /**
         * @param {?} m
         * @param {?} y
         * @return {?}
         */
        MyDatePicker.prototype.setHeaderBtnDisabledState = /**
         * @param {?} m
         * @param {?} y
         * @return {?}
         */
        function (m, y) {
            /** @type {?} */
            var dpm = false;
            /** @type {?} */
            var dpy = false;
            /** @type {?} */
            var dnm = false;
            /** @type {?} */
            var dny = false;
            if (this.opts.disableHeaderButtons) {
                dpm = this.utilService.isMonthDisabledByDisableUntil({ year: m === 1 ? y - 1 : y, month: m === 1 ? 12 : m - 1, day: this.daysInMonth(m === 1 ? 12 : m - 1, m === 1 ? y - 1 : y) }, this.opts.disableUntil);
                dpy = this.utilService.isMonthDisabledByDisableUntil({ year: y - 1, month: m, day: this.daysInMonth(m, y - 1) }, this.opts.disableUntil);
                dnm = this.utilService.isMonthDisabledByDisableSince({ year: m === 12 ? y + 1 : y, month: m === 12 ? 1 : m + 1, day: 1 }, this.opts.disableSince);
                dny = this.utilService.isMonthDisabledByDisableSince({ year: y + 1, month: m, day: 1 }, this.opts.disableSince);
            }
            this.prevMonthDisabled = m === 1 && y === this.opts.minYear || dpm;
            this.prevYearDisabled = y - 1 < this.opts.minYear || dpy;
            this.nextMonthDisabled = m === 12 && y === this.opts.maxYear || dnm;
            this.nextYearDisabled = y + 1 > this.opts.maxYear || dny;
        };
        // Remove listeners or nullify globals on component destroy 
        // Remove listeners or nullify globals on component destroy 
        /**
         * @return {?}
         */
        MyDatePicker.prototype.ngOnDestroy = 
        // Remove listeners or nullify globals on component destroy 
        /**
         * @return {?}
         */
        function () {
            if (this.globalListener) {
                this.globalListener();
            }
        };
        MyDatePicker.decorators = [
            { type: core.Component, args: [{
                        selector: "my-date-picker",
                        exportAs: "mydatepicker",
                        template: "<div class=\"mydp\" [ngStyle]=\"{'width': opts.showInputField ? opts.width : null, 'border': opts.inline ? 'none' : null}\">\n    <div class=\"selectiongroup\" *ngIf=\"!opts.inline\">\n        <input *ngIf=\"opts.showInputField\" [textMask]=\"{mask: opts.dateMaskRegex}\" #inputBoxEl ngtype=\"text\" class=\"selection\" [attr.aria-label]=\"opts.ariaLabelInputField\" (click)=\"opts.openSelectorOnInputClick&&!opts.editableDateField&&openBtnClicked()\" [ngClass]=\"{'invaliddate': invalidDate&&opts.indicateInvalidDate, 'inputnoteditable': opts.openSelectorOnInputClick&&!opts.editableDateField, 'selectiondisabled': opts.componentDisabled}\"\n               placeholder=\"{{placeholder}}\" [ngStyle]=\"{'height': opts.height, 'font-size': opts.selectionTxtFontSize}\" [ngModel]=\"selectionDayTxt\" (ngModelChange)=\"onUserDateInput($event)\" [value]=\"selectionDayTxt\" (keyup)=\"onCloseSelector($event)\"\n               (focus)=\"opts.editableDateField&&onFocusInput($event)\" (blur)=\"opts.editableDateField&&onBlurInput($event)\" [disabled]=\"opts.componentDisabled\" [readonly]=\"!opts.editableDateField\" autocomplete=\"off\" spellcheck=\"false\" autocorrect=\"off\">\n        <div class=\"selbtngroup\" [style.height]=\"opts.height\">\n            <button type=\"button\" [attr.aria-label]=\"opts.ariaLabelDecreaseDate\" class=\"btndecrease\" *ngIf=\"opts.showDecreaseDateBtn\" (click)=\"onDecreaseBtnClicked()\" [ngClass]=\"{'btndecreaseenabled': !opts.componentDisabled, 'btndecreasedisabled': opts.componentDisabled, 'btnleftborderradius': !opts.showInputField}\" [disabled]=\"opts.componentDisabled\">\n                <span class=\"mydpicon icon-mydpleft\"></span>\n            </button>\n            <button type=\"button\" [attr.aria-label]=\"opts.ariaLabelIncreaseDate\" class=\"btnincrease\" *ngIf=\"opts.showIncreaseDateBtn\" (click)=\"onIncreaseBtnClicked()\" [ngClass]=\"{'btnincreaseenabled': !opts.componentDisabled, 'btnincreasedisabled': opts.componentDisabled, 'btnleftborderradius': !opts.showDecreaseDateBtn&&!opts.showInputField}\" [disabled]=\"opts.componentDisabled\">\n                <span class=\"mydpicon icon-mydpright\"></span>\n            </button>\n            <button type=\"button\" [attr.aria-label]=\"opts.ariaLabelClearDate\" class=\"btnclear\" *ngIf=\"selectionDayTxt.length>0&&opts.showClearDateBtn\" (click)=\"removeBtnClicked()\" [ngClass]=\"{'btnclearenabled': !opts.componentDisabled, 'btncleardisabled': opts.componentDisabled, 'btnleftborderradius': !opts.showIncreaseDateBtn&&!opts.showDecreaseDateBtn&&!opts.showInputField}\" [disabled]=\"opts.componentDisabled\">\n                <span class=\"mydpicon icon-mydpremove\"></span>\n            </button>\n            <button type=\"button\" [attr.aria-label]=\"opts.ariaLabelOpenCalendar\" class=\"btnpicker\" (click)=\"openBtnClicked()\" [ngClass]=\"{'btnpickerenabled': !opts.componentDisabled, 'btnpickerdisabled': opts.componentDisabled, 'btnleftborderradius': !opts.showClearDateBtn&&!opts.showIncreaseDateBtn&&!opts.showDecreaseDateBtn&&!opts.showInputField||selectionDayTxt.length===0&&!opts.showInputField}\" [disabled]=\"opts.componentDisabled\">\n                <span class=\"mydpicon icon-mydpcalendar\"></span>\n            </button>\n        </div>\n    </div>\n    <div class=\"selector\" #selectorEl [ngStyle]=\"{'width': opts.selectorWidth, 'height' : opts.selectorHeight, 'bottom': getSelectorTopPosition()}\" *ngIf=\"showSelector||opts.inline\" [mydpfocus]=\"opts.inline?'0':'1'\" [ngClass]=\"{'inlinedp': opts.inline, 'alignselectorright': opts.alignSelectorRight, 'selectorarrow': opts.showSelectorArrow&&!opts.inline, 'selectorarrowleft': opts.showSelectorArrow&&!opts.alignSelectorRight&&!opts.inline, 'selectorarrowright': opts.showSelectorArrow&&opts.alignSelectorRight&&!opts.inline}\" (keyup)=\"onCloseSelector($event)\" tabindex=\"0\">\n        <table class=\"header\">\n            <tr>\n                <td>\n                    <div style=\"float:left\">\n                        <div class=\"headerbtncell\"><button type=\"button\" [attr.aria-label]=\"opts.ariaLabelPrevMonth\" class=\"headerbtn mydpicon icon-mydpleft\" (click)=\"onPrevMonth()\" [disabled]=\"prevMonthDisabled\" [ngClass]=\"{'headerbtnenabled': !prevMonthDisabled, 'headerbtndisabled': prevMonthDisabled}\"></button></div>\n                        <div class=\"headermonthtxt\">\n                            <button class=\"headerlabelbtn\" type=\"button\" [ngClass]=\"{'monthlabel': opts.monthSelector}\" (click)=\"opts.monthSelector&&onSelectMonthClicked($event)\" tabindex=\"{{opts.monthSelector?'0':'-1'}}\">{{visibleMonth.monthTxt}}</button>\n                        </div>\n                        <div class=\"headerbtncell\"><button type=\"button\" [attr.aria-label]=\"opts.ariaLabelNextMonth\" class=\"headerbtn mydpicon icon-mydpright\" (click)=\"onNextMonth()\" [disabled]=\"nextMonthDisabled\" [ngClass]=\"{'headerbtnenabled': !nextMonthDisabled, 'headerbtndisabled': nextMonthDisabled}\"></button></div>\n                    </div>\n                </td>\n                <td>\n                    <button *ngIf=\"opts.showTodayBtn\" type=\"button\" class=\"headertodaybtn\" (click)=\"onTodayClicked()\" [disabled]=\"disableTodayBtn\" [ngClass]=\"{'headertodaybtnenabled': !disableTodayBtn, 'headertodaybtndisabled': disableTodayBtn}\">\n                        <span class=\"mydpicon icon-mydptoday\"></span>\n                        <span>{{opts.todayBtnTxt}}</span>\n                    </button>\n                </td>\n                <td>\n                    <div style=\"float:right\">\n                        <div class=\"headerbtncell\"><button type=\"button\" [attr.aria-label]=\"opts.ariaLabelPrevYear\" class=\"headerbtn mydpicon icon-mydpleft\" (click)=\"onPrevYear()\" [disabled]=\"prevYearDisabled\" [ngClass]=\"{'headerbtnenabled': !prevYearDisabled, 'headerbtndisabled': prevYearDisabled}\"></button></div>\n                        <div class=\"headeryeartxt\">\n                            <button class=\"headerlabelbtn\" type=\"button\" [ngClass]=\"{'yearlabel': opts.yearSelector}\" (click)=\"opts.yearSelector&&onSelectYearClicked($event)\" tabindex=\"{{opts.yearSelector?'0':'-1'}}\">{{visibleMonth.year}}</button>\n                        </div>\n                        <div class=\"headerbtncell\"><button type=\"button\" [attr.aria-label]=\"opts.ariaLabelNextYear\" class=\"headerbtn mydpicon icon-mydpright\" (click)=\"onNextYear()\" [disabled]=\"nextYearDisabled\" [ngClass]=\"{'headerbtnenabled': !nextYearDisabled, 'headerbtndisabled': nextYearDisabled}\"></button></div>\n                    </div>\n                </td>\n            </tr>\n        </table>\n        <table class=\"caltable\" *ngIf=\"!selectMonth&&!selectYear\">\n            <thead><tr><th class=\"weekdaytitle weekdaytitleweeknbr\" *ngIf=\"opts.showWeekNumbers&&opts.firstDayOfWeek==='mo'\">#</th><th class=\"weekdaytitle\" scope=\"col\" *ngFor=\"let d of weekDays\">{{d}}</th></tr></thead>\n            <tbody>\n                <tr *ngFor=\"let w of dates\">\n                    <td class=\"daycell daycellweeknbr\" *ngIf=\"opts.showWeekNumbers&&opts.firstDayOfWeek==='mo'\">{{w.weekNbr}}</td>\n                    <td class=\"daycell\" *ngFor=\"let d of w.week\" [ngClass]=\"{'currmonth':d.cmo===currMonthId&&!d.disabled, 'selectedday':selectedDate.day===d.dateObj.day && selectedDate.month===d.dateObj.month && selectedDate.year===d.dateObj.year && d.cmo===currMonthId, 'disabled': d.disabled, 'tablesingleday':(!opts.allowSelectionOnlyInCurrentMonth||d.cmo===currMonthId&&opts.allowSelectionOnlyInCurrentMonth)&&!d.disabled}\" (click)=\"!d.disabled&&onCellClicked(d);$event.stopPropagation()\" (keydown)=\"onCellKeyDown($event, d)\" tabindex=\"0\">\n                        <div *ngIf=\"d.markedDate.marked\" class=\"markdate\" [ngStyle]=\"{'background-color': d.markedDate.color}\"></div>\n                        <div class=\"datevalue\" [ngClass]=\"{'prevmonth':d.cmo===prevMonthId,'currmonth':d.cmo===currMonthId,'nextmonth':d.cmo===nextMonthId,'highlight':d.highlight}\">\n                            <span [ngClass]=\"{'markcurrday':d.currDay&&opts.markCurrentDay, 'dimday': d.highlight && (d.cmo===prevMonthId || d.cmo===nextMonthId || d.disabled)}\">{{d.dateObj.day}}</span>\n                        </div>\n                    </td>\n                </tr>\n            </tbody>\n        </table>\n        <table class=\"monthtable\" *ngIf=\"selectMonth\">\n            <tbody>\n                <tr *ngFor=\"let mr of months\">\n                    <td class=\"monthcell tablesinglemonth\" [ngClass]=\"{'selectedmonth': m.selected, 'disabled': m.disabled}\" *ngFor=\"let m of mr\" (click)=\"!m.disabled&&onMonthCellClicked(m);$event.stopPropagation()\" (keydown)=\"onMonthCellKeyDown($event, m)\" tabindex=\"0\">\n                        <div class=\"monthvalue\" [ngClass]=\"{'markcurrmonth':m.currMonth&&opts.markCurrentMonth}\">{{m.name}}</div>\n                    </td>\n                </tr>\n            </tbody>\n        </table>\n        <table class=\"yeartable\" *ngIf=\"selectYear\">\n            <tbody>\n                <tr>\n                    <td colspan=\"5\" class=\"yearchangebtncell\" (click)=\"$event.stopPropagation()\">\n                        <button type=\"button\" class=\"yearchangebtn mydpicon icon-mydpup\" (click)=\"onPrevYears($event, years[0][0].year)\" [disabled]=\"prevYearsDisabled\" [ngClass]=\"{'yearchangebtnenabled': !prevYearsDisabled, 'yearchangebtndisabled': prevYearsDisabled}\"></button>\n                    </td>\n                </tr>\n                <tr *ngFor=\"let yr of years\">\n                    <td class=\"yearcell tablesingleyear\" [ngClass]=\"{'selectedyear': y.selected, 'disabled': y.disabled}\" *ngFor=\"let y of yr\" (click)=\"!y.disabled&&onYearCellClicked(y);$event.stopPropagation()\" (keydown)=\"onYearCellKeyDown($event, y)\" tabindex=\"0\">\n                        <div class=\"yearvalue\" [ngClass]=\"{'markcurryear':y.currYear&&opts.markCurrentYear}\">{{y.year}}</div>\n                    </td>\n                </tr>\n                <tr>\n                    <td colspan=\"5\" class=\"yearchangebtncell\" (click)=\"$event.stopPropagation()\">\n                        <button type=\"button\" class=\"yearchangebtn mydpicon icon-mydpdown\" (click)=\"onNextYears($event, years[0][0].year)\" [disabled]=\"nextYearsDisabled\" [ngClass]=\"{'yearchangebtnenabled': !nextYearsDisabled, 'yearchangebtndisabled': nextYearsDisabled}\"></button>\n                    </td>\n                </tr>\n            </tbody>\n        </table>\n    </div>\n</div>\n",
                        providers: [LocaleService, UtilService, MYDP_VALUE_ACCESSOR],
                        encapsulation: core.ViewEncapsulation.None,
                        styles: [".mydp{line-height:1.1;display:inline-block;position:relative}.mydp *{box-sizing:border-box;font-family:Arial,Helvetica,sans-serif;padding:0;margin:0}.mydp,.mydp .headertodaybtn,.mydp .selection,.mydp .selectiongroup,.mydp .selector{border-radius:4px}.mydp .header{border-radius:4px 4px 0 0}.mydp .caltable,.mydp .monthtable,.mydp .yeartable{border-radius:0 0 4px 4px}.mydp .caltable tbody tr:nth-child(6) td:first-child,.mydp .monthtable tbody tr:nth-child(4) td:first-child,.mydp .yeartable tbody tr:nth-child(7) td:first-child{border-bottom-left-radius:4px}.mydp .caltable tbody tr:nth-child(6) td:last-child,.mydp .monthtable tbody tr:nth-child(4) td:last-child,.mydp .yeartable tbody tr:nth-child(7) td:last-child{border-bottom-right-radius:4px}.mydp .btnpicker{border-radius:0 4px 4px 0}.mydp .btnleftborderradius{border-top-left-radius:4px;border-bottom-left-radius:4px}.mydp .selector{margin-top:2px;margin-left:-1px;position:absolute;padding:0;border:1px solid #ccc;z-index:100;-webkit-animation:.1s selectorfadein;animation:.1s selectorfadein}.mydp .selector:focus{border:1px solid #add8e6;outline:0}@-webkit-keyframes selectorfadein{from{opacity:0}to{opacity:1}}@keyframes selectorfadein{from{opacity:0}to{opacity:1}}.mydp .selectorarrow{background:#fafafa;margin-top:12px;padding:0}.mydp .selectorarrow:after,.mydp .selectorarrow:before{bottom:100%;border:solid transparent;content:\" \";height:0;width:0;position:absolute}.mydp .selectorarrow:after{border-color:rgba(250,250,250,0);border-bottom-color:#fafafa;border-width:10px;margin-left:-10px}.mydp .selectorarrow:before{border-color:rgba(204,204,204,0);border-bottom-color:#ccc;border-width:11px;margin-left:-11px}.mydp .selectorarrow:focus:before{border-bottom-color:#add8e6}.mydp .selectorarrowleft:after,.mydp .selectorarrowleft:before{left:24px}.mydp .selectorarrowright:after,.mydp .selectorarrowright:before{left:86%}.mydp .alignselectorright{right:-1px}.mydp .selectiongroup{position:relative;display:table;border:none;border-spacing:0;background-color:#fff}.mydp .selection{width:100%;outline:0;background-color:#fff;display:table-cell;position:absolute;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;padding-left:6px;border:none;color:#555}.mydp .invaliddate{background-color:#f1dede}.mydp ::-ms-clear{display:none}.mydp .selbtngroup{position:relative;vertical-align:middle;white-space:nowrap;width:1%;display:table-cell;font-size:0}.mydp .btnclear,.mydp .btndecrease,.mydp .btnincrease,.mydp .btnpicker{height:100%;width:26px;border:none;padding:0;outline:0;font:inherit;-moz-user-select:none}.mydp .btnleftborder{border-left:1px solid #ccc}.mydp .btnclearenabled,.mydp .btndecreaseenabled,.mydp .btnincreaseenabled,.mydp .btnpickerenabled,.mydp .headerbtnenabled,.mydp .headertodaybtnenabled,.mydp .yearchangebtnenabled{cursor:pointer}.mydp .btncleardisabled,.mydp .btndecreasedisabled,.mydp .btnincreasedisabled,.mydp .btnpickerdisabled,.mydp .headerbtndisabled,.mydp .headertodaybtndisabled,.mydp .selectiondisabled,.mydp .yearchangebtndisabled{cursor:not-allowed;opacity:.65}.mydp .selectiondisabled{background-color:#eee}.mydp .btnclear,.mydp .btndecrease,.mydp .btnincrease,.mydp .btnpicker,.mydp .headertodaybtn{background:#fff}.mydp .header{width:100%;height:30px;background-color:#fafafa}.mydp .header td{vertical-align:middle;border:none;line-height:0}.mydp .header td:nth-child(1){padding-left:4px}.mydp .header td:nth-child(2){text-align:center}.mydp .header td:nth-child(3){padding-right:4px}.mydp .caltable,.mydp .monthtable,.mydp .yeartable{table-layout:fixed;width:100%;height:calc(100% - 30px);background-color:#fff;font-size:14px}.mydp .caltable,.mydp .daycell,.mydp .monthcell,.mydp .monthtable,.mydp .weekdaytitle,.mydp .yearcell,.mydp .yeartable{border-collapse:collapse;color:#036;line-height:1.1}.mydp .daycell,.mydp .monthcell,.mydp .weekdaytitle,.mydp .yearcell{padding:4px;text-align:center}.mydp .weekdaytitle{background-color:#ddd;font-size:11px;font-weight:400;vertical-align:middle;max-width:36px;overflow:hidden;white-space:nowrap}.mydp .weekdaytitleweeknbr{width:20px;border-right:1px solid #bbb}.mydp .monthcell{background-color:#fafafa;overflow:hidden;white-space:nowrap}.mydp .yearcell{background-color:#fafafa;width:20%}.mydp .daycell .datevalue{background-color:inherit;vertical-align:middle}.mydp .daycell .datevalue span{vertical-align:middle}.mydp .daycellweeknbr{font-size:10px;border-right:1px solid #ccc;cursor:default;color:#000}.mydp .inlinedp{position:relative;margin-top:-1px}.mydp .nextmonth,.mydp .prevmonth{color:#999}.mydp .disabled{cursor:default!important;color:#ccc;background:#fbefef}.mydp .highlight{color:#c30000}.mydp .dimday{opacity:.5}.mydp .currmonth{background-color:#f6f6f6;font-weight:400}.mydp .markdate{position:absolute;width:4px;height:4px;border-radius:4px}.mydp .markcurrday,.mydp .markcurrmonth,.mydp .markcurryear{text-decoration:underline}.mydp .selectedday .datevalue,.mydp .selectedmonth .monthvalue,.mydp .selectedyear .yearvalue{border:none;background-color:#8ebfff;border-radius:2px}.mydp .headerbtncell{background-color:#fafafa;display:table-cell;vertical-align:middle}.mydp .yearchangebtncell{text-align:center;background-color:#fafafa}.mydp .headerbtn,.mydp .headerlabelbtn,.mydp .yearchangebtn{background:#fafafa;border:none;height:22px}.mydp .headerbtn{width:16px}.mydp .headerlabelbtn{font-size:14px;outline:0;cursor:default}.mydp,.mydp .headertodaybtn{border:1px solid #ccc}.mydp .btnclear,.mydp .btndecrease,.mydp .btnincrease,.mydp .btnpicker,.mydp .headerbtn,.mydp .headermonthtxt,.mydp .headertodaybtn,.mydp .headeryeartxt,.mydp .yearchangebtn{color:#000}.mydp .headertodaybtn{padding:0 4px;font-size:11px;height:22px;min-width:60px;max-width:84px;overflow:hidden;white-space:nowrap}.mydp button::-moz-focus-inner{border:0}.mydp .headermonthtxt,.mydp .headeryeartxt{text-align:center;display:table-cell;vertical-align:middle;font-size:14px;height:26px;width:40px;max-width:40px;overflow:hidden;white-space:nowrap}.mydp .btnclear:focus,.mydp .btndecrease:focus,.mydp .btnincrease:focus,.mydp .btnpicker:focus,.mydp .headertodaybtn:focus{background:#add8e6}.mydp .headerbtn:focus,.mydp .monthlabel:focus,.mydp .yearchangebtn:focus,.mydp .yearlabel:focus{color:#add8e6;outline:0}.mydp .daycell:focus,.mydp .monthcell:focus,.mydp .yearcell:focus{outline:#ccc solid 1px}.mydp .icon-mydpcalendar,.mydp .icon-mydpremove{font-size:16px}.mydp .icon-mydpdown,.mydp .icon-mydpleft,.mydp .icon-mydpright,.mydp .icon-mydpup{color:#222;font-size:20px}.mydp .btndecrease .icon-mydpleft,.mydp .btnincrease .icon-mydpright{font-size:16px}.mydp .icon-mydptoday{color:#222;font-size:11px}.mydp table{display:table;border-spacing:0}.mydp table td{padding:0}.mydp table,.mydp td,.mydp th{border:none}.mydp .btnclearenabled:hover,.mydp .btndecreaseenabled:hover,.mydp .btnincreaseenabled:hover,.mydp .btnpickerenabled:hover,.mydp .headertodaybtnenabled:hover{background-color:#e6e6e6}.mydp .tablesingleday:hover,.mydp .tablesinglemonth:hover,.mydp .tablesingleyear:hover{background-color:#ddd}.mydp .daycell,.mydp .inputnoteditable,.mydp .monthcell,.mydp .monthlabel,.mydp .yearcell,.mydp .yearlabel{cursor:pointer}.mydp .headerbtnenabled:hover,.mydp .monthlabel:hover,.mydp .yearchangebtnenabled:hover,.mydp .yearlabel:hover{color:#777}@font-face{font-family:mydatepicker;src:url(data:application/octet-stream;base64,AAEAAAAPAIAAAwBwR1NVQiCMJXkAAAD8AAAAVE9TLzI+IEhNAAABUAAAAFZjbWFw6UKcfwAAAagAAAHEY3Z0IAbV/wQAAAz8AAAAIGZwZ22KkZBZAAANHAAAC3BnYXNwAAAAEAAADPQAAAAIZ2x5Zqbn7ycAAANsAAAFXGhlYWQNX0bLAAAIyAAAADZoaGVhBzwDWQAACQAAAAAkaG10eBXB//8AAAkkAAAAIGxvY2EGNATEAAAJRAAAABJtYXhwAXgMOgAACVgAAAAgbmFtZZKUFgMAAAl4AAAC/XBvc3R9NuZlAAAMeAAAAHpwcmVw5UErvAAAGIwAAACGAAEAAAAKADAAPgACbGF0bgAOREZMVAAaAAQAAAAAAAAAAQAAAAQAAAAAAAAAAQAAAAFsaWdhAAgAAAABAAAAAQAEAAQAAAABAAgAAQAGAAAAAQAAAAECuAGQAAUAAAJ6ArwAAACMAnoCvAAAAeAAMQECAAACAAUDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFBmRWQAQOgA6AYDUv9qAFoDUgCWAAAAAQAAAAAAAAAAAAUAAAADAAAALAAAAAQAAAFgAAEAAAAAAFoAAwABAAAALAADAAoAAAFgAAQALgAAAAQABAABAADoBv//AADoAP//AAAAAQAEAAAAAQACAAMABAAFAAYABwAAAQYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAAAAAAAZAAAAAAAAAAHAADoAAAA6AAAAAABAADoAQAA6AEAAAACAADoAgAA6AIAAAADAADoAwAA6AMAAAAEAADoBAAA6AQAAAAFAADoBQAA6AUAAAAGAADoBgAA6AYAAAAHAAEAAAAAAUECfQAOAAq3AAAAZhQBBRUrARQPAQYiJjURND4BHwEWAUEK+gscFhYcC/oKAV4OC/oLFg4B9A8UAgz6CgAAAQAAAAABZwJ8AA0AF0AUAAEAAQFHAAEAAW8AAABmFxMCBRYrAREUBiIvASY0PwE2MhYBZRQgCfoKCvoLHBgCWP4MDhYL+gscC/oLFgAAAAAFAAD/agOhA1IAFAAYACgAOABcALdAECoaAgoFMiICBgoNAQABA0dLsApQWEA/DgwCCgUGBgplAAIEAQQCAW0AAQAEAQBrAAADBAADawgBBgAEAgYEXwcBBQULWA0BCwsMSAADAwlYAAkJDQlJG0BADgwCCgUGBQoGbQACBAEEAgFtAAEABAEAawAAAwQAA2sIAQYABAIGBF8HAQUFC1gNAQsLDEgAAwMJWAAJCQ0JSVlAGFtZVlNQT0xJRkQ/PCYmJiQRFRQXEg8FHSsJAQYiLwEmND8BNjIfATc2Mh8BFhQBIREhNzU0JisBIgYdARQWOwEyNiU1NCYrASIGHQEUFjsBMjY3ERQGIyEiJjURNDY7ATU0NjsBMhYdATM1NDY7ATIWBxUzMhYC1/7iBQ4GoQUFGgUOBnv3Bg4GGQX9awMS/O7XCggkCAoKCCQICgGsCggjCAoKCCMICtcsHPzuHSoqHUg0JSQlNNY2JCMlNgFHHSoBOP7iBQWhBg4FGgUFe/gFBRoFDv5zAjxroQgKCgihCAoKCKEICgoIoQgKCiz9NR0qKh0Cyx0qNiU0NCU2NiU0NCU2KgAAAAAPAAD/agOhA1IAAwAHAAsADwATABcAGwAfACMAMwA3ADsAPwBPAHMAmECVQSUCHRJJLSQDEx0CRyEfAh0TCR1UGwETGRcNAwkIEwlfGBYMAwgVEQcDBQQIBV4UEAYDBA8LAwMBAAQBXhoBEhIeWCABHh4MSA4KAgMAABxYABwcDRxJcnBtamdmY2BdW1ZTTUxFRD8+PTw7Ojk4NzY1NDEvKScjIiEgHx4dHBsaGRgXFhUUExIRERERERERERAiBR0rFzM1IxczNSMnMzUjFzM1IyczNSMBMzUjJzM1IwEzNSMnMzUjAzU0JicjIgYHFRQWNzMyNgEzNSMnMzUjFzM1Izc1NCYnIyIGFxUUFjczMjY3ERQGIyEiJjURNDY7ATU0NjsBMhYdATM1NDY7ATIWBxUzMhZHoaHFsrLFoaHFsrLFoaEBm7Oz1rKyAayhodazs8QMBiQHCgEMBiQHCgGboaHWs7PWoaESCggjBwwBCggjCArXLBz87h0qKh1INCUkJTTWNiQjJTYBRx0qT6GhoSSysrIkof3Eofqh/cShJLIBMKEHCgEMBqEHDAEK/iayJKGhoWuhBwoBDAahBwwBCiz9NR0qKh0Cyx0qNiU0NCU2NiU0NCU2KgAAAAH//wAAAjsByQAOABFADgABAAFvAAAAZhUyAgUWKyUUBichIi4BPwE2Mh8BFgI7FA/+DA8UAgz6Ch4K+gqrDhYBFB4L+goK+gsAAAABAAAAAAI8Ae0ADgAXQBQAAQABAUcAAQABbwAAAGY1FAIFFisBFA8BBiIvASY0NjMhMhYCOwr6CxwL+gsWDgH0DhYByQ4L+gsL+gscFhYAAAEAAP/vAtQChgAkAB5AGyIZEAcEAAIBRwMBAgACbwEBAABmFBwUFAQFGCslFA8BBiIvAQcGIi8BJjQ/AScmND8BNjIfATc2Mh8BFhQPARcWAtQPTBAsEKSkECwQTBAQpKQQEEwQLBCkpBAsEEwPD6SkD3AWEEwPD6WlDw9MECwQpKQQLBBMEBCkpBAQTA8uD6SkDwABAAAAAQAAbdyczV8PPPUACwPoAAAAANUsgZUAAAAA1SyBlf///2oD6ANSAAAACAACAAAAAAAAAAEAAANS/2oAAAPo/////gPoAAEAAAAAAAAAAAAAAAAAAAAIA+gAAAFlAAABZQAAA+gAAAOgAAACO///AjsAAAMRAAAAAAAAACIASgEoAhYCPAJkAq4AAAABAAAACAB0AA8AAAAAAAIARABUAHMAAACpC3AAAAAAAAAAEgDeAAEAAAAAAAAANQAAAAEAAAAAAAEADAA1AAEAAAAAAAIABwBBAAEAAAAAAAMADABIAAEAAAAAAAQADABUAAEAAAAAAAUACwBgAAEAAAAAAAYADABrAAEAAAAAAAoAKwB3AAEAAAAAAAsAEwCiAAMAAQQJAAAAagC1AAMAAQQJAAEAGAEfAAMAAQQJAAIADgE3AAMAAQQJAAMAGAFFAAMAAQQJAAQAGAFdAAMAAQQJAAUAFgF1AAMAAQQJAAYAGAGLAAMAAQQJAAoAVgGjAAMAAQQJAAsAJgH5Q29weXJpZ2h0IChDKSAyMDE3IGJ5IG9yaWdpbmFsIGF1dGhvcnMgQCBmb250ZWxsby5jb21teWRhdGVwaWNrZXJSZWd1bGFybXlkYXRlcGlja2VybXlkYXRlcGlja2VyVmVyc2lvbiAxLjBteWRhdGVwaWNrZXJHZW5lcmF0ZWQgYnkgc3ZnMnR0ZiBmcm9tIEZvbnRlbGxvIHByb2plY3QuaHR0cDovL2ZvbnRlbGxvLmNvbQBDAG8AcAB5AHIAaQBnAGgAdAAgACgAQwApACAAMgAwADEANwAgAGIAeQAgAG8AcgBpAGcAaQBuAGEAbAAgAGEAdQB0AGgAbwByAHMAIABAACAAZgBvAG4AdABlAGwAbABvAC4AYwBvAG0AbQB5AGQAYQB0AGUAcABpAGMAawBlAHIAUgBlAGcAdQBsAGEAcgBtAHkAZABhAHQAZQBwAGkAYwBrAGUAcgBtAHkAZABhAHQAZQBwAGkAYwBrAGUAcgBWAGUAcgBzAGkAbwBuACAAMQAuADAAbQB5AGQAYQB0AGUAcABpAGMAawBlAHIARwBlAG4AZQByAGEAdABlAGQAIABiAHkAIABzAHYAZwAyAHQAdABmACAAZgByAG8AbQAgAEYAbwBuAHQAZQBsAGwAbwAgAHAAcgBvAGoAZQBjAHQALgBoAHQAdABwADoALwAvAGYAbwBuAHQAZQBsAGwAbwAuAGMAbwBtAAAAAAIAAAAAAAAACgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAECAQMBBAEFAQYBBwEIAQkACW15ZHByaWdodAhteWRwbGVmdAlteWRwdG9kYXkMbXlkcGNhbGVuZGFyBm15ZHB1cAhteWRwZG93bgpteWRwcmVtb3ZlAAAAAAABAAH//wAPAAAAAAAAAAAAAAAAAAAAAAAYABgAGAAYA1L/agNS/2qwACwgsABVWEVZICBLuAAOUUuwBlNaWLA0G7AoWWBmIIpVWLACJWG5CAAIAGNjI2IbISGwAFmwAEMjRLIAAQBDYEItsAEssCBgZi2wAiwgZCCwwFCwBCZasigBCkNFY0VSW1ghIyEbilggsFBQWCGwQFkbILA4UFghsDhZWSCxAQpDRWNFYWSwKFBYIbEBCkNFY0UgsDBQWCGwMFkbILDAUFggZiCKimEgsApQWGAbILAgUFghsApgGyCwNlBYIbA2YBtgWVlZG7ABK1lZI7AAUFhlWVktsAMsIEUgsAQlYWQgsAVDUFiwBSNCsAYjQhshIVmwAWAtsAQsIyEjISBksQViQiCwBiNCsQEKQ0VjsQEKQ7ABYEVjsAMqISCwBkMgiiCKsAErsTAFJbAEJlFYYFAbYVJZWCNZISCwQFNYsAErGyGwQFkjsABQWGVZLbAFLLAHQyuyAAIAQ2BCLbAGLLAHI0IjILAAI0JhsAJiZrABY7ABYLAFKi2wBywgIEUgsAtDY7gEAGIgsABQWLBAYFlmsAFjYESwAWAtsAgssgcLAENFQiohsgABAENgQi2wCSywAEMjRLIAAQBDYEItsAosICBFILABKyOwAEOwBCVgIEWKI2EgZCCwIFBYIbAAG7AwUFiwIBuwQFlZI7AAUFhlWbADJSNhRESwAWAtsAssICBFILABKyOwAEOwBCVgIEWKI2EgZLAkUFiwABuwQFkjsABQWGVZsAMlI2FERLABYC2wDCwgsAAjQrILCgNFWCEbIyFZKiEtsA0ssQICRbBkYUQtsA4ssAFgICCwDENKsABQWCCwDCNCWbANQ0qwAFJYILANI0JZLbAPLCCwEGJmsAFjILgEAGOKI2GwDkNgIIpgILAOI0IjLbAQLEtUWLEEZERZJLANZSN4LbARLEtRWEtTWLEEZERZGyFZJLATZSN4LbASLLEAD0NVWLEPD0OwAWFCsA8rWbAAQ7ACJUKxDAIlQrENAiVCsAEWIyCwAyVQWLEBAENgsAQlQoqKIIojYbAOKiEjsAFhIIojYbAOKiEbsQEAQ2CwAiVCsAIlYbAOKiFZsAxDR7ANQ0dgsAJiILAAUFiwQGBZZrABYyCwC0NjuAQAYiCwAFBYsEBgWWawAWNgsQAAEyNEsAFDsAA+sgEBAUNgQi2wEywAsQACRVRYsA8jQiBFsAsjQrAKI7ABYEIgYLABYbUQEAEADgBCQopgsRIGK7ByKxsiWS2wFCyxABMrLbAVLLEBEystsBYssQITKy2wFyyxAxMrLbAYLLEEEystsBkssQUTKy2wGiyxBhMrLbAbLLEHEystsBwssQgTKy2wHSyxCRMrLbAeLACwDSuxAAJFVFiwDyNCIEWwCyNCsAojsAFgQiBgsAFhtRAQAQAOAEJCimCxEgYrsHIrGyJZLbAfLLEAHistsCAssQEeKy2wISyxAh4rLbAiLLEDHistsCMssQQeKy2wJCyxBR4rLbAlLLEGHistsCYssQceKy2wJyyxCB4rLbAoLLEJHistsCksIDywAWAtsCosIGCwEGAgQyOwAWBDsAIlYbABYLApKiEtsCsssCorsCoqLbAsLCAgRyAgsAtDY7gEAGIgsABQWLBAYFlmsAFjYCNhOCMgilVYIEcgILALQ2O4BABiILAAUFiwQGBZZrABY2AjYTgbIVktsC0sALEAAkVUWLABFrAsKrABFTAbIlktsC4sALANK7EAAkVUWLABFrAsKrABFTAbIlktsC8sIDWwAWAtsDAsALABRWO4BABiILAAUFiwQGBZZrABY7ABK7ALQ2O4BABiILAAUFiwQGBZZrABY7ABK7AAFrQAAAAAAEQ+IzixLwEVKi2wMSwgPCBHILALQ2O4BABiILAAUFiwQGBZZrABY2CwAENhOC2wMiwuFzwtsDMsIDwgRyCwC0NjuAQAYiCwAFBYsEBgWWawAWNgsABDYbABQ2M4LbA0LLECABYlIC4gR7AAI0KwAiVJiopHI0cjYSBYYhshWbABI0KyMwEBFRQqLbA1LLAAFrAEJbAEJUcjRyNhsAlDK2WKLiMgIDyKOC2wNiywABawBCWwBCUgLkcjRyNhILAEI0KwCUMrILBgUFggsEBRWLMCIAMgG7MCJgMaWUJCIyCwCEMgiiNHI0cjYSNGYLAEQ7ACYiCwAFBYsEBgWWawAWNgILABKyCKimEgsAJDYGQjsANDYWRQWLACQ2EbsANDYFmwAyWwAmIgsABQWLBAYFlmsAFjYSMgILAEJiNGYTgbI7AIQ0awAiWwCENHI0cjYWAgsARDsAJiILAAUFiwQGBZZrABY2AjILABKyOwBENgsAErsAUlYbAFJbACYiCwAFBYsEBgWWawAWOwBCZhILAEJWBkI7ADJWBkUFghGyMhWSMgILAEJiNGYThZLbA3LLAAFiAgILAFJiAuRyNHI2EjPDgtsDgssAAWILAII0IgICBGI0ewASsjYTgtsDkssAAWsAMlsAIlRyNHI2GwAFRYLiA8IyEbsAIlsAIlRyNHI2EgsAUlsAQlRyNHI2GwBiWwBSVJsAIlYbkIAAgAY2MjIFhiGyFZY7gEAGIgsABQWLBAYFlmsAFjYCMuIyAgPIo4IyFZLbA6LLAAFiCwCEMgLkcjRyNhIGCwIGBmsAJiILAAUFiwQGBZZrABYyMgIDyKOC2wOywjIC5GsAIlRlJYIDxZLrErARQrLbA8LCMgLkawAiVGUFggPFkusSsBFCstsD0sIyAuRrACJUZSWCA8WSMgLkawAiVGUFggPFkusSsBFCstsD4ssDUrIyAuRrACJUZSWCA8WS6xKwEUKy2wPyywNiuKICA8sAQjQoo4IyAuRrACJUZSWCA8WS6xKwEUK7AEQy6wKystsEAssAAWsAQlsAQmIC5HI0cjYbAJQysjIDwgLiM4sSsBFCstsEEssQgEJUKwABawBCWwBCUgLkcjRyNhILAEI0KwCUMrILBgUFggsEBRWLMCIAMgG7MCJgMaWUJCIyBHsARDsAJiILAAUFiwQGBZZrABY2AgsAErIIqKYSCwAkNgZCOwA0NhZFBYsAJDYRuwA0NgWbADJbACYiCwAFBYsEBgWWawAWNhsAIlRmE4IyA8IzgbISAgRiNHsAErI2E4IVmxKwEUKy2wQiywNSsusSsBFCstsEMssDYrISMgIDywBCNCIzixKwEUK7AEQy6wKystsEQssAAVIEewACNCsgABARUUEy6wMSotsEUssAAVIEewACNCsgABARUUEy6wMSotsEYssQABFBOwMiotsEcssDQqLbBILLAAFkUjIC4gRoojYTixKwEUKy2wSSywCCNCsEgrLbBKLLIAAEErLbBLLLIAAUErLbBMLLIBAEErLbBNLLIBAUErLbBOLLIAAEIrLbBPLLIAAUIrLbBQLLIBAEIrLbBRLLIBAUIrLbBSLLIAAD4rLbBTLLIAAT4rLbBULLIBAD4rLbBVLLIBAT4rLbBWLLIAAEArLbBXLLIAAUArLbBYLLIBAEArLbBZLLIBAUArLbBaLLIAAEMrLbBbLLIAAUMrLbBcLLIBAEMrLbBdLLIBAUMrLbBeLLIAAD8rLbBfLLIAAT8rLbBgLLIBAD8rLbBhLLIBAT8rLbBiLLA3Ky6xKwEUKy2wYyywNyuwOystsGQssDcrsDwrLbBlLLAAFrA3K7A9Ky2wZiywOCsusSsBFCstsGcssDgrsDsrLbBoLLA4K7A8Ky2waSywOCuwPSstsGossDkrLrErARQrLbBrLLA5K7A7Ky2wbCywOSuwPCstsG0ssDkrsD0rLbBuLLA6Ky6xKwEUKy2wbyywOiuwOystsHAssDorsDwrLbBxLLA6K7A9Ky2wciyzCQQCA0VYIRsjIVlCK7AIZbADJFB4sAEVMC0AS7gAyFJYsQEBjlmwAbkIAAgAY3CxAAVCsgABACqxAAVCswoCAQgqsQAFQrMOAAEIKrEABkK6AsAAAQAJKrEAB0K6AEAAAQAJKrEDAESxJAGIUViwQIhYsQNkRLEmAYhRWLoIgAABBECIY1RYsQMARFlZWVmzDAIBDCq4Af+FsASNsQIARAAA) format('truetype');font-weight:400;font-style:normal}.mydp .mydpicon{font-family:mydatepicker;font-style:normal;font-weight:400;font-variant:normal;text-transform:none;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.mydp .icon-mydpright:before{content:\"\\e800\"}.mydp .icon-mydpleft:before{content:\"\\e801\"}.mydp .icon-mydptoday:before{content:\"\\e802\"}.mydp .icon-mydpcalendar:before{content:\"\\e803\"}.mydp .icon-mydpup:before{content:\"\\e804\"}.mydp .icon-mydpdown:before{content:\"\\e805\"}.mydp .icon-mydpremove:before{content:\"\\e806\"}"]
                    }] }
        ];
        /** @nocollapse */
        MyDatePicker.ctorParameters = function () { return [
            { type: core.ElementRef },
            { type: core.Renderer },
            { type: core.ChangeDetectorRef },
            { type: LocaleService },
            { type: UtilService }
        ]; };
        MyDatePicker.propDecorators = {
            options: [{ type: core.Input }],
            locale: [{ type: core.Input }],
            defaultMonth: [{ type: core.Input }],
            selDate: [{ type: core.Input }],
            placeholder: [{ type: core.Input }],
            selector: [{ type: core.Input }],
            disabled: [{ type: core.Input }],
            dateChanged: [{ type: core.Output }],
            inputFieldChanged: [{ type: core.Output }],
            calendarViewChanged: [{ type: core.Output }],
            calendarToggle: [{ type: core.Output }],
            inputFocusBlur: [{ type: core.Output }],
            selectorEl: [{ type: core.ViewChild, args: ["selectorEl", { static: false },] }],
            inputBoxEl: [{ type: core.ViewChild, args: ["inputBoxEl", { static: false },] }]
        };
        return MyDatePicker;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var FocusDirective = /** @class */ (function () {
        function FocusDirective(el, renderer) {
            this.el = el;
            this.renderer = renderer;
        }
        // Focus to element: if value 0 = don't set focus, 1 = set focus
        // Focus to element: if value 0 = don't set focus, 1 = set focus
        /**
         * @return {?}
         */
        FocusDirective.prototype.ngAfterViewInit = 
        // Focus to element: if value 0 = don't set focus, 1 = set focus
        /**
         * @return {?}
         */
        function () {
            if (this.value === "0") {
                return;
            }
            this.renderer.invokeElementMethod(this.el.nativeElement, "focus", []);
        };
        FocusDirective.decorators = [
            { type: core.Directive, args: [{
                        selector: "[mydpfocus]"
                    },] }
        ];
        /** @nocollapse */
        FocusDirective.ctorParameters = function () { return [
            { type: core.ElementRef },
            { type: core.Renderer }
        ]; };
        FocusDirective.propDecorators = {
            value: [{ type: core.Input, args: ["mydpfocus",] }]
        };
        return FocusDirective;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MyDatePickerModule = /** @class */ (function () {
        function MyDatePickerModule() {
        }
        MyDatePickerModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [common.CommonModule, forms.FormsModule, angular2TextMask.TextMaskModule],
                        declarations: [MyDatePicker, FocusDirective],
                        exports: [MyDatePicker, FocusDirective]
                    },] }
        ];
        return MyDatePickerModule;
    }());

    exports.FocusDirective = FocusDirective;
    exports.LocaleService = LocaleService;
    exports.MYDP_VALUE_ACCESSOR = MYDP_VALUE_ACCESSOR;
    exports.MyDatePicker = MyDatePicker;
    exports.MyDatePickerModule = MyDatePickerModule;
    exports.MydatepickerService = MydatepickerService;
    exports.UtilService = UtilService;

    Object.defineProperty(exports, '__esModule', { value: true });

}));
//# sourceMappingURL=mydatepicker.umd.js.map

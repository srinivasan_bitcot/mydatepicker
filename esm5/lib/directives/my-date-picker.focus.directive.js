/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, Renderer, Input } from "@angular/core";
var FocusDirective = /** @class */ (function () {
    function FocusDirective(el, renderer) {
        this.el = el;
        this.renderer = renderer;
    }
    // Focus to element: if value 0 = don't set focus, 1 = set focus
    // Focus to element: if value 0 = don't set focus, 1 = set focus
    /**
     * @return {?}
     */
    FocusDirective.prototype.ngAfterViewInit = 
    // Focus to element: if value 0 = don't set focus, 1 = set focus
    /**
     * @return {?}
     */
    function () {
        if (this.value === "0") {
            return;
        }
        this.renderer.invokeElementMethod(this.el.nativeElement, "focus", []);
    };
    FocusDirective.decorators = [
        { type: Directive, args: [{
                    selector: "[mydpfocus]"
                },] }
    ];
    /** @nocollapse */
    FocusDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer }
    ]; };
    FocusDirective.propDecorators = {
        value: [{ type: Input, args: ["mydpfocus",] }]
    };
    return FocusDirective;
}());
export { FocusDirective };
if (false) {
    /** @type {?} */
    FocusDirective.prototype.value;
    /**
     * @type {?}
     * @private
     */
    FocusDirective.prototype.el;
    /**
     * @type {?}
     * @private
     */
    FocusDirective.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktZGF0ZS1waWNrZXIuZm9jdXMuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXlkYXRlcGlja2VyLyIsInNvdXJjZXMiOlsibGliL2RpcmVjdGl2ZXMvbXktZGF0ZS1waWNrZXIuZm9jdXMuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQWlCLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV0RjtJQU9JLHdCQUFvQixFQUFjLEVBQVUsUUFBa0I7UUFBMUMsT0FBRSxHQUFGLEVBQUUsQ0FBWTtRQUFVLGFBQVEsR0FBUixRQUFRLENBQVU7SUFBRyxDQUFDO0lBRWxFLGdFQUFnRTs7Ozs7SUFDaEUsd0NBQWU7Ozs7O0lBQWY7UUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssR0FBRyxFQUFFO1lBQ3BCLE9BQU87U0FDVjtRQUNELElBQUksQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQzFFLENBQUM7O2dCQWZKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsYUFBYTtpQkFDMUI7Ozs7Z0JBSm1CLFVBQVU7Z0JBQUUsUUFBUTs7O3dCQU9uQyxLQUFLLFNBQUMsV0FBVzs7SUFXdEIscUJBQUM7Q0FBQSxBQWhCRCxJQWdCQztTQVpZLGNBQWM7OztJQUN2QiwrQkFBa0M7Ozs7O0lBRXRCLDRCQUFzQjs7Ozs7SUFBRSxrQ0FBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIFJlbmRlcmVyLCBBZnRlclZpZXdJbml0LCBJbnB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbkBEaXJlY3RpdmUoe1xuICAgIHNlbGVjdG9yOiBcIltteWRwZm9jdXNdXCJcbn0pXG5cbmV4cG9ydCBjbGFzcyBGb2N1c0RpcmVjdGl2ZSBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQge1xuICAgIEBJbnB1dChcIm15ZHBmb2N1c1wiKSB2YWx1ZTogc3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBlbDogRWxlbWVudFJlZiwgcHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIpIHt9XG5cbiAgICAvLyBGb2N1cyB0byBlbGVtZW50OiBpZiB2YWx1ZSAwID0gZG9uJ3Qgc2V0IGZvY3VzLCAxID0gc2V0IGZvY3VzXG4gICAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgICAgICBpZiAodGhpcy52YWx1ZSA9PT0gXCIwXCIpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnJlbmRlcmVyLmludm9rZUVsZW1lbnRNZXRob2QodGhpcy5lbC5uYXRpdmVFbGVtZW50LCBcImZvY3VzXCIsIFtdKTtcbiAgICB9XG59Il19
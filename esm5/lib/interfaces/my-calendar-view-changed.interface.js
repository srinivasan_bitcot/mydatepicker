/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IMyCalendarViewChanged() { }
if (false) {
    /** @type {?} */
    IMyCalendarViewChanged.prototype.year;
    /** @type {?} */
    IMyCalendarViewChanged.prototype.month;
    /** @type {?} */
    IMyCalendarViewChanged.prototype.first;
    /** @type {?} */
    IMyCalendarViewChanged.prototype.last;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktY2FsZW5kYXItdmlldy1jaGFuZ2VkLmludGVyZmFjZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL215ZGF0ZXBpY2tlci8iLCJzb3VyY2VzIjpbImxpYi9pbnRlcmZhY2VzL215LWNhbGVuZGFyLXZpZXctY2hhbmdlZC5pbnRlcmZhY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUVBLDRDQUtDOzs7SUFKRyxzQ0FBYTs7SUFDYix1Q0FBYzs7SUFDZCx1Q0FBa0I7O0lBQ2xCLHNDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElNeVdlZWtkYXkgfSBmcm9tIFwiLi9teS13ZWVrZGF5LmludGVyZmFjZVwiO1xuXG5leHBvcnQgaW50ZXJmYWNlIElNeUNhbGVuZGFyVmlld0NoYW5nZWQge1xuICAgIHllYXI6IG51bWJlcjtcbiAgICBtb250aDogbnVtYmVyO1xuICAgIGZpcnN0OiBJTXlXZWVrZGF5O1xuICAgIGxhc3Q6IElNeVdlZWtkYXk7XG59XG4iXX0=
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IMyOptions() { }
if (false) {
    /** @type {?|undefined} */
    IMyOptions.prototype.dayLabels;
    /** @type {?|undefined} */
    IMyOptions.prototype.monthLabels;
    /** @type {?|undefined} */
    IMyOptions.prototype.dateFormat;
    /** @type {?|undefined} */
    IMyOptions.prototype.showTodayBtn;
    /** @type {?|undefined} */
    IMyOptions.prototype.todayBtnTxt;
    /** @type {?|undefined} */
    IMyOptions.prototype.firstDayOfWeek;
    /** @type {?|undefined} */
    IMyOptions.prototype.satHighlight;
    /** @type {?|undefined} */
    IMyOptions.prototype.sunHighlight;
    /** @type {?|undefined} */
    IMyOptions.prototype.highlightDates;
    /** @type {?|undefined} */
    IMyOptions.prototype.markCurrentDay;
    /** @type {?|undefined} */
    IMyOptions.prototype.markCurrentMonth;
    /** @type {?|undefined} */
    IMyOptions.prototype.markCurrentYear;
    /** @type {?|undefined} */
    IMyOptions.prototype.disableUntil;
    /** @type {?|undefined} */
    IMyOptions.prototype.disableSince;
    /** @type {?|undefined} */
    IMyOptions.prototype.disableDays;
    /** @type {?|undefined} */
    IMyOptions.prototype.enableDays;
    /** @type {?|undefined} */
    IMyOptions.prototype.markDates;
    /** @type {?|undefined} */
    IMyOptions.prototype.markWeekends;
    /** @type {?|undefined} */
    IMyOptions.prototype.disableDateRanges;
    /** @type {?|undefined} */
    IMyOptions.prototype.disableWeekends;
    /** @type {?|undefined} */
    IMyOptions.prototype.disableWeekdays;
    /** @type {?|undefined} */
    IMyOptions.prototype.showWeekNumbers;
    /** @type {?|undefined} */
    IMyOptions.prototype.height;
    /** @type {?|undefined} */
    IMyOptions.prototype.width;
    /** @type {?|undefined} */
    IMyOptions.prototype.selectionTxtFontSize;
    /** @type {?|undefined} */
    IMyOptions.prototype.selectorHeight;
    /** @type {?|undefined} */
    IMyOptions.prototype.selectorWidth;
    /** @type {?|undefined} */
    IMyOptions.prototype.allowDeselectDate;
    /** @type {?|undefined} */
    IMyOptions.prototype.inline;
    /** @type {?|undefined} */
    IMyOptions.prototype.showClearDateBtn;
    /** @type {?|undefined} */
    IMyOptions.prototype.showDecreaseDateBtn;
    /** @type {?|undefined} */
    IMyOptions.prototype.showIncreaseDateBtn;
    /** @type {?|undefined} */
    IMyOptions.prototype.alignSelectorRight;
    /** @type {?|undefined} */
    IMyOptions.prototype.openSelectorTopOfInput;
    /** @type {?|undefined} */
    IMyOptions.prototype.indicateInvalidDate;
    /** @type {?|undefined} */
    IMyOptions.prototype.editableDateField;
    /** @type {?|undefined} */
    IMyOptions.prototype.monthSelector;
    /** @type {?|undefined} */
    IMyOptions.prototype.yearSelector;
    /** @type {?|undefined} */
    IMyOptions.prototype.disableHeaderButtons;
    /** @type {?|undefined} */
    IMyOptions.prototype.minYear;
    /** @type {?|undefined} */
    IMyOptions.prototype.maxYear;
    /** @type {?|undefined} */
    IMyOptions.prototype.componentDisabled;
    /** @type {?|undefined} */
    IMyOptions.prototype.showSelectorArrow;
    /** @type {?|undefined} */
    IMyOptions.prototype.showInputField;
    /** @type {?|undefined} */
    IMyOptions.prototype.openSelectorOnInputClick;
    /** @type {?|undefined} */
    IMyOptions.prototype.allowSelectionOnlyInCurrentMonth;
    /** @type {?|undefined} */
    IMyOptions.prototype.ariaLabelInputField;
    /** @type {?|undefined} */
    IMyOptions.prototype.ariaLabelClearDate;
    /** @type {?|undefined} */
    IMyOptions.prototype.ariaLabelDecreaseDate;
    /** @type {?|undefined} */
    IMyOptions.prototype.ariaLabelIncreaseDate;
    /** @type {?|undefined} */
    IMyOptions.prototype.ariaLabelOpenCalendar;
    /** @type {?|undefined} */
    IMyOptions.prototype.ariaLabelPrevMonth;
    /** @type {?|undefined} */
    IMyOptions.prototype.ariaLabelNextMonth;
    /** @type {?|undefined} */
    IMyOptions.prototype.ariaLabelPrevYear;
    /** @type {?|undefined} */
    IMyOptions.prototype.ariaLabelNextYear;
    /** @type {?|undefined} */
    IMyOptions.prototype.dateMaskRegex;
}
/**
 * @record
 */
export function IMyDpOptions() { }
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktb3B0aW9ucy5pbnRlcmZhY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teWRhdGVwaWNrZXIvIiwic291cmNlcyI6WyJsaWIvaW50ZXJmYWNlcy9teS1vcHRpb25zLmludGVyZmFjZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBT0EsZ0NBeURDOzs7SUF4REcsK0JBQXlCOztJQUN6QixpQ0FBNkI7O0lBQzdCLGdDQUFvQjs7SUFDcEIsa0NBQXVCOztJQUN2QixpQ0FBcUI7O0lBQ3JCLG9DQUF3Qjs7SUFDeEIsa0NBQXVCOztJQUN2QixrQ0FBdUI7O0lBQ3ZCLG9DQUFnQzs7SUFDaEMsb0NBQXlCOztJQUN6QixzQ0FBMkI7O0lBQzNCLHFDQUEwQjs7SUFDMUIsa0NBQXVCOztJQUN2QixrQ0FBdUI7O0lBQ3ZCLGlDQUE2Qjs7SUFDN0IsZ0NBQTRCOztJQUM1QiwrQkFBa0M7O0lBQ2xDLGtDQUE2Qjs7SUFDN0IsdUNBQXdDOztJQUN4QyxxQ0FBMEI7O0lBQzFCLHFDQUFnQzs7SUFDaEMscUNBQTBCOztJQUMxQiw0QkFBZ0I7O0lBQ2hCLDJCQUFlOztJQUNmLDBDQUE4Qjs7SUFDOUIsb0NBQXdCOztJQUN4QixtQ0FBdUI7O0lBQ3ZCLHVDQUE0Qjs7SUFDNUIsNEJBQWlCOztJQUNqQixzQ0FBMkI7O0lBQzNCLHlDQUE4Qjs7SUFDOUIseUNBQThCOztJQUM5Qix3Q0FBNkI7O0lBQzdCLDRDQUFpQzs7SUFDakMseUNBQThCOztJQUM5Qix1Q0FBNEI7O0lBQzVCLG1DQUF3Qjs7SUFDeEIsa0NBQXVCOztJQUN2QiwwQ0FBK0I7O0lBQy9CLDZCQUFpQjs7SUFDakIsNkJBQWlCOztJQUNqQix1Q0FBNEI7O0lBQzVCLHVDQUE0Qjs7SUFDNUIsb0NBQXlCOztJQUN6Qiw4Q0FBbUM7O0lBQ25DLHNEQUEyQzs7SUFDM0MseUNBQTZCOztJQUM3Qix3Q0FBNEI7O0lBQzVCLDJDQUErQjs7SUFDL0IsMkNBQStCOztJQUMvQiwyQ0FBK0I7O0lBQy9CLHdDQUE0Qjs7SUFDNUIsd0NBQTRCOztJQUM1Qix1Q0FBMkI7O0lBQzNCLHVDQUEyQjs7SUFDM0IsbUNBQW1COzs7OztBQUd2QixrQ0FBbUQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJTXlEYXlMYWJlbHMgfSBmcm9tIFwiLi9teS1kYXktbGFiZWxzLmludGVyZmFjZVwiO1xuaW1wb3J0IHsgSU15TW9udGhMYWJlbHMgfSBmcm9tIFwiLi9teS1tb250aC1sYWJlbHMuaW50ZXJmYWNlXCI7XG5pbXBvcnQgeyBJTXlEYXRlIH0gZnJvbSBcIi4vbXktZGF0ZS5pbnRlcmZhY2VcIjtcbmltcG9ydCB7IElNeURhdGVSYW5nZSB9IGZyb20gXCIuL215LWRhdGUtcmFuZ2UuaW50ZXJmYWNlXCI7XG5pbXBvcnQgeyBJTXlNYXJrZWREYXRlcyB9IGZyb20gXCIuL215LW1hcmtlZC1kYXRlcy5pbnRlcmZhY2VcIjtcbmltcG9ydCB7IElNeU1hcmtlZERhdGUgfSBmcm9tIFwiLi9teS1tYXJrZWQtZGF0ZS5pbnRlcmZhY2VcIjtcblxuZXhwb3J0IGludGVyZmFjZSBJTXlPcHRpb25zIHtcbiAgICBkYXlMYWJlbHM/OiBJTXlEYXlMYWJlbHM7XG4gICAgbW9udGhMYWJlbHM/OiBJTXlNb250aExhYmVscztcbiAgICBkYXRlRm9ybWF0Pzogc3RyaW5nO1xuICAgIHNob3dUb2RheUJ0bj86IGJvb2xlYW47XG4gICAgdG9kYXlCdG5UeHQ/OiBzdHJpbmc7XG4gICAgZmlyc3REYXlPZldlZWs/OiBzdHJpbmc7XG4gICAgc2F0SGlnaGxpZ2h0PzogYm9vbGVhbjtcbiAgICBzdW5IaWdobGlnaHQ/OiBib29sZWFuO1xuICAgIGhpZ2hsaWdodERhdGVzPzogQXJyYXk8SU15RGF0ZT47XG4gICAgbWFya0N1cnJlbnREYXk/OiBib29sZWFuO1xuICAgIG1hcmtDdXJyZW50TW9udGg/OiBib29sZWFuO1xuICAgIG1hcmtDdXJyZW50WWVhcj86IGJvb2xlYW47XG4gICAgZGlzYWJsZVVudGlsPzogSU15RGF0ZTtcbiAgICBkaXNhYmxlU2luY2U/OiBJTXlEYXRlO1xuICAgIGRpc2FibGVEYXlzPzogQXJyYXk8SU15RGF0ZT47XG4gICAgZW5hYmxlRGF5cz86IEFycmF5PElNeURhdGU+O1xuICAgIG1hcmtEYXRlcz86IEFycmF5PElNeU1hcmtlZERhdGVzPjtcbiAgICBtYXJrV2Vla2VuZHM/OiBJTXlNYXJrZWREYXRlO1xuICAgIGRpc2FibGVEYXRlUmFuZ2VzPzogQXJyYXk8SU15RGF0ZVJhbmdlPjtcbiAgICBkaXNhYmxlV2Vla2VuZHM/OiBib29sZWFuO1xuICAgIGRpc2FibGVXZWVrZGF5cz86IEFycmF5PHN0cmluZz47XG4gICAgc2hvd1dlZWtOdW1iZXJzPzogYm9vbGVhbjtcbiAgICBoZWlnaHQ/OiBzdHJpbmc7XG4gICAgd2lkdGg/OiBzdHJpbmc7XG4gICAgc2VsZWN0aW9uVHh0Rm9udFNpemU/OiBzdHJpbmc7XG4gICAgc2VsZWN0b3JIZWlnaHQ/OiBzdHJpbmc7XG4gICAgc2VsZWN0b3JXaWR0aD86IHN0cmluZztcbiAgICBhbGxvd0Rlc2VsZWN0RGF0ZT86IGJvb2xlYW47XG4gICAgaW5saW5lPzogYm9vbGVhbjtcbiAgICBzaG93Q2xlYXJEYXRlQnRuPzogYm9vbGVhbjtcbiAgICBzaG93RGVjcmVhc2VEYXRlQnRuPzogYm9vbGVhbjtcbiAgICBzaG93SW5jcmVhc2VEYXRlQnRuPzogYm9vbGVhbjtcbiAgICBhbGlnblNlbGVjdG9yUmlnaHQ/OiBib29sZWFuO1xuICAgIG9wZW5TZWxlY3RvclRvcE9mSW5wdXQ/OiBib29sZWFuO1xuICAgIGluZGljYXRlSW52YWxpZERhdGU/OiBib29sZWFuO1xuICAgIGVkaXRhYmxlRGF0ZUZpZWxkPzogYm9vbGVhbjtcbiAgICBtb250aFNlbGVjdG9yPzogYm9vbGVhbjtcbiAgICB5ZWFyU2VsZWN0b3I/OiBib29sZWFuO1xuICAgIGRpc2FibGVIZWFkZXJCdXR0b25zPzogYm9vbGVhbjtcbiAgICBtaW5ZZWFyPzogbnVtYmVyO1xuICAgIG1heFllYXI/OiBudW1iZXI7XG4gICAgY29tcG9uZW50RGlzYWJsZWQ/OiBib29sZWFuO1xuICAgIHNob3dTZWxlY3RvckFycm93PzogYm9vbGVhbjtcbiAgICBzaG93SW5wdXRGaWVsZD86IGJvb2xlYW47XG4gICAgb3BlblNlbGVjdG9yT25JbnB1dENsaWNrPzogYm9vbGVhbjtcbiAgICBhbGxvd1NlbGVjdGlvbk9ubHlJbkN1cnJlbnRNb250aD86IGJvb2xlYW47XG4gICAgYXJpYUxhYmVsSW5wdXRGaWVsZD86IHN0cmluZztcbiAgICBhcmlhTGFiZWxDbGVhckRhdGU/OiBzdHJpbmc7XG4gICAgYXJpYUxhYmVsRGVjcmVhc2VEYXRlPzogc3RyaW5nO1xuICAgIGFyaWFMYWJlbEluY3JlYXNlRGF0ZT86IHN0cmluZztcbiAgICBhcmlhTGFiZWxPcGVuQ2FsZW5kYXI/OiBzdHJpbmc7XG4gICAgYXJpYUxhYmVsUHJldk1vbnRoPzogc3RyaW5nO1xuICAgIGFyaWFMYWJlbE5leHRNb250aD86IHN0cmluZztcbiAgICBhcmlhTGFiZWxQcmV2WWVhcj86IHN0cmluZztcbiAgICBhcmlhTGFiZWxOZXh0WWVhcj86IHN0cmluZztcbiAgICBkYXRlTWFza1JlZ2V4Pzphbnk7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSU15RHBPcHRpb25zIGV4dGVuZHMgSU15T3B0aW9ucyB7fSJdfQ==
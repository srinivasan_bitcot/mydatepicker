/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
/** @type {?} */
var M = "m";
/** @type {?} */
var MM = "mm";
/** @type {?} */
var MMM = "mmm";
/** @type {?} */
var D = "d";
/** @type {?} */
var DD = "dd";
/** @type {?} */
var YYYY = "yyyy";
var UtilService = /** @class */ (function () {
    function UtilService() {
        this.weekDays = ["su", "mo", "tu", "we", "th", "fr", "sa"];
    }
    /**
     * @param {?} dateStr
     * @param {?} dateFormat
     * @param {?} minYear
     * @param {?} maxYear
     * @param {?} disableUntil
     * @param {?} disableSince
     * @param {?} disableWeekends
     * @param {?} disableWeekDays
     * @param {?} disableDays
     * @param {?} disableDateRanges
     * @param {?} monthLabels
     * @param {?} enableDays
     * @return {?}
     */
    UtilService.prototype.isDateValid = /**
     * @param {?} dateStr
     * @param {?} dateFormat
     * @param {?} minYear
     * @param {?} maxYear
     * @param {?} disableUntil
     * @param {?} disableSince
     * @param {?} disableWeekends
     * @param {?} disableWeekDays
     * @param {?} disableDays
     * @param {?} disableDateRanges
     * @param {?} monthLabels
     * @param {?} enableDays
     * @return {?}
     */
    function (dateStr, dateFormat, minYear, maxYear, disableUntil, disableSince, disableWeekends, disableWeekDays, disableDays, disableDateRanges, monthLabels, enableDays) {
        /** @type {?} */
        var returnDate = { day: 0, month: 0, year: 0 };
        /** @type {?} */
        var daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        /** @type {?} */
        var isMonthStr = dateFormat.indexOf(MMM) !== -1;
        /** @type {?} */
        var delimeters = this.getDateFormatDelimeters(dateFormat);
        /** @type {?} */
        var dateValue = this.getDateValue(dateStr, dateFormat, delimeters);
        /** @type {?} */
        var year = this.getNumberByValue(dateValue[0]);
        /** @type {?} */
        var month = isMonthStr ? this.getMonthNumberByMonthName(dateValue[1], monthLabels) : this.getNumberByValue(dateValue[1]);
        /** @type {?} */
        var day = this.getNumberByValue(dateValue[2]);
        if (month !== -1 && day !== -1 && year !== -1) {
            if (year < minYear || year > maxYear || month < 1 || month > 12) {
                return returnDate;
            }
            /** @type {?} */
            var date = { year: year, month: month, day: day };
            if (this.isDisabledDay(date, minYear, maxYear, disableUntil, disableSince, disableWeekends, disableWeekDays, disableDays, disableDateRanges, enableDays)) {
                return returnDate;
            }
            if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
                daysInMonth[1] = 29;
            }
            if (day < 1 || day > daysInMonth[month - 1]) {
                return returnDate;
            }
            // Valid date
            return date;
        }
        return returnDate;
    };
    /**
     * @param {?} dateStr
     * @param {?} dateFormat
     * @param {?} delimeters
     * @return {?}
     */
    UtilService.prototype.getDateValue = /**
     * @param {?} dateStr
     * @param {?} dateFormat
     * @param {?} delimeters
     * @return {?}
     */
    function (dateStr, dateFormat, delimeters) {
        /** @type {?} */
        var del = delimeters[0];
        if (delimeters[0] !== delimeters[1]) {
            del = delimeters[0] + delimeters[1];
        }
        /** @type {?} */
        var re = new RegExp("[" + del + "]");
        /** @type {?} */
        var ds = dateStr.split(re);
        /** @type {?} */
        var df = dateFormat.split(re);
        /** @type {?} */
        var da = [];
        for (var i = 0; i < df.length; i++) {
            if (df[i].indexOf(YYYY) !== -1) {
                da[0] = { value: ds[i], format: df[i] };
            }
            if (df[i].indexOf(M) !== -1) {
                da[1] = { value: ds[i], format: df[i] };
            }
            if (df[i].indexOf(D) !== -1) {
                da[2] = { value: ds[i], format: df[i] };
            }
        }
        return da;
    };
    /**
     * @param {?} df
     * @param {?} monthLabels
     * @return {?}
     */
    UtilService.prototype.getMonthNumberByMonthName = /**
     * @param {?} df
     * @param {?} monthLabels
     * @return {?}
     */
    function (df, monthLabels) {
        if (df.value) {
            for (var key = 1; key <= 12; key++) {
                if (df.value.toLowerCase() === monthLabels[key].toLowerCase()) {
                    return key;
                }
            }
        }
        return -1;
    };
    /**
     * @param {?} df
     * @return {?}
     */
    UtilService.prototype.getNumberByValue = /**
     * @param {?} df
     * @return {?}
     */
    function (df) {
        if (!/^\d+$/.test(df.value)) {
            return -1;
        }
        /** @type {?} */
        var nbr = Number(df.value);
        if (df.format.length === 1 && df.value.length !== 1 && nbr < 10 || df.format.length === 1 && df.value.length !== 2 && nbr >= 10) {
            nbr = -1;
        }
        else if (df.format.length === 2 && df.value.length > 2) {
            nbr = -1;
        }
        return nbr;
    };
    /**
     * @param {?} dateFormat
     * @return {?}
     */
    UtilService.prototype.getDateFormatDelimeters = /**
     * @param {?} dateFormat
     * @return {?}
     */
    function (dateFormat) {
        return dateFormat.match(/[^(dmy)]{1,}/g);
    };
    /**
     * @param {?} monthString
     * @return {?}
     */
    UtilService.prototype.parseDefaultMonth = /**
     * @param {?} monthString
     * @return {?}
     */
    function (monthString) {
        /** @type {?} */
        var month = { monthTxt: "", monthNbr: 0, year: 0 };
        if (monthString !== "") {
            /** @type {?} */
            var split = monthString.split(monthString.match(/[^0-9]/)[0]);
            month.monthNbr = split[0].length === 2 ? parseInt(split[0]) : parseInt(split[1]);
            month.year = split[0].length === 2 ? parseInt(split[1]) : parseInt(split[0]);
        }
        return month;
    };
    /**
     * @param {?} date
     * @param {?} dateFormat
     * @param {?} monthLabels
     * @return {?}
     */
    UtilService.prototype.formatDate = /**
     * @param {?} date
     * @param {?} dateFormat
     * @param {?} monthLabels
     * @return {?}
     */
    function (date, dateFormat, monthLabels) {
        /** @type {?} */
        var formatted = dateFormat.replace(YYYY, String(date.year));
        if (dateFormat.indexOf(MMM) !== -1) {
            formatted = formatted.replace(MMM, monthLabels[date.month]);
        }
        else if (dateFormat.indexOf(MM) !== -1) {
            formatted = formatted.replace(MM, this.preZero(date.month));
        }
        else {
            formatted = formatted.replace(M, String(date.month));
        }
        if (dateFormat.indexOf(DD) !== -1) {
            formatted = formatted.replace(DD, this.preZero(date.day));
        }
        else {
            formatted = formatted.replace(D, String(date.day));
        }
        return formatted;
    };
    /**
     * @param {?} val
     * @return {?}
     */
    UtilService.prototype.preZero = /**
     * @param {?} val
     * @return {?}
     */
    function (val) {
        return val < 10 ? "0" + val : String(val);
    };
    /**
     * @param {?} date
     * @param {?} minYear
     * @param {?} maxYear
     * @param {?} disableUntil
     * @param {?} disableSince
     * @param {?} disableWeekends
     * @param {?} disableWeekDays
     * @param {?} disableDays
     * @param {?} disableDateRanges
     * @param {?} enableDays
     * @return {?}
     */
    UtilService.prototype.isDisabledDay = /**
     * @param {?} date
     * @param {?} minYear
     * @param {?} maxYear
     * @param {?} disableUntil
     * @param {?} disableSince
     * @param {?} disableWeekends
     * @param {?} disableWeekDays
     * @param {?} disableDays
     * @param {?} disableDateRanges
     * @param {?} enableDays
     * @return {?}
     */
    function (date, minYear, maxYear, disableUntil, disableSince, disableWeekends, disableWeekDays, disableDays, disableDateRanges, enableDays) {
        var e_1, _a, e_2, _b, e_3, _c, e_4, _d;
        try {
            for (var enableDays_1 = tslib_1.__values(enableDays), enableDays_1_1 = enableDays_1.next(); !enableDays_1_1.done; enableDays_1_1 = enableDays_1.next()) {
                var e = enableDays_1_1.value;
                if (e.year === date.year && e.month === date.month && e.day === date.day) {
                    return false;
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (enableDays_1_1 && !enableDays_1_1.done && (_a = enableDays_1.return)) _a.call(enableDays_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        /** @type {?} */
        var dn = this.getDayNumber(date);
        if (date.year < minYear && date.month === 12 || date.year > maxYear && date.month === 1) {
            return true;
        }
        /** @type {?} */
        var dateMs = this.getTimeInMilliseconds(date);
        if (this.isInitializedDate(disableUntil) && dateMs <= this.getTimeInMilliseconds(disableUntil)) {
            return true;
        }
        if (this.isInitializedDate(disableSince) && dateMs >= this.getTimeInMilliseconds(disableSince)) {
            return true;
        }
        if (disableWeekends) {
            if (dn === 0 || dn === 6) {
                return true;
            }
        }
        if (disableWeekDays.length > 0) {
            try {
                for (var disableWeekDays_1 = tslib_1.__values(disableWeekDays), disableWeekDays_1_1 = disableWeekDays_1.next(); !disableWeekDays_1_1.done; disableWeekDays_1_1 = disableWeekDays_1.next()) {
                    var wd = disableWeekDays_1_1.value;
                    if (dn === this.getWeekdayIndex(wd)) {
                        return true;
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (disableWeekDays_1_1 && !disableWeekDays_1_1.done && (_b = disableWeekDays_1.return)) _b.call(disableWeekDays_1);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
        try {
            for (var disableDays_1 = tslib_1.__values(disableDays), disableDays_1_1 = disableDays_1.next(); !disableDays_1_1.done; disableDays_1_1 = disableDays_1.next()) {
                var d = disableDays_1_1.value;
                if (d.year === date.year && d.month === date.month && d.day === date.day) {
                    return true;
                }
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (disableDays_1_1 && !disableDays_1_1.done && (_c = disableDays_1.return)) _c.call(disableDays_1);
            }
            finally { if (e_3) throw e_3.error; }
        }
        try {
            for (var disableDateRanges_1 = tslib_1.__values(disableDateRanges), disableDateRanges_1_1 = disableDateRanges_1.next(); !disableDateRanges_1_1.done; disableDateRanges_1_1 = disableDateRanges_1.next()) {
                var d = disableDateRanges_1_1.value;
                if (this.isInitializedDate(d.begin) && this.isInitializedDate(d.end) && dateMs >= this.getTimeInMilliseconds(d.begin) && dateMs <= this.getTimeInMilliseconds(d.end)) {
                    return true;
                }
            }
        }
        catch (e_4_1) { e_4 = { error: e_4_1 }; }
        finally {
            try {
                if (disableDateRanges_1_1 && !disableDateRanges_1_1.done && (_d = disableDateRanges_1.return)) _d.call(disableDateRanges_1);
            }
            finally { if (e_4) throw e_4.error; }
        }
        return false;
    };
    /**
     * @param {?} date
     * @param {?} markedDates
     * @param {?} markWeekends
     * @return {?}
     */
    UtilService.prototype.isMarkedDate = /**
     * @param {?} date
     * @param {?} markedDates
     * @param {?} markWeekends
     * @return {?}
     */
    function (date, markedDates, markWeekends) {
        var e_5, _a, e_6, _b;
        try {
            for (var markedDates_1 = tslib_1.__values(markedDates), markedDates_1_1 = markedDates_1.next(); !markedDates_1_1.done; markedDates_1_1 = markedDates_1.next()) {
                var md = markedDates_1_1.value;
                try {
                    for (var _c = tslib_1.__values(md.dates), _d = _c.next(); !_d.done; _d = _c.next()) {
                        var d = _d.value;
                        if (d.year === date.year && d.month === date.month && d.day === date.day) {
                            return { marked: true, color: md.color };
                        }
                    }
                }
                catch (e_6_1) { e_6 = { error: e_6_1 }; }
                finally {
                    try {
                        if (_d && !_d.done && (_b = _c.return)) _b.call(_c);
                    }
                    finally { if (e_6) throw e_6.error; }
                }
            }
        }
        catch (e_5_1) { e_5 = { error: e_5_1 }; }
        finally {
            try {
                if (markedDates_1_1 && !markedDates_1_1.done && (_a = markedDates_1.return)) _a.call(markedDates_1);
            }
            finally { if (e_5) throw e_5.error; }
        }
        if (markWeekends && markWeekends.marked) {
            /** @type {?} */
            var dayNbr = this.getDayNumber(date);
            if (dayNbr === 0 || dayNbr === 6) {
                return { marked: true, color: markWeekends.color };
            }
        }
        return { marked: false, color: "" };
    };
    /**
     * @param {?} date
     * @param {?} sunHighlight
     * @param {?} satHighlight
     * @param {?} highlightDates
     * @return {?}
     */
    UtilService.prototype.isHighlightedDate = /**
     * @param {?} date
     * @param {?} sunHighlight
     * @param {?} satHighlight
     * @param {?} highlightDates
     * @return {?}
     */
    function (date, sunHighlight, satHighlight, highlightDates) {
        var e_7, _a;
        /** @type {?} */
        var dayNbr = this.getDayNumber(date);
        if (sunHighlight && dayNbr === 0 || satHighlight && dayNbr === 6) {
            return true;
        }
        try {
            for (var highlightDates_1 = tslib_1.__values(highlightDates), highlightDates_1_1 = highlightDates_1.next(); !highlightDates_1_1.done; highlightDates_1_1 = highlightDates_1.next()) {
                var d = highlightDates_1_1.value;
                if (d.year === date.year && d.month === date.month && d.day === date.day) {
                    return true;
                }
            }
        }
        catch (e_7_1) { e_7 = { error: e_7_1 }; }
        finally {
            try {
                if (highlightDates_1_1 && !highlightDates_1_1.done && (_a = highlightDates_1.return)) _a.call(highlightDates_1);
            }
            finally { if (e_7) throw e_7.error; }
        }
        return false;
    };
    /**
     * @param {?} date
     * @return {?}
     */
    UtilService.prototype.getWeekNumber = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        /** @type {?} */
        var d = new Date(date.year, date.month - 1, date.day, 0, 0, 0, 0);
        d.setDate(d.getDate() + (d.getDay() === 0 ? -3 : 4 - d.getDay()));
        return Math.round(((d.getTime() - new Date(d.getFullYear(), 0, 4).getTime()) / 86400000) / 7) + 1;
    };
    /**
     * @param {?} date
     * @param {?} disableUntil
     * @return {?}
     */
    UtilService.prototype.isMonthDisabledByDisableUntil = /**
     * @param {?} date
     * @param {?} disableUntil
     * @return {?}
     */
    function (date, disableUntil) {
        return this.isInitializedDate(disableUntil) && this.getTimeInMilliseconds(date) <= this.getTimeInMilliseconds(disableUntil);
    };
    /**
     * @param {?} date
     * @param {?} disableSince
     * @return {?}
     */
    UtilService.prototype.isMonthDisabledByDisableSince = /**
     * @param {?} date
     * @param {?} disableSince
     * @return {?}
     */
    function (date, disableSince) {
        return this.isInitializedDate(disableSince) && this.getTimeInMilliseconds(date) >= this.getTimeInMilliseconds(disableSince);
    };
    /**
     * @param {?} date
     * @return {?}
     */
    UtilService.prototype.isInitializedDate = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return date.year !== 0 && date.month !== 0 && date.day !== 0;
    };
    /**
     * @param {?} d1
     * @param {?} d2
     * @return {?}
     */
    UtilService.prototype.isSameDate = /**
     * @param {?} d1
     * @param {?} d2
     * @return {?}
     */
    function (d1, d2) {
        return d1.year === d2.year && d1.month === d2.month && d1.day === d2.day;
    };
    /**
     * @param {?} date
     * @return {?}
     */
    UtilService.prototype.getTimeInMilliseconds = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return new Date(date.year, date.month - 1, date.day, 0, 0, 0, 0).getTime();
    };
    /**
     * @param {?} date
     * @return {?}
     */
    UtilService.prototype.getDayNumber = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return new Date(date.year, date.month - 1, date.day, 0, 0, 0, 0).getDay();
    };
    /**
     * @return {?}
     */
    UtilService.prototype.getWeekDays = /**
     * @return {?}
     */
    function () {
        return this.weekDays;
    };
    /**
     * @param {?} wd
     * @return {?}
     */
    UtilService.prototype.getWeekdayIndex = /**
     * @param {?} wd
     * @return {?}
     */
    function (wd) {
        return this.weekDays.indexOf(wd);
    };
    UtilService.decorators = [
        { type: Injectable }
    ];
    return UtilService;
}());
export { UtilService };
if (false) {
    /** @type {?} */
    UtilService.prototype.weekDays;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktZGF0ZS1waWNrZXIudXRpbC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXlkYXRlcGlja2VyLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL215LWRhdGUtcGlja2VyLnV0aWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0lBU3JDLENBQUMsR0FBRyxHQUFHOztJQUNQLEVBQUUsR0FBRyxJQUFJOztJQUNULEdBQUcsR0FBRyxLQUFLOztJQUNYLENBQUMsR0FBRyxHQUFHOztJQUNQLEVBQUUsR0FBRyxJQUFJOztJQUNULElBQUksR0FBRyxNQUFNO0FBRW5CO0lBQUE7UUFFSSxhQUFRLEdBQWtCLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFzUHpFLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7SUFwUEcsaUNBQVc7Ozs7Ozs7Ozs7Ozs7OztJQUFYLFVBQVksT0FBZSxFQUFFLFVBQWtCLEVBQUUsT0FBZSxFQUFFLE9BQWUsRUFBRSxZQUFxQixFQUFFLFlBQXFCLEVBQUUsZUFBd0IsRUFBRSxlQUE4QixFQUFFLFdBQTJCLEVBQUUsaUJBQXNDLEVBQUUsV0FBMkIsRUFBRSxVQUEwQjs7WUFDL1MsVUFBVSxHQUFZLEVBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUM7O1lBQ2pELFdBQVcsR0FBa0IsQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQzs7WUFDN0UsVUFBVSxHQUFZLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDOztZQUNwRCxVQUFVLEdBQWtCLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLENBQUM7O1lBRXBFLFNBQVMsR0FBeUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQzs7WUFDcEYsSUFBSSxHQUFXLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7O1lBQ2xELEtBQUssR0FBVyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7O1lBQzVILEdBQUcsR0FBVyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRXJELElBQUksS0FBSyxLQUFLLENBQUMsQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsSUFBSSxJQUFJLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDM0MsSUFBSSxJQUFJLEdBQUcsT0FBTyxJQUFJLElBQUksR0FBRyxPQUFPLElBQUksS0FBSyxHQUFHLENBQUMsSUFBSSxLQUFLLEdBQUcsRUFBRSxFQUFFO2dCQUM3RCxPQUFPLFVBQVUsQ0FBQzthQUNyQjs7Z0JBRUcsSUFBSSxHQUFZLEVBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUM7WUFFeEQsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFlBQVksRUFBRSxZQUFZLEVBQUUsZUFBZSxFQUFFLGVBQWUsRUFBRSxXQUFXLEVBQUUsaUJBQWlCLEVBQUUsVUFBVSxDQUFDLEVBQUU7Z0JBQ3RKLE9BQU8sVUFBVSxDQUFDO2FBQ3JCO1lBRUQsSUFBSSxJQUFJLEdBQUcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLEtBQUssQ0FBQyxJQUFJLElBQUksR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQzFELFdBQVcsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7YUFDdkI7WUFFRCxJQUFJLEdBQUcsR0FBRyxDQUFDLElBQUksR0FBRyxHQUFHLFdBQVcsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3pDLE9BQU8sVUFBVSxDQUFDO2FBQ3JCO1lBRUQsYUFBYTtZQUNiLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxPQUFPLFVBQVUsQ0FBQztJQUN0QixDQUFDOzs7Ozs7O0lBRUQsa0NBQVk7Ozs7OztJQUFaLFVBQWEsT0FBZSxFQUFFLFVBQWtCLEVBQUUsVUFBeUI7O1lBQ25FLEdBQUcsR0FBVyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQy9CLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNqQyxHQUFHLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN2Qzs7WUFFRyxFQUFFLEdBQVEsSUFBSSxNQUFNLENBQUMsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLENBQUM7O1lBQ3JDLEVBQUUsR0FBa0IsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7O1lBQ3JDLEVBQUUsR0FBa0IsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7O1lBQ3hDLEVBQUUsR0FBeUIsRUFBRTtRQUVqQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNoQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQzVCLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBQyxDQUFDO2FBQ3pDO1lBQ0QsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUN6QixFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQzthQUN6QztZQUNELElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtnQkFDekIsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFDLENBQUM7YUFDekM7U0FDSjtRQUNELE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQzs7Ozs7O0lBRUQsK0NBQXlCOzs7OztJQUF6QixVQUEwQixFQUFpQixFQUFFLFdBQTJCO1FBQ3BFLElBQUksRUFBRSxDQUFDLEtBQUssRUFBRTtZQUNWLEtBQUssSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsSUFBSSxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUU7Z0JBQ2hDLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsS0FBSyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsV0FBVyxFQUFFLEVBQUU7b0JBQzNELE9BQU8sR0FBRyxDQUFDO2lCQUNkO2FBQ0o7U0FDSjtRQUNELE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDZCxDQUFDOzs7OztJQUVELHNDQUFnQjs7OztJQUFoQixVQUFpQixFQUFpQjtRQUM5QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDekIsT0FBTyxDQUFDLENBQUMsQ0FBQztTQUNiOztZQUVHLEdBQUcsR0FBVyxNQUFNLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztRQUNsQyxJQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksR0FBRyxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxFQUFFLEVBQUU7WUFDN0gsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1NBQ1o7YUFDSSxJQUFJLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDcEQsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1NBQ1o7UUFDRCxPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7Ozs7O0lBRUQsNkNBQXVCOzs7O0lBQXZCLFVBQXdCLFVBQWtCO1FBQ3RDLE9BQU8sVUFBVSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7OztJQUVELHVDQUFpQjs7OztJQUFqQixVQUFrQixXQUFtQjs7WUFDN0IsS0FBSyxHQUFhLEVBQUMsUUFBUSxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUM7UUFDMUQsSUFBSSxXQUFXLEtBQUssRUFBRSxFQUFFOztnQkFDaEIsS0FBSyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3RCxLQUFLLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqRixLQUFLLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNoRjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7Ozs7SUFFRCxnQ0FBVTs7Ozs7O0lBQVYsVUFBVyxJQUFhLEVBQUUsVUFBa0IsRUFBRSxXQUEyQjs7WUFDakUsU0FBUyxHQUFXLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFbkUsSUFBSSxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ2hDLFNBQVMsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7U0FDL0Q7YUFDSSxJQUFJLFVBQVUsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDcEMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxPQUFPLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7U0FDL0Q7YUFDSTtZQUNELFNBQVMsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7U0FDeEQ7UUFFRCxJQUFJLFVBQVUsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDL0IsU0FBUyxHQUFHLFNBQVMsQ0FBQyxPQUFPLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDN0Q7YUFDSTtZQUNELFNBQVMsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDdEQ7UUFDRCxPQUFPLFNBQVMsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELDZCQUFPOzs7O0lBQVAsVUFBUSxHQUFXO1FBQ2YsT0FBTyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDOUMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7SUFFRCxtQ0FBYTs7Ozs7Ozs7Ozs7OztJQUFiLFVBQWMsSUFBYSxFQUFFLE9BQWUsRUFBRSxPQUFlLEVBQUUsWUFBcUIsRUFBRSxZQUFxQixFQUFFLGVBQXdCLEVBQUUsZUFBOEIsRUFBRSxXQUEyQixFQUFFLGlCQUFzQyxFQUFFLFVBQTBCOzs7WUFDbFEsS0FBYyxJQUFBLGVBQUEsaUJBQUEsVUFBVSxDQUFBLHNDQUFBLDhEQUFFO2dCQUFyQixJQUFJLENBQUMsdUJBQUE7Z0JBQ04sSUFBSSxDQUFDLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLEdBQUcsRUFBRTtvQkFDdEUsT0FBTyxLQUFLLENBQUM7aUJBQ2hCO2FBQ0o7Ozs7Ozs7Ozs7WUFFRyxFQUFFLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7UUFFaEMsSUFBSSxJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLENBQUMsRUFBRTtZQUNyRixPQUFPLElBQUksQ0FBQztTQUNmOztZQUVHLE1BQU0sR0FBVyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDO1FBQ3JELElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxDQUFDLEVBQUU7WUFDNUYsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUVELElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxDQUFDLEVBQUU7WUFDNUYsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUVELElBQUksZUFBZSxFQUFFO1lBQ2pCLElBQUksRUFBRSxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxFQUFFO2dCQUN0QixPQUFPLElBQUksQ0FBQzthQUNmO1NBQ0o7UUFFRCxJQUFJLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztnQkFDNUIsS0FBZSxJQUFBLG9CQUFBLGlCQUFBLGVBQWUsQ0FBQSxnREFBQSw2RUFBRTtvQkFBM0IsSUFBSSxFQUFFLDRCQUFBO29CQUNQLElBQUksRUFBRSxLQUFLLElBQUksQ0FBQyxlQUFlLENBQUMsRUFBRSxDQUFDLEVBQUU7d0JBQ2pDLE9BQU8sSUFBSSxDQUFDO3FCQUNmO2lCQUNKOzs7Ozs7Ozs7U0FDSjs7WUFFRCxLQUFjLElBQUEsZ0JBQUEsaUJBQUEsV0FBVyxDQUFBLHdDQUFBLGlFQUFFO2dCQUF0QixJQUFJLENBQUMsd0JBQUE7Z0JBQ04sSUFBSSxDQUFDLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLEdBQUcsRUFBRTtvQkFDdEUsT0FBTyxJQUFJLENBQUM7aUJBQ2Y7YUFDSjs7Ozs7Ozs7OztZQUVELEtBQWMsSUFBQSxzQkFBQSxpQkFBQSxpQkFBaUIsQ0FBQSxvREFBQSxtRkFBRTtnQkFBNUIsSUFBSSxDQUFDLDhCQUFBO2dCQUNOLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUNsSyxPQUFPLElBQUksQ0FBQztpQkFDZjthQUNKOzs7Ozs7Ozs7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOzs7Ozs7O0lBRUQsa0NBQVk7Ozs7OztJQUFaLFVBQWEsSUFBYSxFQUFFLFdBQWtDLEVBQUUsWUFBMkI7OztZQUN2RixLQUFlLElBQUEsZ0JBQUEsaUJBQUEsV0FBVyxDQUFBLHdDQUFBLGlFQUFFO2dCQUF2QixJQUFJLEVBQUUsd0JBQUE7O29CQUNQLEtBQWMsSUFBQSxLQUFBLGlCQUFBLEVBQUUsQ0FBQyxLQUFLLENBQUEsZ0JBQUEsNEJBQUU7d0JBQW5CLElBQUksQ0FBQyxXQUFBO3dCQUNOLElBQUksQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLElBQUksQ0FBQyxHQUFHLEVBQUU7NEJBQ3RFLE9BQU8sRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUMsS0FBSyxFQUFDLENBQUM7eUJBQzFDO3FCQUNKOzs7Ozs7Ozs7YUFDSjs7Ozs7Ozs7O1FBQ0QsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLE1BQU0sRUFBRTs7Z0JBQ2pDLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQztZQUNwQyxJQUFJLE1BQU0sS0FBSyxDQUFDLElBQUksTUFBTSxLQUFLLENBQUMsRUFBRTtnQkFDOUIsT0FBTyxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLFlBQVksQ0FBQyxLQUFLLEVBQUMsQ0FBQzthQUNwRDtTQUNKO1FBQ0QsT0FBTyxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBQyxDQUFDO0lBQ3RDLENBQUM7Ozs7Ozs7O0lBRUQsdUNBQWlCOzs7Ozs7O0lBQWpCLFVBQWtCLElBQWEsRUFBRSxZQUFxQixFQUFFLFlBQXFCLEVBQUUsY0FBOEI7OztZQUNyRyxNQUFNLEdBQVcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7UUFDNUMsSUFBSSxZQUFZLElBQUksTUFBTSxLQUFLLENBQUMsSUFBSSxZQUFZLElBQUksTUFBTSxLQUFLLENBQUMsRUFBRTtZQUM5RCxPQUFPLElBQUksQ0FBQztTQUNmOztZQUNELEtBQWMsSUFBQSxtQkFBQSxpQkFBQSxjQUFjLENBQUEsOENBQUEsMEVBQUU7Z0JBQXpCLElBQUksQ0FBQywyQkFBQTtnQkFDTixJQUFJLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLEdBQUcsS0FBSyxJQUFJLENBQUMsR0FBRyxFQUFFO29CQUN0RSxPQUFPLElBQUksQ0FBQztpQkFDZjthQUNKOzs7Ozs7Ozs7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOzs7OztJQUVELG1DQUFhOzs7O0lBQWIsVUFBYyxJQUFhOztZQUNuQixDQUFDLEdBQVMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN2RSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNsRSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3RHLENBQUM7Ozs7OztJQUVELG1EQUE2Qjs7Ozs7SUFBN0IsVUFBOEIsSUFBYSxFQUFFLFlBQXFCO1FBQzlELE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDaEksQ0FBQzs7Ozs7O0lBRUQsbURBQTZCOzs7OztJQUE3QixVQUE4QixJQUFhLEVBQUUsWUFBcUI7UUFDOUQsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNoSSxDQUFDOzs7OztJQUVELHVDQUFpQjs7OztJQUFqQixVQUFrQixJQUFhO1FBQzNCLE9BQU8sSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUM7SUFDakUsQ0FBQzs7Ozs7O0lBRUQsZ0NBQVU7Ozs7O0lBQVYsVUFBVyxFQUFXLEVBQUUsRUFBVztRQUMvQixPQUFPLEVBQUUsQ0FBQyxJQUFJLEtBQUssRUFBRSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsS0FBSyxLQUFLLEVBQUUsQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsR0FBRyxDQUFDO0lBQzdFLENBQUM7Ozs7O0lBRUQsMkNBQXFCOzs7O0lBQXJCLFVBQXNCLElBQWE7UUFDL0IsT0FBTyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDL0UsQ0FBQzs7Ozs7SUFFRCxrQ0FBWTs7OztJQUFaLFVBQWEsSUFBYTtRQUN0QixPQUFPLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUUsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUM5RSxDQUFDOzs7O0lBRUQsaUNBQVc7OztJQUFYO1FBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7Ozs7O0lBRUQscUNBQWU7Ozs7SUFBZixVQUFnQixFQUFVO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDckMsQ0FBQzs7Z0JBdlBKLFVBQVU7O0lBd1BYLGtCQUFDO0NBQUEsQUF4UEQsSUF3UEM7U0F2UFksV0FBVzs7O0lBQ3BCLCtCQUFxRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgSU15RGF0ZSB9IGZyb20gXCIuLi9pbnRlcmZhY2VzL215LWRhdGUuaW50ZXJmYWNlXCI7XG5pbXBvcnQgeyBJTXlEYXRlUmFuZ2UgfSBmcm9tIFwiLi4vaW50ZXJmYWNlcy9teS1kYXRlLXJhbmdlLmludGVyZmFjZVwiO1xuaW1wb3J0IHsgSU15TW9udGggfSBmcm9tIFwiLi4vaW50ZXJmYWNlcy9teS1tb250aC5pbnRlcmZhY2VcIjtcbmltcG9ydCB7IElNeU1vbnRoTGFiZWxzIH0gZnJvbSBcIi4uL2ludGVyZmFjZXMvbXktbW9udGgtbGFiZWxzLmludGVyZmFjZVwiO1xuaW1wb3J0IHsgSU15TWFya2VkRGF0ZXMgfSBmcm9tIFwiLi4vaW50ZXJmYWNlcy9teS1tYXJrZWQtZGF0ZXMuaW50ZXJmYWNlXCI7XG5pbXBvcnQgeyBJTXlNYXJrZWREYXRlIH0gZnJvbSBcIi4uL2ludGVyZmFjZXMvbXktbWFya2VkLWRhdGUuaW50ZXJmYWNlXCI7XG5pbXBvcnQgeyBJTXlEYXRlRm9ybWF0IH0gZnJvbSBcIi4uL2ludGVyZmFjZXMvbXktZGF0ZS1mb3JtYXQuaW50ZXJmYWNlXCI7XG5cbmNvbnN0IE0gPSBcIm1cIjtcbmNvbnN0IE1NID0gXCJtbVwiO1xuY29uc3QgTU1NID0gXCJtbW1cIjtcbmNvbnN0IEQgPSBcImRcIjtcbmNvbnN0IEREID0gXCJkZFwiO1xuY29uc3QgWVlZWSA9IFwieXl5eVwiO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgVXRpbFNlcnZpY2Uge1xuICAgIHdlZWtEYXlzOiBBcnJheTxzdHJpbmc+ID0gW1wic3VcIiwgXCJtb1wiLCBcInR1XCIsIFwid2VcIiwgXCJ0aFwiLCBcImZyXCIsIFwic2FcIl07XG5cbiAgICBpc0RhdGVWYWxpZChkYXRlU3RyOiBzdHJpbmcsIGRhdGVGb3JtYXQ6IHN0cmluZywgbWluWWVhcjogbnVtYmVyLCBtYXhZZWFyOiBudW1iZXIsIGRpc2FibGVVbnRpbDogSU15RGF0ZSwgZGlzYWJsZVNpbmNlOiBJTXlEYXRlLCBkaXNhYmxlV2Vla2VuZHM6IGJvb2xlYW4sIGRpc2FibGVXZWVrRGF5czogQXJyYXk8c3RyaW5nPiwgZGlzYWJsZURheXM6IEFycmF5PElNeURhdGU+LCBkaXNhYmxlRGF0ZVJhbmdlczogQXJyYXk8SU15RGF0ZVJhbmdlPiwgbW9udGhMYWJlbHM6IElNeU1vbnRoTGFiZWxzLCBlbmFibGVEYXlzOiBBcnJheTxJTXlEYXRlPik6IElNeURhdGUge1xuICAgICAgICBsZXQgcmV0dXJuRGF0ZTogSU15RGF0ZSA9IHtkYXk6IDAsIG1vbnRoOiAwLCB5ZWFyOiAwfTtcbiAgICAgICAgbGV0IGRheXNJbk1vbnRoOiBBcnJheTxudW1iZXI+ID0gWzMxLCAyOCwgMzEsIDMwLCAzMSwgMzAsIDMxLCAzMSwgMzAsIDMxLCAzMCwgMzFdO1xuICAgICAgICBsZXQgaXNNb250aFN0cjogYm9vbGVhbiA9IGRhdGVGb3JtYXQuaW5kZXhPZihNTU0pICE9PSAtMTtcbiAgICAgICAgbGV0IGRlbGltZXRlcnM6IEFycmF5PHN0cmluZz4gPSB0aGlzLmdldERhdGVGb3JtYXREZWxpbWV0ZXJzKGRhdGVGb3JtYXQpO1xuXG4gICAgICAgIGxldCBkYXRlVmFsdWU6IEFycmF5PElNeURhdGVGb3JtYXQ+ID0gdGhpcy5nZXREYXRlVmFsdWUoZGF0ZVN0ciwgZGF0ZUZvcm1hdCwgZGVsaW1ldGVycyk7XG4gICAgICAgIGxldCB5ZWFyOiBudW1iZXIgPSB0aGlzLmdldE51bWJlckJ5VmFsdWUoZGF0ZVZhbHVlWzBdKTtcbiAgICAgICAgbGV0IG1vbnRoOiBudW1iZXIgPSBpc01vbnRoU3RyID8gdGhpcy5nZXRNb250aE51bWJlckJ5TW9udGhOYW1lKGRhdGVWYWx1ZVsxXSwgbW9udGhMYWJlbHMpIDogdGhpcy5nZXROdW1iZXJCeVZhbHVlKGRhdGVWYWx1ZVsxXSk7XG4gICAgICAgIGxldCBkYXk6IG51bWJlciA9IHRoaXMuZ2V0TnVtYmVyQnlWYWx1ZShkYXRlVmFsdWVbMl0pO1xuXG4gICAgICAgIGlmIChtb250aCAhPT0gLTEgJiYgZGF5ICE9PSAtMSAmJiB5ZWFyICE9PSAtMSkge1xuICAgICAgICAgICAgaWYgKHllYXIgPCBtaW5ZZWFyIHx8IHllYXIgPiBtYXhZZWFyIHx8IG1vbnRoIDwgMSB8fCBtb250aCA+IDEyKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJldHVybkRhdGU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGxldCBkYXRlOiBJTXlEYXRlID0ge3llYXI6IHllYXIsIG1vbnRoOiBtb250aCwgZGF5OiBkYXl9O1xuXG4gICAgICAgICAgICBpZiAodGhpcy5pc0Rpc2FibGVkRGF5KGRhdGUsIG1pblllYXIsIG1heFllYXIsIGRpc2FibGVVbnRpbCwgZGlzYWJsZVNpbmNlLCBkaXNhYmxlV2Vla2VuZHMsIGRpc2FibGVXZWVrRGF5cywgZGlzYWJsZURheXMsIGRpc2FibGVEYXRlUmFuZ2VzLCBlbmFibGVEYXlzKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiByZXR1cm5EYXRlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoeWVhciAlIDQwMCA9PT0gMCB8fCAoeWVhciAlIDEwMCAhPT0gMCAmJiB5ZWFyICUgNCA9PT0gMCkpIHtcbiAgICAgICAgICAgICAgICBkYXlzSW5Nb250aFsxXSA9IDI5O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoZGF5IDwgMSB8fCBkYXkgPiBkYXlzSW5Nb250aFttb250aCAtIDFdKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJldHVybkRhdGU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIFZhbGlkIGRhdGVcbiAgICAgICAgICAgIHJldHVybiBkYXRlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXR1cm5EYXRlO1xuICAgIH1cblxuICAgIGdldERhdGVWYWx1ZShkYXRlU3RyOiBzdHJpbmcsIGRhdGVGb3JtYXQ6IHN0cmluZywgZGVsaW1ldGVyczogQXJyYXk8c3RyaW5nPik6IEFycmF5PElNeURhdGVGb3JtYXQ+IHtcbiAgICAgICAgbGV0IGRlbDogc3RyaW5nID0gZGVsaW1ldGVyc1swXTtcbiAgICAgICAgaWYgKGRlbGltZXRlcnNbMF0gIT09IGRlbGltZXRlcnNbMV0pIHtcbiAgICAgICAgICAgIGRlbCA9IGRlbGltZXRlcnNbMF0gKyBkZWxpbWV0ZXJzWzFdO1xuICAgICAgICB9XG5cbiAgICAgICAgbGV0IHJlOiBhbnkgPSBuZXcgUmVnRXhwKFwiW1wiICsgZGVsICsgXCJdXCIpO1xuICAgICAgICBsZXQgZHM6IEFycmF5PHN0cmluZz4gPSBkYXRlU3RyLnNwbGl0KHJlKTtcbiAgICAgICAgbGV0IGRmOiBBcnJheTxzdHJpbmc+ID0gZGF0ZUZvcm1hdC5zcGxpdChyZSk7XG4gICAgICAgIGxldCBkYTogQXJyYXk8SU15RGF0ZUZvcm1hdD4gPSBbXTtcblxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGRmLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoZGZbaV0uaW5kZXhPZihZWVlZKSAhPT0gLTEpIHtcbiAgICAgICAgICAgICAgICBkYVswXSA9IHt2YWx1ZTogZHNbaV0sIGZvcm1hdDogZGZbaV19O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGRmW2ldLmluZGV4T2YoTSkgIT09IC0xKSB7XG4gICAgICAgICAgICAgICAgZGFbMV0gPSB7dmFsdWU6IGRzW2ldLCBmb3JtYXQ6IGRmW2ldfTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChkZltpXS5pbmRleE9mKEQpICE9PSAtMSkge1xuICAgICAgICAgICAgICAgIGRhWzJdID0ge3ZhbHVlOiBkc1tpXSwgZm9ybWF0OiBkZltpXX07XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGRhO1xuICAgIH1cblxuICAgIGdldE1vbnRoTnVtYmVyQnlNb250aE5hbWUoZGY6IElNeURhdGVGb3JtYXQsIG1vbnRoTGFiZWxzOiBJTXlNb250aExhYmVscyk6IG51bWJlciB7XG4gICAgICAgIGlmIChkZi52YWx1ZSkge1xuICAgICAgICAgICAgZm9yIChsZXQga2V5ID0gMTsga2V5IDw9IDEyOyBrZXkrKykge1xuICAgICAgICAgICAgICAgIGlmIChkZi52YWx1ZS50b0xvd2VyQ2FzZSgpID09PSBtb250aExhYmVsc1trZXldLnRvTG93ZXJDYXNlKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGtleTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIC0xO1xuICAgIH1cblxuICAgIGdldE51bWJlckJ5VmFsdWUoZGY6IElNeURhdGVGb3JtYXQpOiBudW1iZXIge1xuICAgICAgICBpZiAoIS9eXFxkKyQvLnRlc3QoZGYudmFsdWUpKSB7XG4gICAgICAgICAgICByZXR1cm4gLTE7XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgbmJyOiBudW1iZXIgPSBOdW1iZXIoZGYudmFsdWUpO1xuICAgICAgICBpZiAoZGYuZm9ybWF0Lmxlbmd0aCA9PT0gMSAmJiBkZi52YWx1ZS5sZW5ndGggIT09IDEgJiYgbmJyIDwgMTAgfHwgZGYuZm9ybWF0Lmxlbmd0aCA9PT0gMSAmJiBkZi52YWx1ZS5sZW5ndGggIT09IDIgJiYgbmJyID49IDEwKSB7XG4gICAgICAgICAgICBuYnIgPSAtMTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChkZi5mb3JtYXQubGVuZ3RoID09PSAyICYmIGRmLnZhbHVlLmxlbmd0aCA+IDIpIHtcbiAgICAgICAgICAgIG5iciA9IC0xO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBuYnI7XG4gICAgfVxuXG4gICAgZ2V0RGF0ZUZvcm1hdERlbGltZXRlcnMoZGF0ZUZvcm1hdDogc3RyaW5nKTogQXJyYXk8c3RyaW5nPiB7XG4gICAgICAgIHJldHVybiBkYXRlRm9ybWF0Lm1hdGNoKC9bXihkbXkpXXsxLH0vZyk7XG4gICAgfVxuXG4gICAgcGFyc2VEZWZhdWx0TW9udGgobW9udGhTdHJpbmc6IHN0cmluZyk6IElNeU1vbnRoIHtcbiAgICAgICAgbGV0IG1vbnRoOiBJTXlNb250aCA9IHttb250aFR4dDogXCJcIiwgbW9udGhOYnI6IDAsIHllYXI6IDB9O1xuICAgICAgICBpZiAobW9udGhTdHJpbmcgIT09IFwiXCIpIHtcbiAgICAgICAgICAgIGxldCBzcGxpdCA9IG1vbnRoU3RyaW5nLnNwbGl0KG1vbnRoU3RyaW5nLm1hdGNoKC9bXjAtOV0vKVswXSk7XG4gICAgICAgICAgICBtb250aC5tb250aE5iciA9IHNwbGl0WzBdLmxlbmd0aCA9PT0gMiA/IHBhcnNlSW50KHNwbGl0WzBdKSA6IHBhcnNlSW50KHNwbGl0WzFdKTtcbiAgICAgICAgICAgIG1vbnRoLnllYXIgPSBzcGxpdFswXS5sZW5ndGggPT09IDIgPyBwYXJzZUludChzcGxpdFsxXSkgOiBwYXJzZUludChzcGxpdFswXSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG1vbnRoO1xuICAgIH1cblxuICAgIGZvcm1hdERhdGUoZGF0ZTogSU15RGF0ZSwgZGF0ZUZvcm1hdDogc3RyaW5nLCBtb250aExhYmVsczogSU15TW9udGhMYWJlbHMpOiBzdHJpbmcge1xuICAgICAgICBsZXQgZm9ybWF0dGVkOiBzdHJpbmcgPSBkYXRlRm9ybWF0LnJlcGxhY2UoWVlZWSwgU3RyaW5nKGRhdGUueWVhcikpO1xuXG4gICAgICAgIGlmIChkYXRlRm9ybWF0LmluZGV4T2YoTU1NKSAhPT0gLTEpIHtcbiAgICAgICAgICAgIGZvcm1hdHRlZCA9IGZvcm1hdHRlZC5yZXBsYWNlKE1NTSwgbW9udGhMYWJlbHNbZGF0ZS5tb250aF0pO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKGRhdGVGb3JtYXQuaW5kZXhPZihNTSkgIT09IC0xKSB7XG4gICAgICAgICAgICBmb3JtYXR0ZWQgPSBmb3JtYXR0ZWQucmVwbGFjZShNTSwgdGhpcy5wcmVaZXJvKGRhdGUubW9udGgpKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGZvcm1hdHRlZCA9IGZvcm1hdHRlZC5yZXBsYWNlKE0sIFN0cmluZyhkYXRlLm1vbnRoKSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZGF0ZUZvcm1hdC5pbmRleE9mKEREKSAhPT0gLTEpIHtcbiAgICAgICAgICAgIGZvcm1hdHRlZCA9IGZvcm1hdHRlZC5yZXBsYWNlKERELCB0aGlzLnByZVplcm8oZGF0ZS5kYXkpKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGZvcm1hdHRlZCA9IGZvcm1hdHRlZC5yZXBsYWNlKEQsIFN0cmluZyhkYXRlLmRheSkpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmb3JtYXR0ZWQ7XG4gICAgfVxuXG4gICAgcHJlWmVybyh2YWw6IG51bWJlcik6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB2YWwgPCAxMCA/IFwiMFwiICsgdmFsIDogU3RyaW5nKHZhbCk7XG4gICAgfVxuXG4gICAgaXNEaXNhYmxlZERheShkYXRlOiBJTXlEYXRlLCBtaW5ZZWFyOiBudW1iZXIsIG1heFllYXI6IG51bWJlciwgZGlzYWJsZVVudGlsOiBJTXlEYXRlLCBkaXNhYmxlU2luY2U6IElNeURhdGUsIGRpc2FibGVXZWVrZW5kczogYm9vbGVhbiwgZGlzYWJsZVdlZWtEYXlzOiBBcnJheTxzdHJpbmc+LCBkaXNhYmxlRGF5czogQXJyYXk8SU15RGF0ZT4sIGRpc2FibGVEYXRlUmFuZ2VzOiBBcnJheTxJTXlEYXRlUmFuZ2U+LCBlbmFibGVEYXlzOiBBcnJheTxJTXlEYXRlPik6IGJvb2xlYW4ge1xuICAgICAgICBmb3IgKGxldCBlIG9mIGVuYWJsZURheXMpIHtcbiAgICAgICAgICAgIGlmIChlLnllYXIgPT09IGRhdGUueWVhciAmJiBlLm1vbnRoID09PSBkYXRlLm1vbnRoICYmIGUuZGF5ID09PSBkYXRlLmRheSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBkbiA9IHRoaXMuZ2V0RGF5TnVtYmVyKGRhdGUpO1xuXG4gICAgICAgIGlmIChkYXRlLnllYXIgPCBtaW5ZZWFyICYmIGRhdGUubW9udGggPT09IDEyIHx8IGRhdGUueWVhciA+IG1heFllYXIgJiYgZGF0ZS5tb250aCA9PT0gMSkge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICBsZXQgZGF0ZU1zOiBudW1iZXIgPSB0aGlzLmdldFRpbWVJbk1pbGxpc2Vjb25kcyhkYXRlKTtcbiAgICAgICAgaWYgKHRoaXMuaXNJbml0aWFsaXplZERhdGUoZGlzYWJsZVVudGlsKSAmJiBkYXRlTXMgPD0gdGhpcy5nZXRUaW1lSW5NaWxsaXNlY29uZHMoZGlzYWJsZVVudGlsKSkge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5pc0luaXRpYWxpemVkRGF0ZShkaXNhYmxlU2luY2UpICYmIGRhdGVNcyA+PSB0aGlzLmdldFRpbWVJbk1pbGxpc2Vjb25kcyhkaXNhYmxlU2luY2UpKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChkaXNhYmxlV2Vla2VuZHMpIHtcbiAgICAgICAgICAgIGlmIChkbiA9PT0gMCB8fCBkbiA9PT0gNikge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGRpc2FibGVXZWVrRGF5cy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICBmb3IgKGxldCB3ZCBvZiBkaXNhYmxlV2Vla0RheXMpIHtcbiAgICAgICAgICAgICAgICBpZiAoZG4gPT09IHRoaXMuZ2V0V2Vla2RheUluZGV4KHdkKSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmb3IgKGxldCBkIG9mIGRpc2FibGVEYXlzKSB7XG4gICAgICAgICAgICBpZiAoZC55ZWFyID09PSBkYXRlLnllYXIgJiYgZC5tb250aCA9PT0gZGF0ZS5tb250aCAmJiBkLmRheSA9PT0gZGF0ZS5kYXkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGZvciAobGV0IGQgb2YgZGlzYWJsZURhdGVSYW5nZXMpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmlzSW5pdGlhbGl6ZWREYXRlKGQuYmVnaW4pICYmIHRoaXMuaXNJbml0aWFsaXplZERhdGUoZC5lbmQpICYmIGRhdGVNcyA+PSB0aGlzLmdldFRpbWVJbk1pbGxpc2Vjb25kcyhkLmJlZ2luKSAmJiBkYXRlTXMgPD0gdGhpcy5nZXRUaW1lSW5NaWxsaXNlY29uZHMoZC5lbmQpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGlzTWFya2VkRGF0ZShkYXRlOiBJTXlEYXRlLCBtYXJrZWREYXRlczogQXJyYXk8SU15TWFya2VkRGF0ZXM+LCBtYXJrV2Vla2VuZHM6IElNeU1hcmtlZERhdGUpOiBJTXlNYXJrZWREYXRlIHtcbiAgICAgICAgZm9yIChsZXQgbWQgb2YgbWFya2VkRGF0ZXMpIHtcbiAgICAgICAgICAgIGZvciAobGV0IGQgb2YgbWQuZGF0ZXMpIHtcbiAgICAgICAgICAgICAgICBpZiAoZC55ZWFyID09PSBkYXRlLnllYXIgJiYgZC5tb250aCA9PT0gZGF0ZS5tb250aCAmJiBkLmRheSA9PT0gZGF0ZS5kYXkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHttYXJrZWQ6IHRydWUsIGNvbG9yOiBtZC5jb2xvcn07XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmIChtYXJrV2Vla2VuZHMgJiYgbWFya1dlZWtlbmRzLm1hcmtlZCkge1xuICAgICAgICAgICAgbGV0IGRheU5iciA9IHRoaXMuZ2V0RGF5TnVtYmVyKGRhdGUpO1xuICAgICAgICAgICAgaWYgKGRheU5iciA9PT0gMCB8fCBkYXlOYnIgPT09IDYpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4ge21hcmtlZDogdHJ1ZSwgY29sb3I6IG1hcmtXZWVrZW5kcy5jb2xvcn07XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHttYXJrZWQ6IGZhbHNlLCBjb2xvcjogXCJcIn07XG4gICAgfVxuXG4gICAgaXNIaWdobGlnaHRlZERhdGUoZGF0ZTogSU15RGF0ZSwgc3VuSGlnaGxpZ2h0OiBib29sZWFuLCBzYXRIaWdobGlnaHQ6IGJvb2xlYW4sIGhpZ2hsaWdodERhdGVzOiBBcnJheTxJTXlEYXRlPik6IGJvb2xlYW4ge1xuICAgICAgICBsZXQgZGF5TmJyOiBudW1iZXIgPSB0aGlzLmdldERheU51bWJlcihkYXRlKTtcbiAgICAgICAgaWYgKHN1bkhpZ2hsaWdodCAmJiBkYXlOYnIgPT09IDAgfHwgc2F0SGlnaGxpZ2h0ICYmIGRheU5iciA9PT0gNikge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgZm9yIChsZXQgZCBvZiBoaWdobGlnaHREYXRlcykge1xuICAgICAgICAgICAgaWYgKGQueWVhciA9PT0gZGF0ZS55ZWFyICYmIGQubW9udGggPT09IGRhdGUubW9udGggJiYgZC5kYXkgPT09IGRhdGUuZGF5KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGdldFdlZWtOdW1iZXIoZGF0ZTogSU15RGF0ZSk6IG51bWJlciB7XG4gICAgICAgIGxldCBkOiBEYXRlID0gbmV3IERhdGUoZGF0ZS55ZWFyLCBkYXRlLm1vbnRoIC0gMSwgZGF0ZS5kYXksIDAsIDAsIDAsIDApO1xuICAgICAgICBkLnNldERhdGUoZC5nZXREYXRlKCkgKyAoZC5nZXREYXkoKSA9PT0gMCA/IC0zIDogNCAtIGQuZ2V0RGF5KCkpKTtcbiAgICAgICAgcmV0dXJuIE1hdGgucm91bmQoKChkLmdldFRpbWUoKSAtIG5ldyBEYXRlKGQuZ2V0RnVsbFllYXIoKSwgMCwgNCkuZ2V0VGltZSgpKSAvIDg2NDAwMDAwKSAvIDcpICsgMTtcbiAgICB9XG5cbiAgICBpc01vbnRoRGlzYWJsZWRCeURpc2FibGVVbnRpbChkYXRlOiBJTXlEYXRlLCBkaXNhYmxlVW50aWw6IElNeURhdGUpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNJbml0aWFsaXplZERhdGUoZGlzYWJsZVVudGlsKSAmJiB0aGlzLmdldFRpbWVJbk1pbGxpc2Vjb25kcyhkYXRlKSA8PSB0aGlzLmdldFRpbWVJbk1pbGxpc2Vjb25kcyhkaXNhYmxlVW50aWwpO1xuICAgIH1cblxuICAgIGlzTW9udGhEaXNhYmxlZEJ5RGlzYWJsZVNpbmNlKGRhdGU6IElNeURhdGUsIGRpc2FibGVTaW5jZTogSU15RGF0ZSk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5pc0luaXRpYWxpemVkRGF0ZShkaXNhYmxlU2luY2UpICYmIHRoaXMuZ2V0VGltZUluTWlsbGlzZWNvbmRzKGRhdGUpID49IHRoaXMuZ2V0VGltZUluTWlsbGlzZWNvbmRzKGRpc2FibGVTaW5jZSk7XG4gICAgfVxuXG4gICAgaXNJbml0aWFsaXplZERhdGUoZGF0ZTogSU15RGF0ZSk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gZGF0ZS55ZWFyICE9PSAwICYmIGRhdGUubW9udGggIT09IDAgJiYgZGF0ZS5kYXkgIT09IDA7XG4gICAgfVxuXG4gICAgaXNTYW1lRGF0ZShkMTogSU15RGF0ZSwgZDI6IElNeURhdGUpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIGQxLnllYXIgPT09IGQyLnllYXIgJiYgZDEubW9udGggPT09IGQyLm1vbnRoICYmIGQxLmRheSA9PT0gZDIuZGF5O1xuICAgIH1cblxuICAgIGdldFRpbWVJbk1pbGxpc2Vjb25kcyhkYXRlOiBJTXlEYXRlKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIG5ldyBEYXRlKGRhdGUueWVhciwgZGF0ZS5tb250aCAtIDEsIGRhdGUuZGF5LCAwLCAwLCAwLCAwKS5nZXRUaW1lKCk7XG4gICAgfVxuXG4gICAgZ2V0RGF5TnVtYmVyKGRhdGU6IElNeURhdGUpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gbmV3IERhdGUoZGF0ZS55ZWFyLCBkYXRlLm1vbnRoIC0gMSwgZGF0ZS5kYXksIDAsIDAsIDAsIDApLmdldERheSgpO1xuICAgIH1cblxuICAgIGdldFdlZWtEYXlzKCk6IEFycmF5PHN0cmluZz4ge1xuICAgICAgICByZXR1cm4gdGhpcy53ZWVrRGF5cztcbiAgICB9XG5cbiAgICBnZXRXZWVrZGF5SW5kZXgod2Q6IHN0cmluZykge1xuICAgICAgICByZXR1cm4gdGhpcy53ZWVrRGF5cy5pbmRleE9mKHdkKTtcbiAgICB9XG59Il19